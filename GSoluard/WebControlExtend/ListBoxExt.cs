﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GSoluard.WebControlExtend.Interfaces;

namespace GSoluard.WebControlExtend
{
    [ParseChildren(true)]
    [PersistChildren(true)]
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:ListBoxExt runat=server  Text=Button></{0}:ListBoxExt>")]
    public class ListBoxExt : ListBox
    {

        protected override void OnInit(EventArgs e)
        {            
            base.OnInit(e);
        }

        public void Settings(IListBoxExtSetting s)
        {
            s.ApplySetting(this);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GSoluard.WebControlExtend.Interfaces
{
    public interface IImageButtonExtSetting
    {
        void ApplySetting(ImageButtonExt btne);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GSoluard.WebControlExtend.Interfaces
{
    public interface IButtonExtSetting
    {
        void ApplySetting(ButtonExt btne);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GSoluard.WebControlExtend
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:GridViewExt runat=server></{0}:GridViewExt>")]
    public class GridViewExt : GridView
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        public void Settings(IGridViewExtSetting s)
        {
            s.ApplySetting(this);
        }

        protected override void RenderContents(HtmlTextWriter output)
        {
            base.RenderContents(output);
        }
    }

}

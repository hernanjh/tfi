﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GSoluard.WebControlExtend
{
    [ParseChildren(true)]
    [PersistChildren(true)]
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:RadioButtonExt runat=server></{0}:RadioButtonExt>")]
    public class RadioButtonExt : RadioButton, IControlValidatable
    {

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (Page != null)
            {
                Page.RegisterRequiresControlState(this);
                Page.RegisterRequiresPostBack(this);
            }
        }

        [Bindable(true)]
        [Browsable(true)]
        [DefaultValue("false")]
        [Category("Validations")]
        [Description("Setea si el campo es obligatorio.")]
        public bool Required
        {
            get
            {
                if (this.ViewState["Required"] == null)
                    this.ViewState["Required"] = false;
                return (bool)this.ViewState["Required"];
            }
            set
            {
                this.ViewState["Required"] = value;
            }
        }

        [Bindable(true)]
        [Browsable(true)]
        [DefaultValue(ValidationType.None)]
        [Category("Validations")]
        [Description("Tipo de dato a validar.")]
        public ValidationType Type
        {
            get
            {
                if (this.ViewState["Type"] == null)
                    this.ViewState["Type"] = ValidationType.None;
                return (ValidationType)this.ViewState["Type"];
            }
            set
            {
                this.ViewState["Type"] = value;
            }
        }

        protected override void AddAttributesToRender(HtmlTextWriter output)
        {

            //Agrega el Lang
            String val = Required.ToString().ToLower();
            if (Type != ValidationType.None)
            {
                val += ";" + Converter.EnumToString(Type);
            }
            output.AddAttribute("lang", val);


            base.AddAttributesToRender(output);
        }

        protected override void RenderContents(HtmlTextWriter output)
        {
            //AddAttributesToRender(output);
            base.RenderContents(output);
        }

        #region Miembros de IControlValidatable
        public void Validate()
        {
            throw new NotImplementedException();
        }
        #endregion


    }
}

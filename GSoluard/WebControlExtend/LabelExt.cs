﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GSoluard.WebControlExtend
{
    [ParseChildren(true)]
    [PersistChildren(true)]
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:LabelExt runat=server></{0}:LabelExt>")]
    public class LabelExt : Label 
    {

        protected override void OnInit(EventArgs e)
        {            
            base.OnInit(e);
        }

        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {

            base.AddAttributesToRender(writer);
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            //AddAttributesToRender(writer);
            writer.Write(Text);
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GSoluard.WebControlExtend
{
    interface IControlValidatable
    {
        void Validate();
        bool Required { get; set; }
        ValidationType Type { get; set; }
    }
}

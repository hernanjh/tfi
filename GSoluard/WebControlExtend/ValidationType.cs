﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GSoluard.WebControlExtend
{
    public enum ValidationType
    {
        [StringValue("none")]
        None = 0,
        [StringValue("numeric")]
        Numeric = 1,
        [StringValue("email")]
        Email = 2,
        [StringValue("alpha")]
        AlphaNumeric = 3,
        [StringValue("date")]
        Date = 4,
        [StringValue("url")]
        Url = 5,
        [StringValue("link")]
        Link = 6,
        [StringValue("entero")]
        Entero = 7,
        [StringValue("money")]
        Money = 8,
        [StringValue("cuitcuil")]
        CuitCuil = 9,
        [StringValue("expte")]
        Expediente = 10,
        [StringValue("ingreso")]
        Ingreso = 11,
        [StringValue("NumeracionDoc")]
        NumeracionDoc = 12,
        [StringValue("periodo")]
        Periodo = 13,
        [StringValue("telefono")]
        Telefono = 18,
        [StringValue("porcentaje")]
        Porcentaje = 14,
        [StringValue("nrotelefono")]
        NroTelefono = 15
    }

    public class StringValue : System.Attribute
    {
        private string _value;

        public StringValue(string value)
        {
            _value = value;
        }

        public string Value
        {
            get { return _value; }
        }

    }

    public static class Converter
    {
        public static string EnumToString(Enum value)
        {
            Type type = value.GetType();

            System.Reflection.FieldInfo fieldInfo = type.GetField(value.ToString());

            StringValue[] attribs = fieldInfo.GetCustomAttributes(typeof(StringValue), false) as StringValue[];

            return attribs.Length > 0 ? attribs[0].Value : null;
        }

    }
}

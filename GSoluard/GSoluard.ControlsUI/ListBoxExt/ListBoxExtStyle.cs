﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.WebControlExtend;
using System.Drawing;
using System.Web.UI.WebControls;
using GSoluard.WebControlExtend.Interfaces;

namespace GSoluard.ControlsUI
{
    public class ListBoxExtStyle : IListBoxExtSetting
    {
        public void ApplySetting(ListBoxExt txt)
        {
            txt.CssClass = "ui-widget ui-widget-content ui-corner-all";
        }
    }
}

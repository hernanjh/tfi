﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.WebControlExtend;
using System.Drawing;
using System.Web.UI.WebControls;

namespace GSoluard.ControlsUI
{
    public class GridViewExtBasic : IGridViewExtSetting
    {
        public void ApplySetting(GridViewExt gve)
        {
            gve.AutoGenerateColumns = false;                       
            gve.BackColor = Color.White;
            gve.Width = Unit.Percentage(100);
            gve.AllowPaging = true;
            gve.AllowSorting = true;
            gve.PageSize = 15;

            //Header
            gve.HeaderStyle.CssClass = "ui-widget-header";

            //Row
            gve.RowStyle.CssClass = "rowTable";

            //Pager
            gve.PagerSettings.Mode = PagerButtons.NextPreviousFirstLast;
            gve.PagerSettings.FirstPageImageUrl = "~/Styles/HerGoS-theme/images/icons/first.png";
            gve.PagerSettings.PreviousPageImageUrl = "~/Styles/HerGoS-theme/images/icons/previous.png";
            gve.PagerSettings.NextPageImageUrl = "~/Styles/HerGoS-theme/images/icons/next.png";
            gve.PagerSettings.LastPageImageUrl = "~/Styles/HerGoS-theme/images/icons/last.png";
            gve.PagerStyle.HorizontalAlign = HorizontalAlign.Center;
            gve.PagerStyle.CssClass = "ui-widget-content";

        }
    }
}

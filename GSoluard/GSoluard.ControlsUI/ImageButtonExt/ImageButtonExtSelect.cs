﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.WebControlExtend.Interfaces;
using GSoluard.WebControlExtend;

namespace GSoluard.ControlsUI
{
    public class ImageButtonExtSelect : IImageButtonExtSetting
    {

        public void ApplySetting(ImageButtonExt btne)
        {  
            btne.CssClass = "ui-state-default ui-corner-all";
            btne.ImageUrl = "/Styles/HerGoS-theme/images/icons/next.png";
            btne.ToolTip = "Seleccionar registro";
        }
    }
}

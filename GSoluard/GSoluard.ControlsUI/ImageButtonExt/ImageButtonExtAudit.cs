﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using GSoluard.WebControlExtend.Interfaces;
using GSoluard.WebControlExtend;

namespace GSoluard.ControlsUI
{
    public class ImageButtonExtAudit : IImageButtonExtSetting
    {
        public void ApplySetting(ImageButtonExt btne)
        {
            btne.CssClass = "ui-state-default ui-corner-all";
            btne.ImageUrl = "/Styles/HerGoS-theme/images/icons/lock.png";
            btne.ToolTip = "Ver auditoría";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.WebControlExtend;
using System.Drawing;
using System.Web.UI.WebControls;
using GSoluard.WebControlExtend.Interfaces;

namespace GSoluard.ControlsUI
{
    public class ButtonExtText : IButtonExtSetting
    {
        public void ApplySetting(ButtonExt btne)
        {
            btne.CssClass = "ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only";
        }
    }
}

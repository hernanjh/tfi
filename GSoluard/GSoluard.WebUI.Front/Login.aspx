﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginasMaster.Master" AutoEventWireup="true"
    CodeBehind="Login.aspx.cs" Inherits="GSoluard.WebUI.Front.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        .ui-widget-content {
            border: 1px solid #dddddd;
            background: #ffffff;
            color: #362b36;
            padding: 2px;
        }
    </style>
    <script type="text/javascript">

        $(document).ready(function () {
            $("body").keydown(function (event) {
                if (event.which == 13) { //Enter
                    event.preventDefault();
                    $("#<%=cmdLogin.ClientID %>").click();
                }
            });
        });

        $(function () {
            $("#dialog-login").dialog({
                autoOpen: true,
                draggable: false,
                resizable: false,
                modal: false,
                height: 600,
                width: 1000,
                closeOnEscape: false,
                dialogClass: "dialog-no-titlebar dialog-no-close dialog-WithDropShadow",
                close: function (event, ui) {

                },
                open: function (event, ui) {
                    $(this).parent().appendTo("form");
                }
            }).dialog();



            $(window).resize(function () {
                $("#dialog-login").dialog("option", "position", "center");
            });

            $("#<%=txtUser.ClientID %>").focus();
        });

    </script>
    <div id="wrapper" class="ui-widget-overlay "></div>
    <div id="dialog-login"  class="ui-front">

        <asp:Table runat="server" Width="100%">
            <asp:TableRow>
                <asp:TableCell RowSpan="8" HorizontalAlign="Center" Width="500px">  <img alt="Grupo Soluard" src="/Styles/HerGoS-theme/images/reparación-raee.jpg" class="ui-corner-all" style="width:650px; height:570px"/> </asp:TableCell>
                <asp:TableCell ColumnSpan="2"><br /><br /><br /></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>              
                <asp:TableCell ColumnSpan="2" HorizontalAlign="Center"> <br /><br />               
                    <img alt="Grupo Soluard" src="/Styles/HerGoS-theme/images/Logo_Completo_Grupo_Soluard.png" class="ui-corner-all" width="150px"/><br /><br />
                </asp:TableCell>
            </asp:TableRow>

            <asp:TableRow>
                <asp:TableCell>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Usuario:</asp:TableCell>
                <asp:TableCell>
                    <wce:TextBoxExt runat="server" ID="txtUser" Required="true" MaxLength="50" Type="Email"></wce:TextBoxExt></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Password:</asp:TableCell>
                <asp:TableCell>
                    <wce:TextBoxExt runat="server" ID="txtPassword" Required="true" TextMode="Password" MaxLength="250"></wce:TextBoxExt></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                    <asp:CheckBox id="chkPersistLogin" runat="server" Text="Recordarme" Checked="true" /></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">                
                <div id="ErrorMessage" runat="server" style=" color:#cd0a0a" >&nbsp;</div>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2" HorizontalAlign="Center"><br />
                    <wce:ButtonExt runat="server" ID="cmdLogin" OnClientClick="return validarForm();" OnClick="ProcessLogin" Text="Ingresar" UseSubmitBehavior="true" /></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2"><br /><br /><br /><br /><br /><br /></asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>
</asp:Content>

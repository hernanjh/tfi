﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TGrid.ascx.cs" Inherits="GSoluard.WebUI.Front.TGrid" %>
<div id="GridLoading" class="ui-widget-overlay " style="display:none">
<div style="position:relative; top:50%; left:50%;" class="ui-widget" />
<img src="/Styles/HerGoS-theme/images/ajax_loader.gif" style="margin-left:-24px; margin-top:-24px;"/>
<br />      
<div  style="margin-left:-32px;">Actualizando</div>
</div>
</div>

<div id="DivGrilla" runat="server">
    <asp:HiddenField EnableViewState="false" runat="server" ID="SortExpression" Value="" />
    <asp:HiddenField EnableViewState="true" runat="server" ID="SortDirection" Value="" />
    <asp:HiddenField EnableViewState="false" runat="server" ID="PageIndex" Value="1" />    
    <div style="width:100%;">
    <asp:table Width="100%" runat="server" ID="Theader" CssClass="TablaListaGrid" CellPadding="0" CellSpacing="0" BorderWidth="0"></asp:table>
    <div id="Contenedor" runat="server" style="width:100%">
    <asp:Table Width="100%"  runat="server" ID="TGrilla" CellPadding="2" CellSpacing="0" BorderStyle="None" CssClass="TablaListaGrid"></asp:Table>
    </div>
    </div>
    <table width="100%" id="trPaginacion" border="0" cellspacing="0" cellpadding="0" class="TablaContenidoGris" runat="server">
      <tr>
        <td width="95px"><asp:Button ID="btFIRST" CommandName="FIRST" Text="Primera" CssClass="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" runat="server" OnClick="NavigationButtonClick" OnClientClick="Grid_Loading()"/> </td>
        <td width="95px"><asp:Button ID="btPREVIOUS" CommandName="PREVIOUS" style="text-align:left;" Text="Anterior" CssClass="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" runat="server" OnClick="NavigationButtonClick" OnClientClick="Grid_Loading()" /></td>
        
        <td id="tdPaginadorNumerico" align="center" class="BotonImagen TextoBoton" runat="server">
            Página <asp:Label ID="lblCurPage" runat="server"></asp:Label> de <asp:Label ID="lblTotPages" runat="server"></asp:Label>
        </td>
        
        <td width="95px"><asp:Button ID="btNEXT" CommandName="NEXT" Text="Siguiente" CssClass="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" runat="server" OnClick="NavigationButtonClick" OnClientClick="Grid_Loading()" /></td>
        <td width="95px"><asp:Button ID="btLAST" CommandName="LAST" Text="Ultima" CssClass="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" runat="server" OnClick="NavigationButtonClick" OnClientClick="Grid_Loading()" /></td>
      </tr> 
     </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="TablaContenidoGris"  id="trTotales" runat="server">      
      <tr class="TextoFormuNegro" id="trTotalesPagina" runat="server">
          <td align="right"><b>Total P&aacute;gina:</b></td>
          <td align="center" style="width:50px"><b><asp:Label ID="lblTotalRegPag" runat="server"></asp:Label></b></td>
      </tr>      
      <tr class="TextoFormuNegro">
          <td align="right"><b>Total Consulta: </b></td>
          <td align="center"><b><asp:Label ID="lblTotalRegCon" runat="server"></asp:Label></b></td>
      </tr>
    </table>
    <asp:Button ID="Recarga"  runat="server" OnClick="Grilla_reload" UseSubmitBehavior="false" />
    
</div>
   
    
<script type="text/javascript" language="javascript">
    document.onkeydown = function (e) {
        e = e || window.event;
        if (!document) return false;
        var kCode = (document.all) ? e.keyCode : e.which;
        //Capture backspace
        if (kCode == 8 || kCode == 37 || kCode == 39) {//backspace o izquierda o derecha
            var obj = e.target ? e.target : e.srcElement;
            if (!(obj.tagName == "INPUT" || obj.tagName == "TEXTAREA")) {
                if (e.preventDefault) e.preventDefault();
                e.returnValue = false;
            }
        }
    };
        
    try {
        FreezeGridViewHeader('<%=TGrilla.ClientID%>','<%=Theader.ClientID%>');    
    } catch (ex) {
        
    }
    function Grid_Loading() {
        //document.getElementById('GridLoadingBack').style.display = 'block';
        document.getElementById('GridLoading').style.display = 'block';
        return true;
    }
    function ReloadGrid() {
        Grid_Loading();
        $("#<%=Recarga.ClientID %>").click();
        //__doPostBack('<%=Recarga.ClientID%>', '');
    }

    window.onbeforeunload = function () {
        Grid_Loading();
    };

</script>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GSoluard.Administration.Entities;
using GSoluard.Administration.Engine.Parametrizacion;
using GSoluard.ControlsUI;
using GSoluard.Administration.Engine.Procesos;
using GSoluard.WebControlExtend;
using GSoluard.Interfaces;
using System.Globalization;
using System.Threading;

namespace GSoluard.WebUI.Front.Parametrizacion
{
    public partial class Pedidos_Pagar : PageBase
    {

        #region Propiedades

        public Pedido Pedido
        {
            get
            {
                if (ViewState["Pedido"] != null)
                    return (Pedido)ViewState["Pedido"];
                return null;
            }
            set
            {
                ViewState["Pedido"] = value;
            }

        }

        public int ContadorDetalles
        {
            get
            {
                if (ViewState["ContadorDetalles"] != null)
                    return (int)ViewState["ContadorDetalles"];
                return 0;
            }
            set
            {
                ViewState["ContadorDetalles"] = value;
            }
        }

        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("es-AR");

            if (!Page.IsPostBack)
            {

                Pedido = GestorPedidos.ObtenerPendiente(HGSContext.Usuario.OID);

                float total = (Pedido.PedidoDetalles).AsEnumerable().Sum(row => row.Precio * row.Cantidad);

                htmlprecio.InnerHtml = "Monto Total: <b> $ " + total.ToString("N2") + "</b>";


            }

        }


        protected void btnPagar_OnClick(object sender, EventArgs e)
        {

            Pedido.Estado = 2;
            Pedido.FormaPago = new FormaPago { OID = 2 };
            GestorPedidos.Guardar(Pedido);
            Response.Redirect("/Procesos/Pedidos/Pedidos_Pagado.aspx?id=" + Pedido.OID.ToString());
           

        }

    }
}
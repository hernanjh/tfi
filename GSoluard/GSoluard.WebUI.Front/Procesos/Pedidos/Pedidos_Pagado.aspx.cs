﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GSoluard.Administration.Entities;
using GSoluard.Administration.Engine.Parametrizacion;
using GSoluard.ControlsUI;
using GSoluard.Administration.Engine.Procesos;
using GSoluard.WebControlExtend;
using GSoluard.Interfaces;
using System.Globalization;
using System.Threading;

namespace GSoluard.WebUI.Front.Parametrizacion
{
    public partial class Pedidos_Pagado : PageBase
    {

        public int id
        {
            get
            {
                if (ViewState["id"] != null)
                    return (int)ViewState["id"];
                return 0;
            }
            set
            {
                ViewState["id"] = value;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("es-AR");

            if (!Page.IsPostBack)
            {

                if (Request.QueryString["id"] != null )
                {
                    id = Convert.ToInt32(Request.QueryString["id"]);
                }
                else
                {
                    Response.Redirect("/Procesos/Productos/Productos_Principal.aspx");
                }

            }

        }

    }
}
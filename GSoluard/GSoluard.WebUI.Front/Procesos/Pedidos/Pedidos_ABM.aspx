﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginasMaster.Master" AutoEventWireup="true" CodeBehind="Pedidos_ABM.aspx.cs" Inherits="GSoluard.WebUI.Front.Parametrizacion.Pedidos_ABM" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<script type ="text/javascript">
    $(function () {

        $("#ac").accordion("option", "active", 0);


        Globalize.culture('de-DE');

        $('#<%=txtFecha.ClientID%>').live('blur', function () {
            ValidarFechas();
        })

    });

    function CerrarPopup() {
        $('#modificar_div').dialog('close');
        $('#modificar_div').remove();
    }
        
    function DialogDetalle() {
        $("#dialog-detalle").dialog({
            resizable: false,
            height: 250,
            width: 460,
            modal: false,
            open: function (event, ui) {
                $(this).parent().appendTo("form");
            }
        });
        return false;
    }

    function LimpiarDetalle() {
        $("#<%=txtCantidad.ClientID %>").val(0);        

    }

</script>

   <div class="formRegistro ui-widget">	
        <div class="filaCampo">
			<div class="etiqueta">Fecha : </div>
			<div class="campo"> <wce:TextBoxExt  ID="txtFecha" runat="server" ReadOnly="true"  Required="true" Width="100px"></wce:TextBoxExt></div>
        </div> 
       <br />
		<div class="filaCampo">
			<div class="etiqueta">Número  : </div>
			<div class="campo"> <wce:TextBoxExt  ID="txtNumero" runat="server" ReadOnly="true"></wce:TextBoxExt></div>
		</div>
       <br />
        <div class="filaCampo">
			<div class="etiqueta">Forma de Pago  : </div>
			<div class="campo">   <wce:TextBoxExt  ID="txtFormaPago" runat="server"  ReadOnly="true"></wce:TextBoxExt>                
            </div>
		</div>
       <br />
        <div class="filaCampo">
			<div class="etiqueta">Usuario  : </div>
			<div class="campo">   <wce:TextBoxExt  ID="txtUsuario" runat="server"  ReadOnly="true"></wce:TextBoxExt>                
            </div>
		</div>
       <br />
        <div class="filaCampo">
			<div class="etiqueta">Estado  : </div>
			<div class="campo">   <wce:TextBoxExt  ID="txtEstado" runat="server"  ReadOnly="true"></wce:TextBoxExt>                
            </div>
		</div>
       <br /><br />
         <div id="detalle"class="accordion">
         <h3> Detalle</h3>
         <div>

        <asp:UpdatePanel runat="server" ID="UpdatePanelDetalle" UpdateMode="Conditional">
        <ContentTemplate>
          <wce:GridViewExt ID="GridDetalle" runat="server" AutoGenerateColumns="false"
          DataKeyNames="OID" OnRowCommand="GridDetalle_RowCommand"
          OnRowDataBound="GridDetalle_RowDataBound">
          <Columns>
            <asp:BoundField DataField="Cantidad" HeaderText="Cantidad" ItemStyle-Width="40px"  ItemStyle-HorizontalAlign="Center"/>
            <asp:BoundField DataField="Producto" HeaderText="Producto" ItemStyle-Width="120px"/>             
            <asp:BoundField DataField="Precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:C2}"/>  
            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="40px">
             <ItemTemplate>
                    <wce:ImageButtonExt ID="btnEditar" runat="server"
                     CommandName="Editar" CommandArgument="<%# ((GridViewRow)Container).RowIndex %>"></wce:ImageButtonExt>
               </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>  
           <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="40px">
                    <ItemTemplate>
                      <wce:ImageButtonExt ID="btnBorrar" runat="server" 
                       CommandName="Borrar" CommandArgument="<%# ((GridViewRow)Container).RowIndex %>">
                      </wce:ImageButtonExt>
                   </ItemTemplate>
                  <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
         </Columns>
            
        </wce:GridViewExt>
        </ContentTemplate>

        </asp:UpdatePanel>
        </div>
         
         </div>

       <br /><br />
       <div class="filaCampo">
         <wce:ButtonExt ID="btnGuardar" runat="server"  Text="Guardar" 
            onclick="btnGuardar_Click" OnClientClick="return validarForm();" />
         <wce:ButtonExt ID="btnCerrar" runat="server"  Text="Cerrar" OnClientClick="parent.CerrarPopup(); return false;"  />
       </div>
    </div>

<div id="dialog-detalle" title="Detalle" style="display: none;">
         <asp:UpdatePanel ID="UpdatePanelABMDetalle" runat="server" UpdateMode="Conditional">
         <ContentTemplate>
                <asp:HiddenField ID="hiddetalle" runat="server" />

                 <br /> <br />
               <div class="filaCampo">
			        <div class="etiqueta">Cantidad : </div>
			        <div class="campo">
                        <wce:TextBoxExt ID="txtCantidad" runat="server"  MaxLength="5" Width="40px" Type="Entero" Required="true"></wce:TextBoxExt>
			        </div>
		        </div>
               <br />
                <div class="filaCampo">
			        <div class="etiqueta">Producto  : </div>
			        <div class="campo">   
                       <wce:TextBoxExt  ID="txtProducto" runat="server"  ReadOnly="true"></wce:TextBoxExt>  
                    </div>
		        </div><br />
		        <div class="filaCampo">
			        <div class="etiqueta">Precio  : </div>
			        <div class="campo"> $ <wce:TextBoxExt  ID="txtPrecio" runat="server"  Width="100px" Type="Money" Style="text-align: right;" ReadOnly="true" ></wce:TextBoxExt></div>
		        </div>               
              <br /> <br />
                                         
                    <wce:ButtonExt ID="btn_AgregarDetalle" runat="server" Text="Guardar"  OnClick = "btn_AgregarDetalle_Click"                   
                    OnClientClick="return validarDialog('dialog-detalle');"/>

                   <wce:ButtonExt ID="btn_Cerrar_dialog_detalle" runat="server" Text="Cerrar" 
                   OnClientClick="$('#dialog-detalle').dialog('close'); return false;" />
                   </ContentTemplate>
            </asp:UpdatePanel>
    </div>

<script type="text/javascript">
    //activa la validacion en el onblur de los campos
    validarFormBlurActivo();
    $("#<%=txtCantidad.ClientID %>").spinner();
</script>



</asp:Content>

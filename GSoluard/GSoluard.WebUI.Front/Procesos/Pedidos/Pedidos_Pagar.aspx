﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginasMaster.Master" AutoEventWireup="true" CodeBehind="Pedidos_Pagar.aspx.cs" Inherits="GSoluard.WebUI.Front.Parametrizacion.Pedidos_Pagar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<style>


        form {
            max-width: 400px;
            padding-top:50px;
            margin: 0 auto;
        }

        label, input, select {
            display: block;
            margin-bottom: 10px;
        }

        input.ui-button {
            padding: 10px 20px;
        }

        .ui-button-green {
            background-color: #4CAF50;
            color: white;
            padding: 10px 20px;
            border: none;
            border-radius: 5px;
            width:100px;
        }

        .ui-button-red {
            background-color: #f44336;
            color: white;
            padding: 10px 20px;
            border: none;
            border-radius: 5px;
            width:100px;
        }

        .error {
            color: red;
            display: none;
        }

    .producto-precio {
      font-size: 22px;
      color: #17834c; /* Color del precio */
      margin: 10px 0;
      text-align:right;
    }

</style>

 <script>
     $(document).ready(function () {
         $("#formularioPago").on("submit", function (event) {
             event.preventDefault();
             if (validarPago()) {
                 // Aquí debes implementar la lógica de procesamiento de pago
                 alert("¡Pago exitoso!");
             }
         });

         $('[name="btnCancelar"]').click(function (e) {
             event.preventDefault(e);
             redirect('/Procesos/Pedidos/Pedidos_Pendientes.aspx');
         });

         function validarPago() {

             $(".error").hide();

             var tipoTarjeta = $("#tipoTarjeta").val();
             var cuotas = $("#cuotas").val();
             var nombreApellido = $("#nombreApellido").val();
             var numeroDocumento = $("#numeroDocumento").val();
             var numeroTarjeta = $("#numeroTarjeta").val();
             var vencimiento = $("#vencimiento").val();
             var codigoSeguridad = $("#codigoSeguridad").val();

             var valid = true;

             if (!tipoTarjeta) {
                 $("#tipoTarjeta + .error").show();
                 valid = false;
             }
             if (!cuotas) {
                 $("#cuotas + .error").show();
                 valid = false;
             }
             if (!nombreApellido) {
                 $("#nombreApellido + .error").show();
                 valid = false;
             }
             if (!numeroDocumento) {
                 $("#numeroDocumento + .error").show();
                 valid = false;
             }
             if (!numeroTarjeta) {
                 $("#numeroTarjeta + .error").show();
                 valid = false;
             }
             if (!vencimiento) {
                 $("#vencimiento + .error").show();
                 valid = false;
             }
             if (!codigoSeguridad) {
                 $("#codigoSeguridad + .error").show();
                 valid = false;
             }

             return valid;
         }
     });
 </script>
        <div class="ui-widget-header ui-corner-top">
            <h2>Realizar el Pago</h2>
        </div>
    <br /> <br />
     <p id="htmlprecio" runat="server" class="producto-precio">Total: $ 125.251</p>

    <br /> <br />

<label for="tipoTarjeta">Tipo Tarjeta:</label>
        <select id="tipoTarjeta" required>
            <option value="">Selecciona un tipo de tarjeta</option>
            <option value="Visa">Visa</option>
            <option value="Mastercard">Mastercard</option>
            <option value="AMEX">AMEX</option>
        </select>
        
        <label for="cuotas">Cuotas:</label>
        <select id="cuotas" required>
            <option value="">Selecciona la cantidad de cuotas</option>
            <option value="1">1</option>
            <option value="3">3</option>
            <option value="6">6</option>
        </select>
        
        <label for="nombreApellido">Nombre y Apellido:</label>
        <input type="text" id="nombreApellido" required>
        
        <label for="numeroDocumento">Número de Documento:</label>
        <input type="text" id="numeroDocumento" required pattern="[0-9]{7,8}">
        <span class="error">Formato incorrecto (7-8 dígitos)</span>
        
        <label for="numeroTarjeta">Número de Tarjeta:</label>
        <input type="text" id="numeroTarjeta" required pattern="[0-9]{16}">
        <span class="error">Formato incorrecto (16 dígitos)</span>
        
        <label for="vencimiento">Vencimiento (MM/AAAA):</label>
        <input type="text" id="vencimiento" required pattern="(0[1-9]|1[0-2])\/(20\d{2})">
        <span class="error">Formato incorrecto (MM/AAAA)</span>
        
        <label for="codigoSeguridad">Código de Seguridad (3 dígitos):</label>
        <input type="text" id="codigoSeguridad" required pattern="[0-9]{3}">
        <span class="error">Formato incorrecto (3 dígitos)</span>
        <br />
    <center>

        <input name="btnCancelar" value="Cancelar" id="btnCancelar" class="ui-button ui-widget ui-corner-all ui-button-red" />

        <asp:Button ID="btnPagar" CssClass="ui-button ui-widget ui-corner-all ui-button-green" Text="Pagar" runat="server" OnClick="btnPagar_OnClick" />
    </center>

</asp:Content>

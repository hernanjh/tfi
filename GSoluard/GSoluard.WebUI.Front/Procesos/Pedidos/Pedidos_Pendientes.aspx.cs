﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GSoluard.Administration.Entities;
using GSoluard.Administration.Engine.Parametrizacion;
using GSoluard.ControlsUI;
using GSoluard.Administration.Engine.Procesos;
using GSoluard.WebControlExtend;
using GSoluard.Interfaces;
using System.Globalization;
using System.Threading;

namespace GSoluard.WebUI.Front.Parametrizacion
{
    public partial class Pedidos_Pendientes : PageBase
    {

        #region Propiedades

        public Pedido Pedido
        {
            get
            {
                if (ViewState["Pedido"] != null)
                    return (Pedido)ViewState["Pedido"];
                return null;
            }
            set
            {
                ViewState["Pedido"] = value;
            }

        }

        public int ContadorDetalles
        {
            get
            {
                if (ViewState["ContadorDetalles"] != null)
                    return (int)ViewState["ContadorDetalles"];
                return 0;
            }
            set
            {
                ViewState["ContadorDetalles"] = value;
            }
        }

        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("es-AR");

            if (!Page.IsPostBack)
            {

                Pedido = GestorPedidos.ObtenerPendiente(HGSContext.Usuario.OID);


                if (Request.QueryString["idprod"] != null && Request.QueryString["cant"] != null)
                {
                    Producto prod = GestorProducto.Obtener(Convert.ToInt32(Request.QueryString["idprod"]));
                    var Cant = Convert.ToInt32(Request.QueryString["cant"]);

                    PedidoDetalle pd = new PedidoDetalle();
                    pd.Cantidad = Cant;
                    pd.Producto = prod;
                    pd.Precio = prod.Precio;

                    Pedido.PedidoDetalles.Add(pd);
                    GestorPedidos.Guardar(Pedido);
                }

                LoadGridDetalles();
                Initialize();

            }

        }

        void Initialize()
        {
            this.btnGuardar.Settings(new ButtonExtText());
            //this.txtFecha.Settings(new TextBoxExtStyle());

            this.GridDetalle.Settings(new GridViewExtBasic());


        }


        private void LoadGridDetalles()
        {
            GridDetalle.DataSource = GestorPedidoDetalle.Activos(Pedido);
            GridDetalle.DataBind();

        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {

            GestorPedidos.Guardar(Pedido);

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "closedialog", "parent.IntervaloCerrarPopup();", true);

        }

        protected void btnComprar_Click(object sender, EventArgs e)
        {

            GestorPedidos.Guardar(Pedido);

           

        }


        protected void btnCerrar_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "closedialog", "parent.CerrarPopup();", true);
        }


        protected void GridDetalle_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Borrar")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                int id = Convert.ToInt32(GridDetalle.DataKeys[index].Value);


                if (id > 0)
                {
                    PedidoDetalle detalle = Pedido.PedidoDetalles.Where(x => x.OID == id).FirstOrDefault();
                    detalle.Persistencia = EstadoPersistencia.DELETE;

                }
                else
                {
                    Pedido.PedidoDetalles.RemoveAt(index);

                }
                LoadGridDetalles();
            }

        }

        protected void GridDetalle_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                PedidoDetalle pedidodetalle = (PedidoDetalle)e.Row.DataItem;

                float total = pedidodetalle.Cantidad * pedidodetalle.Precio;

                Label lab = e.Row.FindControl("lblSubtotal") as Label;
                if (lab != null)
                {
                    lab.Text = total.ToString("C2");
                }

                ImageButtonExt Borrar = e.Row.FindControl("btnBorrar") as ImageButtonExt;
                if (Borrar != null)
                {
                    Borrar.Settings(new ImageButtonExtDelete());
                }

            }


        }
    }
}
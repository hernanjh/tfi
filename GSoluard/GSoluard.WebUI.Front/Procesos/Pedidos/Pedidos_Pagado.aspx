﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginasMaster.Master" AutoEventWireup="true" CodeBehind="Pedidos_Pagado.aspx.cs" Inherits="GSoluard.WebUI.Front.Parametrizacion.Pedidos_Pagado" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<style>


        body {
            font-family: Arial, sans-serif;
            text-align: center;
        }

        h1 {
            color: #4CAF50;
        }

        #botonVerFactura {
            background-color: #4CAF50;
            color: white;
            padding: 10px 20px;
            border: none;
            border-radius: 5px;
            text-decoration: none;
            display: inline-block;
        }

</style>

 <script>
     $(document).ready(function () {


     });

     function Imprimir() {

         LoadPageDialogSinTitulo("Pedido_Impresion.aspx?id=<%=id.ToString()%>", "Impresion de Factura", 1091, 550, "impresion_div");

     }

 </script>
    <br /><br /><br /><br /><br /><br />
    <h1>Pago Exitoso</h1>
    <p>Su pago ha sido procesado con éxito.</p>
    <a id="botonVerFactura" href="#" onclick="Imprimir()">Ver Factura</a>

</asp:Content>

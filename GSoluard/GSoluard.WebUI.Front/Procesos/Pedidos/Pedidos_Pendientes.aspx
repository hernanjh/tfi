﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginasMaster.Master" AutoEventWireup="true" CodeBehind="Pedidos_Pendientes.aspx.cs" Inherits="GSoluard.WebUI.Front.Parametrizacion.Pedidos_Pendientes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<script type ="text/javascript">
    $(function () {

        //$("#ac").accordion("option", "active", 0);


        Globalize.culture('de-DE');

        $('[name="btnComprar"]').click(function (e) {
            event.preventDefault(e);
            redirect('/Procesos/Pedidos/Pedidos_Pagar.aspx');
        });

    });

    function CerrarPopup() {
        $('#modificar_div').dialog('close');
        $('#modificar_div').remove();
    }
        
    function DialogDetalle() {
        $("#dialog-detalle").dialog({
            resizable: false,
            height: 250,
            width: 460,
            modal: false,
            open: function (event, ui) {
                $(this).parent().appendTo("form");
            }
        });
        return false;
    }



</script>

   <div class="formRegistro ui-widget">	
        <div class="ui-widget-header ui-corner-top">
            <h2>
                Mi Pedido</h2>
        </div>

         <div id="detalle"class="accordion">
         <h3> Detalle</h3>
         <div>

        <asp:UpdatePanel runat="server" ID="UpdatePanelDetalle" UpdateMode="Conditional">
        <ContentTemplate>
          <wce:GridViewExt ID="GridDetalle" runat="server" AutoGenerateColumns="false"
          DataKeyNames="OID" OnRowCommand="GridDetalle_RowCommand"
          OnRowDataBound="GridDetalle_RowDataBound">
          <Columns>
            <asp:BoundField DataField="Cantidad" HeaderText="Cantidad" ItemStyle-Width="100px"  ItemStyle-HorizontalAlign="Center"/>
            <asp:BoundField DataField="Producto" HeaderText="Producto" />             
            <asp:BoundField DataField="Precio" HeaderText="Precio" ItemStyle-Width="180px" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:C2}"/>  
                    <asp:TemplateField  ItemStyle-HorizontalAlign="Right" ItemStyle-Width="180px">
                        <HeaderTemplate>Subtotal</HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSubtotal" runat="server" ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
           <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="40px">
                    <ItemTemplate>
                      <wce:ImageButtonExt ID="btnBorrar" runat="server" 
                       CommandName="Borrar" CommandArgument="<%# ((GridViewRow)Container).RowIndex %>">
                      </wce:ImageButtonExt>
                   </ItemTemplate>
                  <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
         </Columns>
            
        </wce:GridViewExt>
        </ContentTemplate>

        </asp:UpdatePanel>
        </div>
         
         </div>

       <br /><br />
       <div class="filaCampo">
         <wce:ButtonExt ID="btnGuardar" runat="server"  Text="Actualizar" 
            onclick="btnGuardar_Click" OnClientClick="return validarForm();" />

            <button name="btnComprar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="padding: 5px">Realizar Pago</button>
       </div>
    </div>



<script type="text/javascript">
    //activa la validacion en el onblur de los campos
    validarFormBlurActivo();

</script>



</asp:Content>

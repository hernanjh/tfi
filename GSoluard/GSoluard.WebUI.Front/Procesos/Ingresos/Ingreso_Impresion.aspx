﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Ingreso_Impresion.aspx.cs" Inherits="GSoluard.WebUI.Front.Parametrizacion.Ingreso_Impresion" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/Styles/HerGoS-theme/jquery-ui.custom.css" rel="stylesheet" type="text/css" />
    <link href="/Styles/Site.css" rel="stylesheet" type="text/css" />
<style type="text/css">
        body
        {
            background-color: #FFFFFF;
            font-size: 11.0pt;
        }
        .border
        {
            border: 1px #000000 solid;
        }
        .moneda
        {
            text-align:right;
        }
    .style3
    {
        width: 105px;
    }
    
    </style>
<style type="text/css" media="print">
        .ocultar {display:none;}
</style>
</head>
<body>
    <form id="form1" runat="server">
    <div style="float:right" class="ocultar">
        <wce:ButtonExt ID="btnImprimir" runat="server" Text="Imprimir" OnClientClick="window.print(); return false;"/>
    </div>
     <table style="width: 100%; height: 301px; " border="0" cellspacing="5px" cellpadding="5px">
        <tr >
            <td align="center"  class="border"><img alt="Grupo Soluard" src="/Styles/HerGoS-theme/images/Logo_Completo_Grupo_Soluard.png" class="ui-corner-all" width="100px"/></td>
            <td align="center"  colspan="3" class="border">               
              <b> INGRESO DE MATERIA PRIMA</b>
            </td>
        </tr>         
        <tr >
            <td  class="" align="left" style="width:150px">Fecha:</td>
            <td class="" align="left"  colspan="3">
                <asp:Label ID="txtfecha" runat="server" Text="00/00/00"></asp:Label>
            </td>           
        </tr>
        <tr>
            <td  class="" align="left">Número:</td>
            <td  class="" align="left" colspan="3">
                <asp:Label ID="txtNumero" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left" class="">
                Proveedor</td>
            <td colspan="3"  align="left" class="">
                <asp:Label ID="txtProveedor" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr >
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="4" >
                <asp:GridView ID="GridReporte" runat="server" Width="100%" AutoGenerateColumns="False" CellPadding="5" CellSpacing="5" >
               <Columns>
                   <asp:BoundField DataField="Cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Center">
                   </asp:BoundField>
                 <asp:BoundField DataField="MateriaPrima" HeaderText="Materia Prima" ItemStyle-HorizontalAlign ="left" >
                   </asp:BoundField>
             </Columns>                
                </asp:GridView>


            </td>
        </tr>
     

    </table>


    </form>
</body>
</html>

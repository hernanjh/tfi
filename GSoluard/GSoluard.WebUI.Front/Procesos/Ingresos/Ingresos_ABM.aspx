﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginasMaster.Master" AutoEventWireup="true" CodeBehind="Ingresos_ABM.aspx.cs" Inherits="GSoluard.WebUI.Front.Parametrizacion.Ingresos_ABM" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<script type ="text/javascript">
    $(function () {

        $("#ac").accordion("option", "active", 0);

        //$("#<%=cboProveedor.ClientID %>").combobox();

        Globalize.culture('de-DE');

        $('#<%=txtFecha.ClientID%>').live('blur', function () {
            ValidarFechas();
        })

    });

    function CerrarPopup() {
        $('#modificar_div').dialog('close');
        $('#modificar_div').remove();
    }

    function DialogDetalle() {
        $("#dialog-detalle").dialog({
            resizable: false,
            height: 250,
            width: 460,
            modal: false,
            open: function (event, ui) {
                $(this).parent().appendTo("form");
            }
        });
        return false;
    }

    function LimpiarDetalle() {
        $("#<%=txtCantidad.ClientID %>").val(0);        

    }

    function ValidarFechas() {


        if (moment($('#<%=txtFecha.ClientID%>').val(), "DD/MM/YYYY") > moment('<%=DateTime.Now.ToShortDateString()%>',"DD/MM/YYYY") ) {
            addLabelError($('#<%=txtFecha.ClientID%>').attr('id'), "La fecha no puede ser mayor a la fecha actual");
            $(".errorlabel").parpadear();
            return false;
        }
        else {
            removeLabelError($('#<%=txtFecha.ClientID%>').attr('id'));
            return true;
        }

    }

    function validarFormulario() {

        if (validarForm() == true) {

            if (!ValidarFechas()) {
                return false
            }
            else {

                return true;
            }
        }
        else return validarForm();

    }

</script>

   <div class="formRegistro ui-widget">	
        <div class="filaCampo">
			<div class="etiqueta">Fecha : </div>
			<div class="campo"> <wce:TextBoxExt CssClass="fecha" ID="txtFecha" runat="server"  Type="Date" Required="true"></wce:TextBoxExt></div>
        </div> 
       <br />
		<div class="filaCampo">
			<div class="etiqueta">Número  : </div>
			<div class="campo"> <wce:TextBoxExt  ID="txtNumero" runat="server" Required="true" MaxLength="50"></wce:TextBoxExt></div>
		</div>
       <br />
        <div class="filaCampo">
			<div class="etiqueta">Proveedor  : </div>
			<div class="campo">   <wce:DropDownListExt ID="cboProveedor" runat="server" Required="true" >
                </wce:DropDownListExt>
            </div>
		</div>
       <br /><br />
         <div id="detalle"class="accordion">
         <h3> Detalle</h3>
         <div>
              <wce:ButtonExt ID="btnAgregar_Detalle" runat="server" Text="Agregar Detalle" 
              OnClientClick=" LimpiarDetalle(); return DialogDetalle();"/>
         <br />
        <asp:UpdatePanel runat="server" ID="UpdatePanelDetalle" UpdateMode="Conditional">
        <ContentTemplate>
          <wce:GridViewExt ID="GridDetalle" runat="server" AutoGenerateColumns="false"
          DataKeyNames="OID" OnRowCommand="GridDetalle_RowCommand"
          OnRowDataBound="GridDetalle_RowDataBound">
          <Columns>
            <asp:BoundField DataField="Cantidad" HeaderText="Cantidad" ItemStyle-Width="40px"  ItemStyle-HorizontalAlign="Center"/>
            <asp:BoundField DataField="MateriaPrima" HeaderText="Materia Prima" ItemStyle-Width="120px"/>             
            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="40px">
             <ItemTemplate>
                    <wce:ImageButtonExt ID="btnEditar" runat="server"
                     CommandName="Editar" CommandArgument="<%# ((GridViewRow)Container).RowIndex %>"></wce:ImageButtonExt>
               </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>  
           <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="40px">
                    <ItemTemplate>
                      <wce:ImageButtonExt ID="btnBorrar" runat="server" 
                       CommandName="Borrar" CommandArgument="<%# ((GridViewRow)Container).RowIndex %>">
                      </wce:ImageButtonExt>
                   </ItemTemplate>
                  <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
         </Columns>
            
        </wce:GridViewExt>
        </ContentTemplate>
           <Triggers>
              <asp:AsyncPostBackTrigger ControlID="btn_AgregarDetalle" EventName="Click" />
           </Triggers>

        </asp:UpdatePanel>
        </div>
         
         </div>

       <br /><br />
       <div class="filaCampo">
         <wce:ButtonExt ID="btnGuardar" runat="server"  Text="Guardar" 
            onclick="btnGuardar_Click" OnClientClick="return validarFormulario();" />
         <wce:ButtonExt ID="btnCerrar" runat="server"  Text="Cerrar" OnClientClick="parent.CerrarPopup(); return false;"  />
       </div>
    </div>

<div id="dialog-detalle" title="Detalle" style="display: none;">
         <asp:UpdatePanel ID="UpdatePanelABMDetalle" runat="server" UpdateMode="Conditional">
         <ContentTemplate>
                <asp:HiddenField ID="hiddetalle" runat="server" />

                 <br /> <br />
               <div class="filaCampo">
			        <div class="etiqueta">Cantidad : </div>
			        <div class="campo">
                        <wce:TextBoxExt ID="txtCantidad" runat="server"  MaxLength="5" Width="40px" Type="Entero" Required="true"></wce:TextBoxExt>
			        </div>
		        </div>
               <br />
                <div class="filaCampo">
			        <div class="etiqueta">Materia Prima  : </div>
			        <div class="campo">   
                        <wce:DropDownListExt ID="cboMateriaPrima" runat="server" Required="true" ></wce:DropDownListExt>
                    </div>
		        </div>
                 
              <br /> <br /><br /> <br />
                                         
                    <wce:ButtonExt ID="btn_AgregarDetalle" runat="server" Text="Guardar"  OnClick = "btn_AgregarDetalle_Click"                   
                    OnClientClick="return validarDialog('dialog-detalle');"/>

                   <wce:ButtonExt ID="btn_Cerrar_dialog_detalle" runat="server" Text="Cerrar" 
                   OnClientClick="$('#dialog-detalle').dialog('close'); return false;" />
                   </ContentTemplate>
            </asp:UpdatePanel>
    </div>

<script type="text/javascript">
    //activa la validacion en el onblur de los campos
    validarFormBlurActivo();
    $("#<%=txtCantidad.ClientID %>").spinner();
</script>



</asp:Content>

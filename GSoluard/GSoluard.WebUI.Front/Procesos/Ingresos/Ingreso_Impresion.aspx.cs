﻿using GSoluard.Administration.Entities;
using GSoluard.Administration.Engine.Parametrizacion;
using GSoluard.ControlsUI;
using GSoluard.Administration.Engine.Procesos;
using GSoluard.WebControlExtend;
using GSoluard.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Globalization;

namespace GSoluard.WebUI.Front.Parametrizacion
{
    public partial class Ingreso_Impresion : System.Web.UI.Page
    {
 
        protected void Page_Load(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("es-ES");
            this.btnImprimir.Settings(new ButtonExtText());

            if (Request.QueryString["id"] != null)
            {
                var Id = Convert.ToInt32(Request.QueryString["id"]);
                Ingreso Ingreso = GestorIngresos.Obtener(Id);
                

                this.txtfecha.Text = Ingreso.Fecha.Value.ToShortDateString();
                this.txtNumero.Text = Ingreso.Numero.ToString();
                this.txtProveedor.Text = Ingreso.Proveedor.ToString();

                GridReporte.DataSource = GestorIngresosDetalle.Activos(Ingreso);
                GridReporte.DataBind();
                
            }
        }

        protected void GridReporteTramos_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
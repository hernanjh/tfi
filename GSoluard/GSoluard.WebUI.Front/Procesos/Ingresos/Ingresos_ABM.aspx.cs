﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GSoluard.Administration.Entities;
using GSoluard.Administration.Engine.Parametrizacion;
using GSoluard.ControlsUI;
using GSoluard.Administration.Engine.Procesos;
using GSoluard.WebControlExtend;
using GSoluard.Interfaces;
using System.Globalization;
using System.Threading;

namespace GSoluard.WebUI.Front.Parametrizacion
{
    public partial class Ingresos_ABM : PageBase
    {

        #region Propiedades

        public Ingreso Ingreso
        {
            get
            {
                if (ViewState["Ingreso"] != null)
                    return (Ingreso)ViewState["Ingreso"];
                return null;
            }
            set
            {
                ViewState["Ingreso"] = value;
            }

        }

        public int ContadorDetalles
        {
            get
            {
                if (ViewState["ContadorDetalles"] != null)
                    return (int)ViewState["ContadorDetalles"];
                return 0;
            }
            set
            {
                ViewState["ContadorDetalles"] = value;
            }
        }

        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("es-ES");

            if (!Page.IsPostBack)
            {
                LoadCombos();
                if (Request.QueryString["id"] == null)
                {
                    Ingreso = new Ingreso();
                }
                else
                {
                    var Id = Convert.ToInt32(Request.QueryString["id"]);
                    Ingreso = GestorIngresos.Obtener(Id);
                    this.txtNumero.Text = Ingreso.Numero;
                    this.cboProveedor.SelectedValue = Ingreso.Proveedor.OID == 0 ? "0" : Ingreso.Proveedor.OID.ToString();
                    this.txtFecha.Text = Ingreso.Fecha.Value.ToShortDateString();
                    LoadGridDetalles();
                }
                Initialize();
            }

        }

        void Initialize()
        {
            this.btnGuardar.Settings(new ButtonExtText());
            this.btnCerrar.Settings(new ButtonExtText());
            this.txtNumero.Settings(new TextBoxExtStyle());
            //this.txtFecha.Settings(new TextBoxExtStyle());
            this.cboProveedor.Settings(new DropDownListExtStyle());
            this.GridDetalle.Settings(new GridViewExtBasic());
            this.btnAgregar_Detalle.Settings(new ButtonExtText());
            this.btn_AgregarDetalle.Settings(new ButtonExtText());
            this.btn_Cerrar_dialog_detalle.Settings(new ButtonExtText());
            this.cboMateriaPrima.Settings(new DropDownListExtStyle());

        }

        private void LoadCombos()
        {
            this.cboProveedor.DataSource = GestorProveedor.Consultar();
            cboProveedor.DataTextField = "RazonSocial";
            cboProveedor.DataValueField = "OID";
            cboProveedor.DataBind();
            cboProveedor.Items.Insert(0, new ListItem("--Seleccione-- ", "0"));

            this.cboMateriaPrima.DataSource = GestorMateriaPrima.Consultar();
            cboMateriaPrima.DataTextField = "Nombre";
            cboMateriaPrima.DataValueField = "OID";
            cboMateriaPrima.DataBind();
            cboMateriaPrima.Items.Insert(0, new ListItem("--Seleccione-- ", "0"));
            
        }

        private void LoadGridDetalles()
        {
            GridDetalle.DataSource = GestorIngresosDetalle.Activos(Ingreso);
            GridDetalle.DataBind();

        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {

            Ingreso.Numero = this.txtNumero.Text;
            Ingreso.Proveedor.OID = this.cboProveedor.SelectedValue.ToInteger() == 0 ? 0 : cboProveedor.SelectedValue.ToInteger();
            Ingreso.Fecha = Convert.ToDateTime(txtFecha.Text);
            GestorIngresos.Guardar(Ingreso);

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "closedialog", "parent.IntervaloCerrarPopup();", true);

        }


        protected void btnCerrar_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "closedialog", "parent.CerrarPopup();", true);
        }


        protected void GridDetalle_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Borrar")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                int id = Convert.ToInt32(GridDetalle.DataKeys[index].Value);


                if (id > 0)
                {
                    IngresoDetalle detalle = Ingreso.IngresoDetalles.Where(x => x.OID == id).FirstOrDefault();
                    detalle.Persistencia = EstadoPersistencia.DELETE;

                }
                else
                {
                    Ingreso.IngresoDetalles.RemoveAt(index);

                }
                LoadGridDetalles();
            }
            if (e.CommandName == "Editar")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                int oid = Convert.ToInt32(GridDetalle.DataKeys[index].Value);
                this.hiddetalle.Value = oid.ToString();
                IngresoDetalle detalle = Ingreso.IngresoDetalles.Find(y => y.OID == oid);
                this.txtCantidad.Text = detalle.Cantidad.ToString();
                this.cboMateriaPrima.SelectedValue = detalle.MateriaPrima.OID.ToString();

                UpdatePanelABMDetalle.Update();

                ScriptManager.RegisterStartupScript(Page, typeof(Page), "Edicion", "DialogDetalle();", true);
            }


        }

        protected void GridDetalle_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButtonExt Borrar = e.Row.FindControl("btnBorrar") as ImageButtonExt;
                if (Borrar != null)
                {
                    Borrar.Settings(new ImageButtonExtDelete());
                }


            }

            ImageButtonExt Editar = e.Row.FindControl("btnEditar") as ImageButtonExt;
            if (Editar != null)
            {

                Editar.Settings(new ImageButtonExtEdit());

            }

        }

        protected void btn_AgregarDetalle_Click(object sender, EventArgs e)
        {

            try
            {
                int idDetalle = this.hiddetalle.Value.ToInteger();
                if (idDetalle != 0)
                {
                    foreach (var item in Ingreso.IngresoDetalles.Where(x => x.OID == idDetalle))
                    {
                        item.Cantidad = this.txtCantidad.Text.ToInteger();
                        item.MateriaPrima = GestorMateriaPrima.Obtener(this.cboMateriaPrima.SelectedValue.ToInteger());
                    }

                    LoadGridDetalles();
                    hiddetalle.Value = "0";
                }
                else
                {
                    IngresoDetalle detalle = new IngresoDetalle();

                    detalle.Cantidad = this.txtCantidad.Text.ToInteger();
                    detalle.MateriaPrima = GestorMateriaPrima.Obtener(this.cboMateriaPrima.SelectedValue.ToInteger());
                    this.ContadorDetalles = ContadorDetalles - 1;
                    detalle.OID = ContadorDetalles;
                    Ingreso.IngresoDetalles.Add(detalle);
                    LoadGridDetalles();

                }
                this.cboMateriaPrima.SelectedValue = "0";
                this.txtCantidad.Text = string.Empty;

            }
            catch (Exception ex)
            { }

        }

    }
}
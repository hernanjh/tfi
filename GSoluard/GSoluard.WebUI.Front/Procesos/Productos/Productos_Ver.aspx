﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginasMaster.Master" AutoEventWireup="true" CodeBehind="Productos_Ver.aspx.cs" Inherits="GSoluard.WebUI.Front.Parametrizacion.Productos_Ver" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>


/* Producto individual */
.producto {
  border: 1px solid #ddd;
  margin: 10px;
  background-color: #fff;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1);
  transition: box-shadow 0.3s;
  width: 95%;
  height: 500px;
  
}

.producto:hover {
  box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.2);
}

.producto-imagen {
  padding: 10px;
  display: flex;
  justify-content: center;
  align-items: center;
}

.producto-imagen img {
  max-width: 100%;
  max-height: 300px;
  height: auto;
}

.producto-informacion {
  padding: 10px;
  width: 400px;
}

.producto h2 {
  font-size: 30px;
  margin: 0;
}

.producto-precio {
  font-size: 22px;
  color: #17834c; /* Color del precio */
  margin: 10px 0;
  text-align:right;
}

.boton-comprar {
  background-color: #ff9900; /* Color del botón */
  color: #fff;
  border: none;
  padding: 10px 20px;
  cursor: pointer;
  font-weight: bold;
  border-radius: 4px;
}
.boton-comprar:hover {
  background-color: #ffcc00; /* Color al pasar el cursor por encima del botón */
}

.boton-ver {
  background-color: #0072ff; /* Color del botón */
  color: #fff;
  border: none;
  padding: 10px 20px;
  cursor: pointer;
  font-weight: bold;
  border-radius: 4px;
}


.boton-ver:hover {
  background-color: #5e9deb; /* Color al pasar el cursor por encima del botón */
}


    </style>
  <script language="javascript" type="text/javascript">

      $(document).ready(function () {


      });

      function validarNumero() {
          var input = document.getElementById('txtCantidad');
          var valor = input.value;
          var maximo = parseInt(input.getAttribute('max'));

          if (valor > maximo) {
              input.value = maximo;
          }
      }


  </script>
    <br /><br /><br />

        <table id="productos" class="producto">
            <tr>
                <td style="width:100px">
                 </td>
                <td style="width:400px">
                        <div class="producto-imagen">
                            <img id="htmlimagen" runat="server" />
                        </div>
                </td>
                <td >
                        <div class="producto-informacion">
                            <h2 id="htmlnombre" runat="server"></h2> <br />
                            <p id="htmlprecio" runat="server" class="producto-precio"></p> <br />
                            <p id="htmldescripcion" runat="server"></p> <br /> <br />
                            <div style="text-align:right; width:90%">
                                Cantidad: <input type="number" id="txtCantidad"  runat="server" name="numero" min="1" max="50" width="40px" oninput="validarNumero()" value="1"/>

                                <asp:Button runat="server" ID="btnComprar" CssClass="boton-comprar" Text="Comprar" OnClick="ComprarOnClick" />
                            </div>                          
                        </div>
                </td>

            </tr>

</table>

</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GSoluard.Administration.Entities;
using GSoluard.Administration.Engine.Parametrizacion;
using GSoluard.ControlsUI;
using GSoluard.Administration.Engine.Procesos;
using GSoluard.WebControlExtend;
using GSoluard.Interfaces;
using System.Globalization;
using System.Threading;

namespace GSoluard.WebUI.Front.Parametrizacion
{
    public partial class Productos_ABM : PageBase
    {

        #region Propiedades

        public Producto Producto
        {
            get
            {
                if (ViewState["Producto"] != null)
                    return (Producto)ViewState["Producto"];
                return null;
            }
            set
            {
                ViewState["Producto"] = value;
            }

        }

        public int ContadorDetalles
        {
            get
            {
                if (ViewState["ContadorDetalles"] != null)
                    return (int)ViewState["ContadorDetalles"];
                return 0;
            }
            set
            {
                ViewState["ContadorDetalles"] = value;
            }
        }

        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("es-ES");

            if (!Page.IsPostBack)
            {
                LoadCombos();
                if (Request.QueryString["id"] == null)
                {
                    Producto = new Producto();
                }
                else
                {
                    var Id = Convert.ToInt32(Request.QueryString["id"]);
                    Producto = GestorProducto.Obtener(Id);
                    this.txtNombre.Text = Producto.Nombre;
                    this.txtDescripcion.Text = Producto.Descripcion;
                    this.cboCategoria.SelectedValue = Producto.Categoria.OID == 0 ? "0" : Producto.Categoria.OID.ToString();
                    this.txtStock.Text = Producto.Stock.ToString();
                    this.txtPrecio.Text = Producto.Precio.ToString("F2");
                    if (Producto.Imagen != null)
                        imagenControl.ImageUrl = $"data:image/jpeg;base64,{Convert.ToBase64String(Producto.Imagen)}";
                    LoadGridDetalles();
                }
                Initialize();
            }

        }

        void Initialize()
        {
            this.btnGuardar.Settings(new ButtonExtText());
            this.btnCerrar.Settings(new ButtonExtText());
            this.txtNombre.Settings(new TextBoxExtStyle());
            this.txtDescripcion.Settings(new TextBoxExtStyle());
            this.cboCategoria.Settings(new DropDownListExtStyle());
            this.txtPrecio.Settings(new TextBoxExtStyle());
            this.GridDetalle.Settings(new GridViewExtBasic());
            this.btnAgregar_Detalle.Settings(new ButtonExtText());
            this.btn_AgregarDetalle.Settings(new ButtonExtText());
            this.btn_Cerrar_dialog_detalle.Settings(new ButtonExtText());
            this.cboMateriaPrima.Settings(new DropDownListExtStyle());

        }

        private void LoadCombos()
        {
            this.cboCategoria.DataSource = GestorCategoria.Consultar();
            cboCategoria.DataTextField = "Descripcion";
            cboCategoria.DataValueField = "OID";
            cboCategoria.DataBind();
            cboCategoria.Items.Insert(0, new ListItem("--Seleccione-- ", "0"));

            this.cboMateriaPrima.DataSource = GestorMateriaPrima.Consultar();
            cboMateriaPrima.DataTextField = "Nombre";
            cboMateriaPrima.DataValueField = "OID";
            cboMateriaPrima.DataBind();
            cboMateriaPrima.Items.Insert(0, new ListItem("--Seleccione-- ", "0"));
            
        }

        private void LoadGridDetalles()
        {
            GridDetalle.DataSource = GestorProductoMateriaPrima.Activos(Producto);
            GridDetalle.DataBind();

        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {

            Producto.Nombre = this.txtNombre.Text;
            Producto.Descripcion = this.txtDescripcion.Text;
            Producto.Categoria.OID = this.cboCategoria.SelectedValue.ToInteger() == 0 ? 0 : cboCategoria.SelectedValue.ToInteger();
            Producto.Stock = txtStock.Text.ToInteger();
            Producto.Precio = txtPrecio.Text.ToFloat();
            if (FileImagen.HasFile)
            {
                Producto.Imagen = FileImagen.FileBytes;
            }
            GestorProducto.Guardar(Producto);

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "closedialog", "parent.IntervaloCerrarPopup();", true);

        }


        protected void btnCerrar_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "closedialog", "parent.CerrarPopup();", true);
        }


        protected void GridDetalle_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Borrar")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                int id = Convert.ToInt32(GridDetalle.DataKeys[index].Value);


                if (id > 0)
                {
                    ProductoMateriaPrima detalle = Producto.MateriasPrimas.Where(x => x.OID == id).FirstOrDefault();
                    detalle.Persistencia = EstadoPersistencia.DELETE;
                    
                }
                else
                {
                    Producto.MateriasPrimas.RemoveAt(index);

                }
                LoadGridDetalles();
            }
            if (e.CommandName == "Editar")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                int oid = Convert.ToInt32(GridDetalle.DataKeys[index].Value);
                this.hiddetalle.Value = oid.ToString();
                ProductoMateriaPrima detalle = Producto.MateriasPrimas.Find(y => y.OID == oid);
                this.txtCantidad.Text = detalle.Cantidad.ToString();
                this.cboMateriaPrima.SelectedValue = detalle.MateriaPrima.OID.ToString();

                UpdatePanelABMDetalle.Update();

                ScriptManager.RegisterStartupScript(Page, typeof(Page), "Edicion", "DialogDetalle();", true);
            }


        }

        protected void GridDetalle_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButtonExt Borrar = e.Row.FindControl("btnBorrar") as ImageButtonExt;
                if (Borrar != null)
                {
                    Borrar.Settings(new ImageButtonExtDelete());
                }


            }

            ImageButtonExt Editar = e.Row.FindControl("btnEditar") as ImageButtonExt;
            if (Editar != null)
            {

                Editar.Settings(new ImageButtonExtEdit());

            }

        }

        protected void btn_AgregarDetalle_Click(object sender, EventArgs e)
        {

            try
            {
                int idDetalle = this.hiddetalle.Value.ToInteger();
                if (idDetalle != 0)
                {
                    foreach (var item in Producto.MateriasPrimas.Where(x => x.OID == idDetalle))
                    {
                        item.Cantidad = this.txtCantidad.Text.ToInteger();
                        item.MateriaPrima = GestorMateriaPrima.Obtener(this.cboMateriaPrima.SelectedValue.ToInteger());
                    }

                    LoadGridDetalles();
                    hiddetalle.Value = "0";
                }
                else
                {
                    ProductoMateriaPrima detalle = new ProductoMateriaPrima();

                    detalle.Cantidad = this.txtCantidad.Text.ToInteger();
                    detalle.MateriaPrima = GestorMateriaPrima.Obtener(this.cboMateriaPrima.SelectedValue.ToInteger());
                    this.ContadorDetalles = ContadorDetalles - 1;
                    detalle.OID = ContadorDetalles;
                    Producto.MateriasPrimas.Add(detalle);
                    LoadGridDetalles();

                }
                this.cboMateriaPrima.SelectedValue = "0";
                this.txtCantidad.Text = string.Empty;

            }
            catch (Exception ex)
            { }

        }

    }
}
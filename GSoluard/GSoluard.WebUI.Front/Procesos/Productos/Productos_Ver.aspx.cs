﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GSoluard.Administration.Entities;
using GSoluard.ControlsUI;
using GSoluard.Administration.Engine.Parametrizacion;
using GSoluard.WebControlExtend;
using GSoluard.Administration.Engine.Procesos;
using System.Globalization;
using System.Threading;

namespace GSoluard.WebUI.Front.Parametrizacion
{
    public partial class Productos_Ver :  PageBase
    {
       
        #region Propiedades
        public Producto Producto
        {
            get
            {
                if (ViewState["Producto"] != null)
                    return (Producto)ViewState["Producto"];
                return null;
            }
            set
            {
                ViewState["Producto"] = value;
            }

        }
        public int paramid
        {
            get
            {
                if (ViewState["paramid"] != null)
                    return (int)ViewState["paramid"];
                return 0;
            }
            set
            {
                ViewState["paramid"] = value; 
            }

        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("es-AR");
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    paramid = Convert.ToInt32(Request.QueryString["id"]);
                }
                else
                {
                    Response.Redirect("/Procesos/Productos/Productos_Principal.aspx");
                }
                Initialize();
                fillPage();
            }
        }

        void Initialize()
        {
            Producto = GestorProducto.Obtener(paramid);

        }      
        void fillPage()
        {
            htmlimagen.Attributes["src"] = "data:image/jpeg;base64," + Convert.ToBase64String(Producto.Imagen);
            htmlimagen.Attributes["alt"] = Producto.Nombre;
            htmlnombre.InnerText = Producto.Nombre;
            htmlprecio.InnerHtml = "Precio: <b>" + Producto.Precio.ToString("N2") + "</b>";
            htmldescripcion.InnerText = Producto.Descripcion;
        }

        protected void ComprarOnClick(object sender, EventArgs e)
        {
            Response.Redirect("/Procesos/Pedidos/Pedidos_Pendientes.aspx?idprod=" + Producto.OID.ToString() + "&cant=" + txtCantidad.Value);
        }

    }
}
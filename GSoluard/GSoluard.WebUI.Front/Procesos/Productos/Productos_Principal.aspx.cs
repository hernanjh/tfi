﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GSoluard.Administration.Entities;
using GSoluard.ControlsUI;
using GSoluard.Administration.Engine.Parametrizacion;
using GSoluard.WebControlExtend;
using GSoluard.Administration.Engine.Procesos;
using System.Globalization;
using System.Threading;

namespace GSoluard.WebUI.Front.Parametrizacion
{
    public partial class Productos_Principal :  PageBase
    {
       
        #region Propiedades
        public List<Producto> Productos
        {
            get
            {
                if (ViewState["Producto"] != null)
                    return (List<Producto>)ViewState["Producto"];
                return null;
            }
            set
            {
                ViewState["Producto"] = value;
            }

        }
        public string txt
        {
            get
            {
                if (ViewState["txt"] != null)
                    return (string)ViewState["txt"];
                return null;
            }
            set
            {
                ViewState["txt"] = value; 
            }

        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("es-AR");
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["txt"] != null)
                {
                    txt = Request.QueryString["txt"];
                }

                Initialize();
                LoadGrid(false);
            }
        }

        void Initialize()
        {

        }      

        private void LoadGrid(bool cache)
        {

            if (!cache)
                Productos = GestorProducto.Consultar();

            List<Producto> filtroResult = null;

            if (!string.IsNullOrEmpty(txt))
            {
                filtroResult = Productos.FindAll(FindProductos);
            }

            if (filtroResult == null) filtroResult = Productos;

            RepeaterProductos.DataSource = filtroResult;
            RepeaterProductos.DataBind();

        }

        private bool FindProductos(Producto obj)
        {
            if (obj.Nombre.ToLower().Contains(txt.ToLower()) || obj.Descripcion.ToLower().Contains(txt.ToLower()))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
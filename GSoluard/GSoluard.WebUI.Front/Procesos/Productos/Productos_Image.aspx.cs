﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GSoluard.Administration.Entities;
using GSoluard.Administration.Engine.Parametrizacion;
using GSoluard.ControlsUI;
using GSoluard.Administration.Engine.Procesos;
using GSoluard.WebControlExtend;
using GSoluard.Interfaces;
using System.Globalization;
using System.Threading;

namespace GSoluard.WebUI.Front.Parametrizacion
{
    public partial class Productos_Image : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                
                if (Request.QueryString["id"] == null)
                {
                    Response.Write("La imagen no fue encontrada.");
                }
                else
                {
                    var Id = Convert.ToInt32(Request.QueryString["id"]);
                    Producto prd = GestorProducto.Obtener(Id);
                    if (prd != null)
                    {
                        byte[] imagen = prd.Imagen;
                        if (imagen != null)
                        {
                            Response.Clear();
                            Response.Buffer = true;
                            Response.ContentType = "image/jpeg";
                            Response.BinaryWrite(imagen);
                            Response.End();
                        }
                        else { Response.Write("La imagen no fue encontrada."); }
                    }
                    else { Response.Write("La imagen no fue encontrada."); }
                }
                
            }

        }
    }
}
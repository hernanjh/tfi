﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginasMaster.Master" AutoEventWireup="true" CodeBehind="Productos_Principal.aspx.cs" Inherits="GSoluard.WebUI.Front.Parametrizacion.Productos_Principal" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>

.productos-container {
  display: flex;
  flex-wrap: wrap; /* Cambiar a wrap para permitir que los productos pasen al renglón de abajo */
  /*justify-content: space-between;*/
  overflow-x: auto; 
  padding: 10px;

}

/* Producto individual */
.producto {
  border: 1px solid #ddd;
  margin: 10px;
  background-color: #fff;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1);
  transition: box-shadow 0.3s;
  flex: 0 0 300px;
  display: block !important; /* Anular el display:block del elemento padre */
}

.producto:hover {
  box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.2);
}

.producto-imagen {
  padding: 10px;
  display: flex;
  justify-content: center;
  align-items: center;
}

.producto-imagen img {
  max-width: 100%;
  max-height: 150px;
  height: auto;
}

.producto-informacion {
  padding: 10px;
}

.producto h3 {
  font-size: 18px;
  margin: 0;
}

.producto-precio {
  font-size: 22px;
  color: #17834c; /* Color del precio */
  margin: 10px 0;
  text-align:right;
}

.boton-comprar {
  background-color: #ff9900; /* Color del botón */
  color: #fff;
  border: none;
  padding: 10px 20px;
  cursor: pointer;
  font-weight: bold;
  border-radius: 4px;
}
.boton-comprar:hover {
  background-color: #ffcc00; /* Color al pasar el cursor por encima del botón */
}

.boton-ver {
  background-color: #0072ff; /* Color del botón */
  color: #fff;
  border: none;
  padding: 10px 20px;
  cursor: pointer;
  font-weight: bold;
  border-radius: 4px;
}


.boton-ver:hover {
  background-color: #5e9deb; /* Color al pasar el cursor por encima del botón */
}


    </style>
  <script language="javascript" type="text/javascript">

      $(document).ready(function () {

          $('[name="btnVer"]').click(function (e) {
              event.preventDefault(e);
              var id = $(this).data('id');
              redirect('/Procesos/Productos/Productos_Ver.aspx?id=' + id);
          });

          $('[name="btnComprar"]').click(function (e) {
              event.preventDefault(e);
              var id = $(this).data('id');
              redirect('/Procesos/Pedidos/Pedidos_Pendientes.aspx?idprod=' + id + '&cant=1');
          });

      });



  </script>
    <br /><br /><br />
    <div id="productos" class="productos-container">
        <asp:Repeater ID="RepeaterProductos" runat="server">
            <ItemTemplate>
                <div class="producto">
                    <div class="producto-imagen">
                        <img src="data:image/jpeg;base64,<%#Convert.ToBase64String((byte[])Eval("Imagen"))%>" alt='<%# Eval("Nombre") %>' />
                    </div>
                    <div class="producto-informacion">
                        <h3><%# Eval("Nombre") %></h3>
                        <p class="producto-precio"><b>$<%# Eval("Precio", "{0:N2}") %></b></p>
                        <p><%# Eval("Descripcion") %></p>
                        <div style="text-align:center; width:90%">
                            <button name="btnVer" class="boton-ver" data-id="<%# Eval("OID") %>">Ver</button>
                            <button name="btnComprar" class="boton-comprar" data-id="<%# Eval("OID") %>">Comprar</button>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>

  </div>
</asp:Content>

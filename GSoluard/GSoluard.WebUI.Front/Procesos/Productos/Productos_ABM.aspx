﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginasMaster.Master" AutoEventWireup="true" CodeBehind="Productos_ABM.aspx.cs" Inherits="GSoluard.WebUI.Front.Parametrizacion.Productos_ABM" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<script type ="text/javascript">
    $(function () {

        $("#ac").accordion("option", "active", 0);

        Globalize.culture('de-DE');


    });

    function CerrarPopup() {
        $('#modificar_div').dialog('close');
        $('#modificar_div').remove();
    }

    function DialogDetalle() {
        $("#dialog-detalle").dialog({
            resizable: false,
            height: 250,
            width: 460,
            modal: false,
            open: function (event, ui) {
                $(this).parent().appendTo("form");
            }
        });
        return false;
    }

    function LimpiarDetalle() {
        $("#<%=txtCantidad.ClientID %>").val(0);        

    }


</script>

   <div class="formRegistro ui-widget">	
        <div class="filaCampo">
			<div class="etiqueta">Nombre : </div>
			<div class="campo"> <wce:TextBoxExt ID="txtNombre" runat="server"  Required="true" MaxLength="50"></wce:TextBoxExt></div>
        </div> 
       <br />
		<div class="filaCampo">
			<div class="etiqueta">Descripcion  : </div>
			<div class="campo"> <wce:TextBoxExt  ID="txtDescripcion" runat="server" Required="true" TextMode="MultiLine" Height="80px" Width="90%"></wce:TextBoxExt></div>
		</div>
       <br /><br /><br /><br /><br />
        <div class="filaCampo">
			<div class="etiqueta">Categoria  : </div>
			<div class="campo">   <wce:DropDownListExt ID="cboCategoria" runat="server" Required="true" ></wce:DropDownListExt>
            </div>
		</div>
       <br /><br />
		<div class="filaCampo">
			<div class="etiqueta">Stock  : </div>
			<div class="campo"> <wce:TextBoxExt  ID="txtStock" runat="server" Required="true" MaxLength="5" Width="60px" Type="Entero"></wce:TextBoxExt></div>
		</div>
       <br /><br />
		<div class="filaCampo">
			<div class="etiqueta">Precio  : </div>
			<div class="campo"> $ <wce:TextBoxExt  ID="txtPrecio" runat="server" Required="true" MaxLength="10" Width="100px" Type="Money" Style="text-align: right;" ></wce:TextBoxExt></div>
		</div>
       <br /><br />
		<div class="filaCampo">
			<div class="etiqueta">Imagen  :</div>
			<div class="campo">  
                <center>
                <asp:Image ID="imagenControl" runat="server" Height="100px"  /><br />
                    </center>
                <asp:FileUpload ID="FileImagen" runat="server"  />                
			</div>
		</div>
       <br /><br /><br /><br /><br /><br /><br /><br />
         <div id="detalle"class="accordion">
         <h3> Detalle</h3>
         <div>
              <wce:ButtonExt ID="btnAgregar_Detalle" runat="server" Text="Agregar Materia Prima" 
              OnClientClick=" LimpiarDetalle(); return DialogDetalle();"/>
         <br />
        <asp:UpdatePanel runat="server" ID="UpdatePanelDetalle" UpdateMode="Conditional">
        <ContentTemplate>
          <wce:GridViewExt ID="GridDetalle" runat="server" AutoGenerateColumns="false"
          DataKeyNames="OID" OnRowCommand="GridDetalle_RowCommand"
          OnRowDataBound="GridDetalle_RowDataBound">
          <Columns>
            <asp:BoundField DataField="Cantidad" HeaderText="Cantidad" ItemStyle-Width="40px"  ItemStyle-HorizontalAlign="Center"/>
            <asp:BoundField DataField="MateriaPrima" HeaderText="Materia Prima" ItemStyle-Width="120px"/>             
            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="40px">
             <ItemTemplate>
                    <wce:ImageButtonExt ID="btnEditar" runat="server"
                     CommandName="Editar" CommandArgument="<%# ((GridViewRow)Container).RowIndex %>"></wce:ImageButtonExt>
               </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>  
           <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="40px">
                    <ItemTemplate>
                      <wce:ImageButtonExt ID="btnBorrar" runat="server" 
                       CommandName="Borrar" CommandArgument="<%# ((GridViewRow)Container).RowIndex %>">
                      </wce:ImageButtonExt>
                   </ItemTemplate>
                  <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
         </Columns>
            
        </wce:GridViewExt>
        </ContentTemplate>
           <Triggers>
              <asp:AsyncPostBackTrigger ControlID="btn_AgregarDetalle" EventName="Click" />
           </Triggers>

        </asp:UpdatePanel>
        </div>
         
         </div>

       <br /><br />
       <div class="filaCampo">
         <wce:ButtonExt ID="btnGuardar" runat="server"  Text="Guardar" 
            onclick="btnGuardar_Click" OnClientClick="return validarFormulario();"/>
         <wce:ButtonExt ID="btnCerrar" runat="server"  Text="Cerrar" OnClientClick="parent.CerrarPopup(); return false;"  />
       </div>
    </div>

<div id="dialog-detalle" title="Detalle" style="display: none;">
         <asp:UpdatePanel ID="UpdatePanelABMDetalle" runat="server" UpdateMode="Conditional">
         <ContentTemplate>
                <asp:HiddenField ID="hiddetalle" runat="server" />

                 <br /> <br />
               <div class="filaCampo">
			        <div class="etiqueta">Cantidad : </div>
			        <div class="campo">
                        <wce:TextBoxExt ID="txtCantidad" runat="server"  MaxLength="5" Width="40px" Type="Entero" Required="true"></wce:TextBoxExt>
			        </div>
		        </div>
               <br />
                <div class="filaCampo">
			        <div class="etiqueta">Materia Prima  : </div>
			        <div class="campo">   
                        <wce:DropDownListExt ID="cboMateriaPrima" runat="server" Required="true" ></wce:DropDownListExt>
                    </div>
		        </div>
                 
              <br /> <br /><br /> <br />
                                         
                    <wce:ButtonExt ID="btn_AgregarDetalle" runat="server" Text="Guardar"  OnClick = "btn_AgregarDetalle_Click"                   
                    OnClientClick="return validarDialog('dialog-detalle');"/>

                   <wce:ButtonExt ID="btn_Cerrar_dialog_detalle" runat="server" Text="Cerrar" 
                   OnClientClick="$('#dialog-detalle').dialog('close'); return false;" />
                   </ContentTemplate>
            </asp:UpdatePanel>
    </div>

<script type="text/javascript">
    //activa la validacion en el onblur de los campos
    validarFormBlurActivo();
    $("#<%=txtStock.ClientID %>").spinner();
    $("#<%=txtCantidad.ClientID %>").spinner();

    function validarExtension() {
        var fileInput = document.getElementById("FileImagen");
        var filePath = fileInput.value;
        if (filePath != "") {
            var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;

            if (!allowedExtensions.exec(filePath)) {
                alert("Por favor, selecciona un archivo de imagen con una extensión válida (.jpg, .jpeg, .png o .gif).");
                FileImagen.value = "";
                return false;
            }
        }
        return true;
    }

    function validarFormulario() {

        if (validarForm() == true) {

            if (!validarExtension()) {
                return false
            }
            else {

                return true;
            }
        }
        else return false;

    }

</script>



</asp:Content>

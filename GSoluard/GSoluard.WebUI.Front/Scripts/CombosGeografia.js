﻿//SetearCombosGeografia(
//	{
//	    cboPais: '<%=cboPais.ClientID%>',
//	    cboProvincia: '<%=cbProvincia.ClientID%>',
//	    cboCiudad: '<%=cboCiudad.ClientID%>'
//	}
//);


//$(function () {

function SetearCombosGeografia(comboPais, comboPcia, comboCiudad) {

// $("#" + comboPais).change(function () {
  
     $("#" + comboPais).live('change', function () {
        
        SelectLoading("#" + comboPcia);
        SelectLoading("#" + comboCiudad);
        // armo el objeto que servira de parametro, para ello utilizo una libreria de JSON
        //este parametro mapeara con el definido en metodo estatico
        var params = new Object();
        params.paisId = $("#" + comboPais).val();
        params = JSON.stringify(params);

        $.ajax({
            type: "POST",
            url: "/Administracion/Geografia/Geografia.asmx/GetProvinciaByPais",
            data: params,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(textStatus + ": " + XMLHttpRequest.responseText);
            }
        }).done(function (result) {
            SelectDefaultValue("#" + comboPcia);

            $.each(result.d, function () {
                $("#" + comboPcia).append($("<option></option>").attr("value", this.OID).text(this.Nombre))
            });
        });

    });

    $("#" + comboPcia).live('change',function () {

        SelectLoading("#" + comboCiudad);
        // armo el objeto que servira de parametro, para ello utilizo una libreria de JSON
        //este parametro mapeara con el definido en el web service
        var params = new Object();
        params.provId = $("#" + comboPcia).val();
        params = JSON.stringify(params);

        $.ajax({
            type: "POST",
            url: "/Administracion/Geografia/Geografia.asmx/GetCiudadByProvincia",
            data: params,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(textStatus + ": " + XMLHttpRequest.responseText);
            }

        }).done(function (result) {
            SelectDefaultValue("#" + comboCiudad);
            $.each(result.d, function () {
                $("#" + comboCiudad).append($("<option></option>").attr("value", this.OID).text(this.Nombre));
            });
        });

    });

    $("#" + comboPais).change();
}


﻿
function Autocompletar(txtapellidonombre, hidden) {


    var autocompleteData;

   // $(document).ready(function () {
        $("#ContentPlaceHolder1_txtPersonaFirmante").autocomplete({
            source: function (request, response) {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                     url: "/Administracion/Personas/WebServicePersonas.asmx/ListarPersonasByApellidos",
                    data: "{ 'prefixText': '" + request.term + "' }",
                    dataType: "json",
                    async: true,
                    success: function (data) {

                        var autocompleteOutput = [];


                        $.each(data.d, function (index, item) {
                            autocompleteOutput[index] = item.NombresYApellidos;
                        });

                        // pasar todos los valores al autocomplete UI
                        response(autocompleteOutput);


                        autocompleteData = data.d;
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert(textStatus);
                    }
                });
            },
            minLength: 0,
            open: function (event, ui) {
                $(this).autocomplete("widget").css({
                    "width": 200, "font-size": 12
                });

            },

            select: function (event, ui) {
                // ui.item can be null when user didn't select any option from displayed hints
                // it can happen when user regardless of displayed hints simply typed something into the text input
                // the safest option would be to take value directly from text input
                var selectedDescription = ui.item ? selectedDescription = ui.item.value : selectedDescription = $("#ContentPlaceHolder1_txtApyNombre").val();

                var matchingElementsArray = $.grep(autocompleteData, function (item) { return item.NombresYApellidos == selectedDescription; });


                if (matchingElementsArray[0]) {

                    $("#ContentPlaceHolder1_txtPersonaFirmante").val(matchingElementsArray[0].NombresYApellidos)

                    $("#ContentPlaceHolder1_hidpersona").val(matchingElementsArray[0].OID);
                  
                }

            }
        });


   // });








}




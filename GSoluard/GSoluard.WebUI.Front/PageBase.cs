﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GSoluard.Administration.Entities;
using System.Diagnostics;
using System.Reflection;
using GSoluard.WebUI.Front.GlobalUI;

namespace GSoluard.WebUI.Front
{
    public class PageBase : System.Web.UI.Page
    {
        protected Modulo modulo;
        protected override void OnInitComplete(EventArgs e)
        {
            base.OnInitComplete(e);

            //Carga el modulo de la pagina actual
            modulo = SiteController.GetCurrentModulo();
            
        }



    }
}
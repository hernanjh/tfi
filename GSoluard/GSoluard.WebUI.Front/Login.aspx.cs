﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GSoluard.Administration.Engine;
using System.Web.Security;
using GSoluard.ControlsUI;

namespace GSoluard.WebUI.Front
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Initialize();
        }
        void Initialize()
        {
            this.cmdLogin.Settings(new ButtonExtText());
            this.txtUser.Settings(new TextBoxExtStyle());
            this.txtPassword.Settings(new TextBoxExtStyle());
        }

        protected void ProcessLogin(object sender, EventArgs e)
        {
            try
            {
                GestorUsuario.Autenticar(txtUser.Text, txtPassword.Text, true);
                FormsAuthentication.RedirectFromLoginPage(txtUser.Text, chkPersistLogin.Checked);
            }
            catch (Exception ex)
            {
                ErrorMessage.InnerHtml = ex.Message;
            }
             
        }
    }
}
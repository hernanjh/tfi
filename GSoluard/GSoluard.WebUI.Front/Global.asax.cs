﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Configuration;
using GSoluard.DAO;
using GSoluard.Interfaces.Observer;
using GSoluard.Administration.Entities;
using GSoluard.Administration.Engine;

namespace GSoluard.WebUI.Front
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            //Inicializa la Aplicacion
            MyApplication.Init();

            //Notifica a los observadores
            ObserverApplicationStart.Instance.Notify();
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            //si se cae la session la vuelve a levantar
            if (HGSContext.Usuario.OID == 0 && User.Identity.Name != null)
            {
                GestorUsuario.LoadContexto(User.Identity.Name, true);
            }
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {
            //Notifica a los observadores
            ObserverSessionEnd.Instance.Notify();
            try
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
            }
            catch (Exception)
            {
            }
        }

        protected void Application_End(object sender, EventArgs e)
        {
            
        }
    }
}
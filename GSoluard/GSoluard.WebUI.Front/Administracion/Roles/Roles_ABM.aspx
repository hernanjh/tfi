﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginasMaster.Master" AutoEventWireup="true" CodeBehind="Roles_ABM.aspx.cs" Inherits="GSoluard.WebUI.Front.Administracion.Roles.Roles_ABM" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <div class="formRegistro ui-widget">	
		<div class="filaCampo">
			<div class="etiqueta">Codigo o Siglas : </div>
			<div class="campo"> <wce:TextBoxExt  ID="txtCodigo" runat="server" Required="true"></wce:TextBoxExt></div>
		</div>
        <br />
		<div class="filaCampo">
			<div class="etiqueta">Nombre Rol  : </div>
			<div class="campo"> <wce:TextBoxExt  ID="txtNombre" runat="server" Required="true"></wce:TextBoxExt></div>
		</div>
        <br />
        <div id="tree" class="filaCampo ui-widget-content ui-corner" style="height:200px">
            <asp:TreeView ID="treeModulos" runat="server" ShowExpandCollapse="true" ExpandDepth="0">
            </asp:TreeView>
        </div>
       <br />
       <div class="filaCampo">
         <wce:ButtonExt ID="btnGuardar" runat="server"  Text="Guardar" 
            onclick="btnGuardar_Click" OnClientClick="return validarForm();" />
         <wce:ButtonExt ID="btnCerrar" runat="server"  Text="Cerrar" OnClientClick="parent.CerrarPopup(); return false;"  />
       </div>
    </div>
<script type="text/javascript">
    //activa la validacion en el onblur de los campos
    validarFormBlurActivo();

    $("#tree").niceScroll({ styler: "fb", cursorcolor: "#3baae3" });

</script>


</asp:Content>

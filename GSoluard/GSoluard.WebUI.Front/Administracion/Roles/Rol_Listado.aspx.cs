﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GSoluard.Administration.Engine;
using GSoluard.Administration.Entities;
using System.Data;
using GSoluard.WebControlExtend;
using GSoluard.ControlsUI;

namespace GSoluard.WebUI.Front
{
    public partial class Rol_Listado : PageBase
    {
        #region Propiedades
        public List<Rol> Roles
        {
            get
            {
                if (ViewState["roles"] != null)
                    return (List<Rol>)ViewState["roles"];
                return null;
            }
            set
            {
                ViewState["roles"] = value;
            }

        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Initialize();
                LoadGridRol(false);
            }
        }

        void Initialize()
        {
            this.GridRol.Settings(new GridViewExtBasic());
            this.btnNuevo.Settings(new ButtonExtText());
            this.btnRefresh.Settings(new ButtonExtText());
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadGridRol(false);
        }

        protected void btnBorrar_Click(object sender, EventArgs e)
        {
            ImageButton btn = (ImageButton)sender;
            int index = Convert.ToInt32(btn.CommandArgument);
            int id = Convert.ToInt32(GridRol.DataKeys[index].Value);

            Rol rol = ((List<Rol>)this.GridRol.DataSource).Find(c => c.OID == id);

            GestorRoles.Eliminar(rol);
            LoadGridRol(false);

        }


        private void LoadGridRol(bool paging)
        {
            if (!paging)
                Roles = GestorRoles.Consultar();
            this.GridRol.DataSource = Roles;
            this.GridRol.DataBind();
        }

        protected void GridTipoDocumento_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButtonExt Editar = e.Row.FindControl("btnEditar") as ImageButtonExt;
                if (Editar != null)
                {
                    Editar.Settings(new ImageButtonExtEdit());
                    int oid = DataBinder.Eval(e.Row.DataItem, "OID").ToInteger();
                    Editar.OnClientClick = "Editar(" + oid.ToString() + "); return false;";
                }

                ImageButtonExt Borrar = e.Row.FindControl("btnBorrar") as ImageButtonExt;
                if (Borrar != null)
                {
                    Borrar.Settings(new ImageButtonExtDelete());
                }
            }
        }

        protected void GridTipoDocumento_PageIndexChanging(Object sender, GridViewPageEventArgs e)
        {
            GridRol.PageIndex = e.NewPageIndex;
            LoadGridRol(true);
        }


    }

}

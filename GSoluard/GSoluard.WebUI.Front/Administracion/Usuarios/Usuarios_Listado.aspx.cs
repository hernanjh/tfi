﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GSoluard.ControlsUI;
using GSoluard.Administration.Entities;
using GSoluard.Administration.Engine;
using GSoluard.WebControlExtend;

namespace GSoluard.WebUI.Front.Administracion.Usuarios
{
    public partial class Usuarios_Listado : PageBase
    {
        #region Propiedades
        public List<Usuario> Usuarios
        {
            get
            {
                if (ViewState["usuarios"] != null)
                    return (List<Usuario>)ViewState["usuarios"];
                return null;
            }
            set
            {
                ViewState["usuarios"] = value;
            }

        }
        #endregion

        #region EvalApyNombre

        protected string GetApyNombre()
        {
            return  Eval("Apellido") + ", " + Eval("Nombre");

        }


        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Initialize();
                LoadGridUsuario(false);
            }
        }

        void Initialize()
        {
            this.GridUsuario.Settings(new GridViewExtBasic());
            this.btnNuevo.Settings(new ButtonExtText());
            this.btnRefresh.Settings(new ButtonExtText());
            this.btnFiltrar.Settings(new ButtonExtText());
            this.bntQuitarFiltros.Settings(new ButtonExtText());
            this.txtApyNombre.Settings(new TextBoxExtStyle());
            this.txtUserName.Settings(new TextBoxExtStyle());
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadGridUsuario(false);
        }

        protected void btnBorrar_Click(object sender, EventArgs e)
        {
            try
            {
                ImageButton btn = (ImageButton)sender;
                int index = Convert.ToInt32(btn.CommandArgument);
                int id = Convert.ToInt32(GridUsuario.DataKeys[index].Value);
                              
                 Usuario usuario = Usuarios.Find(c => c.OID == id);
                 Usuarios.Remove(usuario);
                
                 GestorUsuario.Eliminar(usuario);
                 LoadGridUsuario(true);
            }
            catch (Exception ex)
            { }
        }


        private void LoadGridUsuario(bool paging)
        {
            if (!paging)
            {
                Usuarios = GestorUsuario.ConsultaTodos() ;
           
            }

            this.GridUsuario.DataSource = Usuarios;
            this.GridUsuario.DataBind();
        }

        protected void GridUsuario_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {


                bool bloqueado = (bool)DataBinder.Eval(e.Row.DataItem,"Bloqueado");
                if (bloqueado )
                e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#fc9296");



                ImageButtonExt Editar = e.Row.FindControl("btnEditar") as ImageButtonExt;
                if (Editar != null)
                {
                    Editar.Settings(new ImageButtonExtEdit());
                    int oid = DataBinder.Eval(e.Row.DataItem, "OID").ToInteger();
                    Editar.OnClientClick = "Editar(" + oid.ToString() + "); return false;";
                }

                ImageButtonExt Borrar = e.Row.FindControl("btnBorrar") as ImageButtonExt;
                if (Borrar != null)
                {
                    Borrar.Settings(new ImageButtonExtDelete());
                }
            }
        }

        protected void GridUsuario_PageIndexChanging(Object sender, GridViewPageEventArgs e)
        {
            this.GridUsuario.PageIndex = e.NewPageIndex;
            LoadGridUsuario(true);
        }



        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            List<Usuario> filtroResult = null;

            if (!string.IsNullOrEmpty(this.txtUserName.Text))
            {
                filtroResult = Usuarios.FindAll(y => y.Nick.ToLower().Contains(this.txtUserName.Text.ToLower()));
            }

            if (!string.IsNullOrEmpty(this.txtApyNombre.Text))
            {
                filtroResult = Usuarios.FindAll(y => y.NombresYApellidos.ToLower().Contains(this.txtApyNombre.Text.ToLower()));
            }
            if (filtroResult == null) filtroResult = Usuarios;

            this.GridUsuario.DataSource = filtroResult;
            this.GridUsuario.DataBind();
        }
        protected void btnQuitarFiltros_Click(object sender, EventArgs e)
        {
            this.txtApyNombre.Text = string.Empty;
            this.txtUserName.Text = string.Empty;

            LoadGridUsuario(true);

        }
    }
}
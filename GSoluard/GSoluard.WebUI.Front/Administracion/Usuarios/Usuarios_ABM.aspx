﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginasMaster.Master" AutoEventWireup="true" CodeBehind="Usuarios_ABM.aspx.cs" Inherits="GSoluard.WebUI.Front.Administracion.Usuarios.Usuarios_ABM" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<style type="text/css">
.izquierda
{
  float: left;
}

    .style3
    {
        width: 172px;
    }
    .style4
    {
        width: 79px;
    }
    .style5
    {
        width: 94px;
    }

</style>
  <div class="formRegistro ui-widget">	
		

        <div class="filaCampo">
			<div class="etiqueta">Usuario : </div>
			<div class="campo"> 
                <wce:TextBoxExt  ID="txtUsuario" runat="server" MaxLength="50" 
                    Width="165px"  Type="Email" ></wce:TextBoxExt></div>
		</div>
        <br />
          <div class="filaCampo">
			<div class="etiqueta">Clave : </div>
			<div class="campo"> <wce:TextBoxExt  ID="txtClave" runat="server"  
                     Font-Bold="True" Required="True" TextMode="Password" MaxLength="250"></wce:TextBoxExt></div>
		</div>
        <br />
          <div class="filaCampo">
			<div class="etiqueta">Nombre : </div>
			<div class="campo"> <wce:TextBoxExt  ID="txtNombre" runat="server"  
                     Font-Bold="True" Required="True" MaxLength="100"></wce:TextBoxExt></div>
		</div>
        <br />
          <div class="filaCampo">
			<div class="etiqueta">Apellido : </div>
			<div class="campo"> <wce:TextBoxExt  ID="txtApellido" runat="server"  
                     Font-Bold="True" Required="True" MaxLength="100"></wce:TextBoxExt></div>
		</div>
        <br />
        <br />
         <div class="filaCampo">
			<div class="etiqueta">Bloqueado  : </div>
			<div class="campo">
                <asp:CheckBox ID="chkBloqueado" runat="server" />
                
            </div>
		</div>
        <br />
         <div class="filaCampo">
			<div class="etiqueta">Super Admin  : </div>
			<div class="campo">
                <asp:CheckBox ID="chkSuperAdmin" runat="server" />
                
            </div>
		</div>
        <br />        
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
    <ContentTemplate>
      <table style="width: 100%;">
          <tr>
              <td class="style3">
                  Disponible</td>
              <td class="style5">
                  &nbsp;</td>
              <td>
                  Asignados</td>
          </tr>
     
          <tr>
              <td class="style3">
                <wce:ListBoxExt ID="lstDisponibles" runat="server" SelectionMode="multiple" Width="200px" />
              </td>
              <td class="style5" style="text-align:center">             
                <wce:ButtonExt id="uxLeftBtn" runat="server"  Text=" << "  onclick="uxLeftBtn_Click" Height="26px" />
                <wce:ButtonExt id="uxRightBtn" runat="server" Text=" >> "     onclick="uxRightBtn_Click" Height="26px"  />
              </td>
              <td>
                <wce:ListBoxExt ID="lstAsignados" runat="server" SelectionMode="multiple" HorizontalAlign="Right" Width="200px" />
              </td>
          </tr>
     
      </table>
       </ContentTemplate>
       </asp:UpdatePanel>
        	 <br />
            <br />
       <div class="filaCampo">
         <wce:ButtonExt ID="btnGuardar" runat="server"  Text="Guardar" 
            onclick="btnGuardar_Click" OnClientClick="return validarForm();" />
         <wce:ButtonExt ID="btnCerrar" runat="server"  Text="Cerrar" OnClientClick="parent.CerrarPopup(); return false;"  />
       </div>
    </div>
<script type="text/javascript">
    //activa la validacion en el onblur de los campos
    validarFormBlurActivo();

</script>



</asp:Content>


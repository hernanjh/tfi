﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GSoluard.Administration.Entities;
using GSoluard.ControlsUI;
using GSoluard.Administration.Engine.Parametrizacion;
using GSoluard.WebControlExtend;

namespace GSoluard.WebUI.Front.Parametrizacion
{
    public partial class FormaPago_Listado :  PageBase
    {
       
        #region Propiedades
        public List<FormaPago> FormaPagos
        {
            get
            {
                if (ViewState["FormaPago"] != null)
                    return (List<FormaPago>)ViewState["FormaPago"];
                return null;
            }
            set
            {
                ViewState["FormaPago"] = value;
            }

        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Initialize();
                LoadGrid(false);
            }
        }

        void Initialize()
        {
            this.GridFormaPago.Settings(new GridViewExtBasic());
            this.btnNuevo.Settings(new ButtonExtText());
            this.btnRefresh.Settings(new ButtonExtText());                        
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadGrid(false);
        }

        protected void btnBorrar_Click(object sender, EventArgs e)
        {
            ImageButton btn = (ImageButton)sender;
            int index = Convert.ToInt32(btn.CommandArgument);
            int id = Convert.ToInt32(GridFormaPago.DataKeys[index].Value);
                    
            FormaPago obj = FormaPagos.Find(c => c.OID == id);
            FormaPagos.Remove(obj);

            GestorFormaPago.Eliminar(obj);
            LoadGrid(true);

        }

        

        private void LoadGrid(bool cache)
        {
            if (!cache)
                FormaPagos = GestorFormaPago.Consultar();

            this.GridFormaPago.DataSource = FormaPagos;
            this.GridFormaPago.DataBind();
        }

        protected void GridFormaPago_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButtonExt Editar = e.Row.FindControl("btnEditar") as ImageButtonExt;
                if (Editar != null)
                {
                    Editar.Settings(new ImageButtonExtEdit());
                    int oid = DataBinder.Eval(e.Row.DataItem, "OID").ToInteger();
                    Editar.OnClientClick = "Editar(" + oid.ToString() + "); return false;";
                }

                ImageButtonExt Borrar = e.Row.FindControl("btnBorrar") as ImageButtonExt;
                if (Borrar != null)
                {
                    Borrar.Settings(new ImageButtonExtDelete());
                }

            }
        }

        protected void GridFormaPago_PageIndexChanging(Object sender, GridViewPageEventArgs e)
        {
            GridFormaPago.PageIndex = e.NewPageIndex;
            LoadGrid(true);
        }


    }
}
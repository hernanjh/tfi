﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GSoluard.Administration.Entities;
using GSoluard.ControlsUI;
using GSoluard.Administration.Engine.Parametrizacion;
using GSoluard.WebControlExtend;

namespace GSoluard.WebUI.Front.Parametrizacion
{
    public partial class MateriaPrima_Listado :  PageBase
    {
       
        #region Propiedades
        public List<MateriaPrima> MateriaPrimas
        {
            get
            {
                if (ViewState["MateriaPrima"] != null)
                    return (List<MateriaPrima>)ViewState["MateriaPrima"];
                return null;
            }
            set
            {
                ViewState["MateriaPrima"] = value;
            }

        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Initialize();
                LoadGrid(false);
            }
        }

        void Initialize()
        {
            this.GridMateriaPrima.Settings(new GridViewExtBasic());
            this.btnNuevo.Settings(new ButtonExtText());
            this.btnRefresh.Settings(new ButtonExtText());
            this.btnFiltrar.Settings(new ButtonExtText());
            this.bntQuitarFiltros.Settings(new ButtonExtText());
            this.txtNombre.Settings(new TextBoxExtStyle());
            this.txtDescripcion.Settings(new TextBoxExtStyle());
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadGrid(false);
        }

        protected void btnBorrar_Click(object sender, EventArgs e)
        {
            ImageButton btn = (ImageButton)sender;
            int index = Convert.ToInt32(btn.CommandArgument);
            int id = Convert.ToInt32(GridMateriaPrima.DataKeys[index].Value);
                    
            MateriaPrima obj = MateriaPrimas.Find(c => c.OID == id);
            MateriaPrimas.Remove(obj);

            GestorMateriaPrima.Eliminar(obj);
            LoadGrid(true);

        }

        

        private void LoadGrid(bool cache)
        {
            if (!cache)
                MateriaPrimas = GestorMateriaPrima.Consultar();

            this.GridMateriaPrima.DataSource = MateriaPrimas;
            this.GridMateriaPrima.DataBind();
        }

        protected void GridMateriaPrima_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButtonExt Editar = e.Row.FindControl("btnEditar") as ImageButtonExt;
                if (Editar != null)
                {
                    Editar.Settings(new ImageButtonExtEdit());
                    int oid = DataBinder.Eval(e.Row.DataItem, "OID").ToInteger();
                    Editar.OnClientClick = "Editar(" + oid.ToString() + "); return false;";
                }

                ImageButtonExt Borrar = e.Row.FindControl("btnBorrar") as ImageButtonExt;
                if (Borrar != null)
                {
                    Borrar.Settings(new ImageButtonExtDelete());
                }

            }
        }

        protected void GridMateriaPrima_PageIndexChanging(Object sender, GridViewPageEventArgs e)
        {
            GridMateriaPrima.PageIndex = e.NewPageIndex;
            LoadGrid(true);
        }

        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            List<MateriaPrima> filtroResult = null;

            if (!string.IsNullOrEmpty(this.txtNombre.Text))
            {
                filtroResult = MateriaPrimas.FindAll(y => y.Nombre.ToLower().Contains(this.txtNombre.Text.ToLower()));
            }

            if (!string.IsNullOrEmpty(this.txtDescripcion.Text))
            {
                filtroResult = MateriaPrimas.FindAll(y => y.Descripcion.ToLower().Contains(this.txtDescripcion.Text.ToLower()));
            }
            if (filtroResult == null) filtroResult = MateriaPrimas;

            this.GridMateriaPrima.DataSource = filtroResult;
            this.GridMateriaPrima.DataBind();
        }
        protected void btnQuitarFiltros_Click(object sender, EventArgs e)
        {
            this.txtNombre.Text = string.Empty;
            this.txtDescripcion.Text = string.Empty;

            LoadGrid(true);

        }



    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GSoluard.Administration.Entities;
using GSoluard.Administration.Engine.Parametrizacion;
using GSoluard.ControlsUI;

namespace GSoluard.WebUI.Front.Parametrizacion
{
    public partial class MateriaPrima_ABM : PageBase
    {

        #region Propiedades

        public MateriaPrima MateriaPrima
        {
            get
            {
                if (ViewState["MateriaPrima"] != null)
                    return (MateriaPrima)ViewState["MateriaPrima"];
                return null;
            }
            set
            {
                ViewState["MateriaPrima"] = value;
            }

        }

        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["id"] == null)
                {
                    MateriaPrima = new MateriaPrima();
                }
                else
                {
                    var Id = Convert.ToInt32(Request.QueryString["id"]);
                    MateriaPrima = GestorMateriaPrima.Obtener(Id);
                    this.txtNombre.Text = MateriaPrima.Nombre;
                    this.txtDescripcion.Text = MateriaPrima.Descripcion;
                    this.txtStock.Text = MateriaPrima.Stock.ToString();
                }
                Initialize();
            }

        }

        void Initialize()
        {
            this.btnGuardar.Settings(new ButtonExtText());
            this.btnCerrar.Settings(new ButtonExtText());
            this.txtNombre.Settings(new TextBoxExtStyle());
            this.txtDescripcion.Settings(new TextBoxExtStyle());

        }


        protected void btnGuardar_Click(object sender, EventArgs e)
        {

            MateriaPrima.Nombre = this.txtNombre.Text;
            MateriaPrima.Descripcion = this.txtDescripcion.Text;
            MateriaPrima.Stock = this.txtStock.Text.ToInteger();

            GestorMateriaPrima.Guardar(MateriaPrima);

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "closedialog", "parent.IntervaloCerrarPopup();", true);

        }


        protected void btnCerrar_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "closedialog", "parent.CerrarPopup();", true);
        }

      
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GSoluard.Administration.Entities;
using GSoluard.Administration.Engine.Parametrizacion;
using GSoluard.ControlsUI;

namespace GSoluard.WebUI.Front.Parametrizacion
{
    public partial class Proveedores_ABM : PageBase
    {

        #region Propiedades

        public Proveedor Proveedor
        {
            get
            {
                if (ViewState["Proveedor"] != null)
                    return (Proveedor)ViewState["Proveedor"];
                return null;
            }
            set
            {
                ViewState["Proveedor"] = value;
            }

        }

        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["id"] == null)
                {
                    Proveedor = new Proveedor();
                }
                else
                {
                    var Id = Convert.ToInt32(Request.QueryString["id"]);
                    Proveedor = GestorProveedor.Obtener(Id);
                    this.txtRazonSocial.Text = Proveedor.RazonSocial;
                    this.txtCuit.Text = Proveedor.Cuit;

                }
                Initialize();
            }

        }

        void Initialize()
        {
            this.btnGuardar.Settings(new ButtonExtText());
            this.btnCerrar.Settings(new ButtonExtText());
            this.txtCuit.Settings(new TextBoxExtStyle());
            this.txtRazonSocial.Settings(new TextBoxExtStyle());
        }


        protected void btnGuardar_Click(object sender, EventArgs e)
        {

            Proveedor.RazonSocial = this.txtRazonSocial.Text;
            Proveedor.Cuit = this.txtCuit.Text;

            GestorProveedor.Guardar(Proveedor);

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "closedialog", "parent.IntervaloCerrarPopup();", true);

        }


        protected void btnCerrar_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "closedialog", "parent.CerrarPopup();", true);
        }

      
    }
}
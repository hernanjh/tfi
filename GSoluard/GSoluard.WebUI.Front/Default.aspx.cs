﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GSoluard.Interfaces.Observer;
using System.Web.Security;
using System.Threading;
using GSoluard.Administration.Entities;
using GSoluard.Administration.Engine;

namespace GSoluard.WebUI.Front
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            divUsuario.InnerHtml = "<span style='float: left; margin-right: .3em;'><img src='/Styles/HerGoS-theme/images/icons/user.png' /></span>" + HGSContext.Usuario.ToString();

            //Genera el menu segun los permisos de usuario
            var sb = new System.Text.StringBuilder(); 
            sb.Append("<ul class=\"menu\">");
            foreach (Modulo modulo in HGSContext.Permisos.Modulos)
            {
                WriteMenu(modulo, sb);
            }
            sb.Append("</ul>");
            MenuContainer.InnerHtml = sb.ToString();
        }

        protected void btnSalir_Click(object sender, EventArgs e)
        {            
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
        }

        private void WriteMenu(Modulo modulo, System.Text.StringBuilder sb)
        {
            if (modulo.IsMenu)
            {
                List<Modulo> submodulos = modulo.Modulos;
                sb.Append(string.Format("<li><a href=\"{1}\" target=\"ifmPaginas\">{0}</a>", modulo.Nombre, modulo.Link ?? "#"));
                if (submodulos.Count() > 0)
                {
                    sb.Append(string.Format("<ul class=\"{0}\">", "submenu"));
                    foreach (Modulo m in submodulos)
                    {
                        WriteMenu(m, sb);
                    }
                    sb.Append("</ul>");

                }
                sb.Append("</li>");
            }
        }
        
    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GSoluard.Interfaces.Observer;
using System.Configuration;
using GSoluard.DAO;
using GSoluard.WebUI.Front.GlobalUI;

namespace GSoluard.WebUI.Front
{
    public class MyApplication
    {
        //Se jecuta al Iniciar la aplicacion
        public static void Init()
        {
            LoadConnectionString();
            SiteController.LoadPagesModules();

        }

        static void LoadConnectionString()
        {
            //Carga la lista estatica con las conecciones configuradas en el web.config
            foreach (ConnectionStringSettings conn in ConfigurationManager.ConnectionStrings)
            {
                RepositoryFactory.AddConnection(conn.Name, conn.ConnectionString);
            }
        }


    }
}
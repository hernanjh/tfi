﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;

namespace GSoluard.DAO
{
    public interface IRepository : IDisposable
    {
        string Nombre { get; set; }
        int CommandTimeout { get; set; }

        void CerrarConexion();
        DbTransaction BeginTransaction();
        DbTransaction GetTransaction();

        void AddParameter(string name, object value);

        int ExecuteNonQuery();
        object ExecuteReturnValue();
        object ExecuteScalar();
        DataTable ExecuteDataTable();
        DataSet ExecuteDataSet();
        string ExecuteScalarXml();

        void Commit();
        void Rollback();

        event EventHandler OnCommit;
        event EventHandler OnRollback;

        String printSP();
        Boolean IsDisposed { get; }

    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GSoluard.CommonTools
{
    public class Mail
    {
        public static void SendMessage(string sendTo, string sendFrom, string sendSubject, string sendMessage)
        {
            try
            {                
                System.Web.Mail.MailMessage message = new System.Web.Mail.MailMessage();
                message.From = sendFrom;
                message.To = sendTo;
                message.Subject = sendSubject;
                message.Body = sendMessage;
                message.BodyFormat = System.Web.Mail.MailFormat.Html;

                System.Web.Mail.SmtpMail.Send(message);

            }
            catch (Exception ex)
            {
                //return ex.Message.ToString();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Data;
using System.Reflection;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace System
{
    public static class ExtensionMethods
    {

        public static void Each<T>(this IEnumerable<T> list, Action<T> action)
        {
            foreach (var item in list) { action(item); }
        }


        public static List<T> ToGenericList<T>(this DataTable datatable, Func<DataRow, T> converter)
        {
            return (from row in datatable.AsEnumerable()
                    select converter(row)).ToList();
        }

        public static DateTime ToDateTime(this object data)
        {
            DateTime result;
            try
            {
                result = System.Convert.ToDateTime(data.ToString());
            }
            catch (Exception)
            {
                result = DateTime.MinValue;
            }
            return result;
        }

        public static DateTime?  ToNullableDateTime(this object data)
        {
            DateTime? result;
            try
            {
                result = System.Convert.ToDateTime(data.ToString());
            }
            catch (Exception)
            {
                result = null;
            }
            return result;
        }


        public static int ToInteger(this object data)
        {
            int result;
            int.TryParse(data.ToString(), out result);
            return result;
        }

        public static decimal ToDecimal(this object data)
        {
            decimal result;
            decimal.TryParse(data.ToString(), out result);
            return result;
        }

        public static bool ToBoolean(this object data)
        {
            bool result;
            bool.TryParse(data.ToString(), out result);
            return result;
        }

        public static float ToFloat(this object data)
        {
            float result;
            float.TryParse(data.ToString(), out result);
            return result;

        }
        public static byte[] ToBytes(this object data)
        {
            byte[] result;
            try
            {
                result = (byte[])data;
            }
            catch (Exception)
            {
                result = null;
            }
            return result;
        }

        public static string ToJson(this object data)
        {
            string result = "";
            try
            {
                result = JsonConvert.SerializeObject(data, Newtonsoft.Json.Formatting.Indented,
                                            new JsonSerializerSettings()
                                            {
                                                ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                                            });
            }
            catch (Exception)
            {
            }
            return result;
        }
        public static T FromJson<T>(this string data)
        {
            T result = default(T);
            try
            {
                result = JsonConvert.DeserializeObject<T>(data);
            }
            catch (Exception)
            {
            }
            return result;
        }

        public static T ToDerived<T>(this object baseclass)
        {
            T derived = (T)Activator.CreateInstance(typeof(T));
            foreach (PropertyInfo propBase in baseclass.GetType().GetProperties())
            {
                try
                {
                    PropertyInfo propDerived = typeof(T).GetProperty(propBase.Name);
                    propDerived.SetValue(derived, propBase.GetValue(baseclass, null), null);
                }
                catch (Exception)
                {

                }
            }
            return derived;
        }

        public static string ToXML(this object data)
        {
            XmlSerializer xsSubmit = new XmlSerializer(data.GetType());

            using (StringWriter sww = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(sww))
                {
                    xsSubmit.Serialize(writer, data);

                    return sww.ToString();
                }
            }
        }

        #region Datatable
        public static DataTable ToDataTable<T>(this IEnumerable<T> varlist)
        {
            DataTable dtReturn = new DataTable();

            PropertyInfo[] oProps = null;

            if (varlist == null) return dtReturn;

            foreach (T rec in varlist)
            {
                if (oProps == null)
                {
                    oProps = ((Type)rec.GetType()).GetProperties();
                    foreach (PropertyInfo pi in oProps)
                    {
                        Type colType = pi.PropertyType;

                        if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition() == typeof(Nullable<>)))
                        {
                            colType = colType.GetGenericArguments()[0];
                        }

                        dtReturn.Columns.Add(new DataColumn(pi.Name, colType));
                    }
                }

                DataRow dr = dtReturn.NewRow();

                foreach (PropertyInfo pi in oProps)
                {
                    dr[pi.Name] = pi.GetValue(rec, null) == null ? DBNull.Value : pi.GetValue
                    (rec, null);
                }

                dtReturn.Rows.Add(dr);
            }
            return dtReturn;
        }

        public static DataTable ToRangeDataTable(this DataTable table, int inicio, int fin)
        {
            return new DataTable().ToRangeDataTable(inicio, fin, 1);
        }

        public static DataTable ToRangeDataTable(this DataTable table, int inicio, int fin, int incremento)
        {
            table.Clear();
            table.Columns.Add("0", typeof(string));
            table.Columns.Add("1", typeof(string));
            for (int i = inicio; i < fin + 1; i += incremento)
            {
                table.Rows.Add(i.ToString(), i.ToString());
            }
            return table;
        }

        public static DataTable Invertir(this DataTable inputTable)
        {
            DataTable outputTable = inputTable.Clone();

            for (int i = inputTable.Rows.Count - 1; i >= 0; i--)
            {
                outputTable.ImportRow(inputTable.Rows[i]);
            }

            return outputTable;
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Interfaces;

namespace GSoluard.Administration.Entities
{
    [Serializable]
    public class Pedido : HGSEntity
    {
        #region Construction
        public Pedido()
		{
            Initializer();
        }
        private void Initializer()
        {
			_PedidoDetalles = new List<PedidoDetalle>();
			_Usuario = new Usuario();
			_FormaPago = new FormaPago();
			_Estado = 0;
		}
		#endregion

		#region DataMembers

		private int? _Numero;
		private Usuario _Usuario;
		private List<PedidoDetalle> _PedidoDetalles;
		private FormaPago _FormaPago;
		private int _Estado;
		private DateTime? _Fecha;
		#endregion

		#region Properties


		public int? Numero
		{
		    get { return _Numero; }
		    set { this._Numero = value; }
		}

		public Usuario Usuario
		{
			get { return _Usuario; }
			set { this._Usuario = value; }
		}

		public List<PedidoDetalle> PedidoDetalles
		{
			get { return _PedidoDetalles; }
			set { this._PedidoDetalles = value; }
		}

		public FormaPago FormaPago
		{
			get { return _FormaPago; }
			set { this._FormaPago = value; }
		}

		public int Estado
		{
			get { return _Estado; }
			set { this._Estado = value; }
		}

		public DateTime? Fecha
		{
			get { return _Fecha; }
			set { this._Fecha = value; }
		}
		#endregion

		#region Overrides
		/// <summary>
		/// Returns the string of the object.
		/// </summary>
		/// <returns>String</returns>
		public override String ToString()
		{			
			return String.Format("{0} - {1}", this._Numero, this.Usuario.NombresYApellidos);
		}

		#endregion

    }
}

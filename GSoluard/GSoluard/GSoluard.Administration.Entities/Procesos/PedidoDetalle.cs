﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Interfaces;

namespace GSoluard.Administration.Entities
{
    [Serializable]
    public class PedidoDetalle : HGSEntity
    {
        #region Construction
        public PedidoDetalle()
		{
            Initializer();
        }
        private void Initializer()
        {
			_Producto = new Producto();

		}
		#endregion

		#region DataMembers

		private int _Cantidad;
		private Producto _Producto;
		private float _Precio;
		#endregion

		#region Properties


		public int Cantidad
		{
		    get { return _Cantidad; }
		    set { this._Cantidad = value; }
		}

		public Producto Producto
		{
			get { return _Producto; }
			set { this._Producto = value; }
		}

		public float Precio
		{
			get { return _Precio; }
			set { this._Precio = value; }
		}
		#endregion

		#region Overrides
		/// <summary>
		/// Returns the string of the object.
		/// </summary>
		/// <returns>String</returns>
		public override String ToString()
		{			
			return String.Format("{0} - {1}", this.Cantidad, this.Producto);
		}

		#endregion

    }
}

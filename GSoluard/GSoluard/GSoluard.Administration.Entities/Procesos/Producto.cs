﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Interfaces;

namespace GSoluard.Administration.Entities
{
    [Serializable]
    public class Producto : HGSEntity
    {
        #region Construction
        public Producto()
		{
            Initializer();
        }
        private void Initializer()
        {
			_MateriasPrimas = new List<ProductoMateriaPrima>();
			_Categoria = new Categoria();
		}
		#endregion

		#region DataMembers

		private String _Nombre;
		private String _Descripcion;
		private int _Stock;
		private float _Precio;
		private byte[] _Imagen;
		private Categoria _Categoria;
		private List<ProductoMateriaPrima> _MateriasPrimas;

		#endregion

		#region Properties


		public String Nombre
		{
		    get { return _Nombre; }
		    set { this._Nombre = value; }
		}

		public String Descripcion
		{
			get { return _Descripcion; }
			set { this._Descripcion = value; }
		}

		public int Stock
		{
			get { return _Stock; }
			set { this._Stock = value; }
		}

		public float Precio
		{
			get { return _Precio; }
			set { this._Precio = value; }
		}

		public byte[] Imagen
		{
			get { return _Imagen; }
			set { this._Imagen = value; }
		}


		public Categoria Categoria
		{
			get { return _Categoria; }
			set { this._Categoria = value; }
		}

		public List<ProductoMateriaPrima> MateriasPrimas
		{
			get { return _MateriasPrimas; }
			set { this._MateriasPrimas = value; }
		}


		#endregion

		#region Overrides
		/// <summary>
		/// Returns the string of the object.
		/// </summary>
		/// <returns>String</returns>
		public override String ToString()
		{			
			return String.Format("{0}", this._Nombre);
		}

		#endregion

    }
}

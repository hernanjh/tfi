﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Interfaces;

namespace GSoluard.Administration.Entities
{
    [Serializable]
    public class ProductoMateriaPrima : HGSEntity
    {
        #region Construction
        public ProductoMateriaPrima()
		{
            Initializer();
        }
        private void Initializer()
        {
			_MateriaPrima = new MateriaPrima();

		}
		#endregion

		#region DataMembers

		private int _Cantidad;
		private MateriaPrima _MateriaPrima;
		#endregion

		#region Properties


		public int Cantidad
		{
		    get { return _Cantidad; }
		    set { this._Cantidad = value; }
		}

		public MateriaPrima MateriaPrima
		{
			get { return _MateriaPrima; }
			set { this._MateriaPrima = value; }
		}
		#endregion

		#region Overrides
		/// <summary>
		/// Returns the string of the object.
		/// </summary>
		/// <returns>String</returns>
		public override String ToString()
		{			
			return String.Format("{0} - {1}", this.Cantidad, this.MateriaPrima);
		}

		#endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Interfaces;

namespace GSoluard.Administration.Entities
{
    [Serializable]
    public class Ingreso : HGSEntity
    {
        #region Construction
        public Ingreso()
		{
            Initializer();
        }
        private void Initializer()
        {
			_IngresoDetalles = new List<IngresoDetalle>();
			_Proveedor = new Proveedor();
		}
		#endregion

		#region DataMembers

		private String _Numero;
		private Proveedor _Proveedor;
		private List<IngresoDetalle> _IngresoDetalles;
		private DateTime? _Fecha;
		#endregion

		#region Properties


		public String Numero
		{
		    get { return _Numero; }
		    set { this._Numero = value; }
		}

		public Proveedor Proveedor
		{
			get { return _Proveedor; }
			set { this._Proveedor = value; }
		}

		public List<IngresoDetalle> IngresoDetalles
		{
			get { return _IngresoDetalles; }
			set { this._IngresoDetalles = value; }
		}

		public DateTime? Fecha
		{
			get { return _Fecha; }
			set { this._Fecha = value; }
		}

		#endregion

		#region Overrides
		/// <summary>
		/// Returns the string of the object.
		/// </summary>
		/// <returns>String</returns>
		public override String ToString()
		{			
			return String.Format("{0} - {1}", this._Numero, this.Proveedor);
		}

		#endregion

    }
}

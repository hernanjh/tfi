﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace GSoluard.Administration.Entities
{
   public class HGSContext
    {
        public static Usuario Usuario 
        {
            get
            {
                if (HttpContext.Current.Session["usuariofront"] == null)
                    HttpContext.Current.Session["usuariofront"] = new Usuario();

                return HttpContext.Current.Session["usuariofront"] as Usuario;
            }
            set
            {
                HttpContext.Current.Session["usuariofront"] = value;
            }
        }

        public static Permisos Permisos
        {
            get
            {
                if (HttpContext.Current.Session["permisosfront"] == null)
                    HttpContext.Current.Session["permisosfront"] = new Permisos();

                return HttpContext.Current.Session["permisosfront"] as Permisos;
            }
            set
            {
                HttpContext.Current.Session["permisosfront"] = value;
            }
        }

    }
}

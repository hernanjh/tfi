﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Interfaces;

namespace GSoluard.Administration.Entities
{
    [Serializable]
    public class MateriaPrima : HGSEntity
    {
        #region Construction
        public MateriaPrima()
		{
            Initializer();
        }
        private void Initializer()
        {
        }
		#endregion

		#region DataMembers
		private String _Nombre;
		private String _Descripcion;
		private int _Stock;
		#endregion

		#region Properties

		public String Nombre
		{
			get { return _Nombre; }
			set { this._Nombre = value; }
		}
		public String Descripcion {
		    get { return _Descripcion; }
		    set { this._Descripcion = value; }
		}
		public int Stock
		{
			get { return _Stock; }
			set { this._Stock = value; }
		}
		#endregion

		#region Overrides
		/// <summary>
		/// Returns the string of the object.
		/// </summary>
		/// <returns>String</returns>
		public override String ToString()
		{
			return Nombre.ToString();
		}

		#endregion

    }
}

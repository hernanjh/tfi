﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Interfaces;

namespace GSoluard.Administration.Entities
{
    [Serializable]
    public class Proveedor : HGSEntity
    {
        #region Construction
        public Proveedor()
		{
            Initializer();
        }
        private void Initializer()
        {
        }
		#endregion

		#region DataMembers

		private String _RazonSocial;
		private String _Cuit;
		#endregion

		#region Properties


		public String RazonSocial
		{
		    get { return _RazonSocial; }
		    set { this._RazonSocial = value; }
		}

		public String Cuit
		{
			get { return _Cuit; }
			set { this._Cuit = value; }
		}
		#endregion

		#region Overrides
		/// <summary>
		/// Returns the string of the object.
		/// </summary>
		/// <returns>String</returns>
		public override String ToString()
		{			
			return String.Format("{0} - {1}", this.Cuit, this.RazonSocial);
		}

		#endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Interfaces;

namespace GSoluard.Administration.Entities
{
    [Serializable]
    public class Categoria : HGSEntity
    {
        #region Construction
        public Categoria()
		{
            Initializer();
        }
        private void Initializer()
        {
        }
		#endregion

		#region DataMembers

		private String _Descripcion;
		#endregion

		#region Properties
        

		public String Descripcion {
		    get { return _Descripcion; }
		    set { this._Descripcion = value; }
		}
		#endregion
		
		#region Overrides
		/// <summary>
		/// Returns the string of the object.
		/// </summary>
		/// <returns>String</returns>
		public override String ToString()
		{
			return Descripcion.ToString();
		}

		#endregion

    }
}

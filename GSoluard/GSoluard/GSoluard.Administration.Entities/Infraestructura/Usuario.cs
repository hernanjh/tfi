﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Interfaces;

namespace GSoluard.Administration.Entities
{
    [Serializable]
    public class Usuario : HGSEntity
    {
        #region Construction
        public Usuario() : base()
		{
            Initializer();
        }
        private void Initializer()
        {
            Nick = "";
            Bloqueado = false;
            IsSuperAdmin = false;
            Roles = new List<Rol>();
        }

        #endregion

        #region DataMembers
        private string _Apellido;
        private string _Nombre;
        private string _Nick;
        private bool _Bloqueado;
        private bool _IsSuperAdmin;
        private List<Rol> _roles;
        private string _Clave;
        #endregion

        #region Properties

        public String Apellido
        {
            get { return _Apellido; }
            set { this._Apellido = value; }
        }
        public String Nombre
        {
            get { return _Nombre; }
            set { this._Nombre = value; }
        }
        public String Nick
        {
            get { return _Nick; }
            set { this._Nick = value; }
        }

        public bool Bloqueado
        {
            get { return _Bloqueado; }
            set { this._Bloqueado = value; }
        }

        public bool IsSuperAdmin
        {
            get { return _IsSuperAdmin; }
            set { this._IsSuperAdmin = value; }
        }

        public List<Rol> Roles
        {
            get { return _roles; }
            set { this._roles = value; }
        }

     
        public string Clave
        {
            get { return _Clave; }
            set { _Clave = value; }
        }

        public string NombresYApellidos
        {
            get { return _Apellido + ", " + _Nombre; }

        }
        

        #endregion

        #region Overrides
        /// <summary>
        /// Returns the string of the object.
        /// </summary>
        /// <returns>String</returns>
        public override string ToString()
        {
            return String.Format("({0}) - {1}", this.Nick, NombresYApellidos.ToString());
        }
        #endregion
    }
}

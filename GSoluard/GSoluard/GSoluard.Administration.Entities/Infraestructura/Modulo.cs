﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Interfaces;

namespace GSoluard.Administration.Entities
{
    [Serializable]
    public class Modulo : HGSEntity
    {
		#region Construction
        public Modulo()
		{
		}

		#endregion

        #region DataMembers
        private string _codigo;
        private string _nombre;
        private string _descripcion;
        private string _link;
        private List<Modulo> _modulos; //Modulos hijos
        private int _orden;
        private bool _ismenu;
        #endregion

        #region Properties

        public string Codigo
        {
            get { return _codigo; }
            set { this._codigo = value; }
        }
        public string Nombre
        {
            get { return _nombre; }
            set { this._nombre = value; }
        }
        public string Descripcion
        {
            get { return _descripcion; }
            set { this._descripcion = value; }
        }
        public string Link
        {
            get { return _link; }
            set { this._link = value; }
        }
        public int Orden
        {
            get { return _orden; }
            set { this._orden = value; }
        }
        public bool IsMenu
        {
            get { return _ismenu; }
            set { this._ismenu = value; }
        }

        public List<Modulo> Modulos
        {
            get { return _modulos; }
            set { this._modulos = value; }
        }


        #endregion

        #region Overrides
        /// <summary>
        /// Returns the string of the object.
        /// </summary>
        /// <returns>String</returns>
        public override String ToString()
        {
            return String.Format("({0}) {1}",this.Codigo, this.Nombre);
        }

        #endregion

    }
}

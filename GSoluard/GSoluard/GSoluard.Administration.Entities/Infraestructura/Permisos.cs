﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GSoluard.Administration.Entities
{
    public class Permisos
    {
        #region Construction
        public Permisos()
        {
            Initializer();
        }
        private void Initializer()
        {
        }

        #endregion

        #region DataMembers
        private List<Modulo> _modulos;
        #endregion

        #region Properties
        public List<Modulo> Modulos
        {
            get { return _modulos; }
            set { this._modulos = value; }
        }

        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Interfaces;

namespace GSoluard.Administration.Entities
{
    [Serializable]
    public class Rol : HGSEntity
    {
        #region Construction
        public Rol()
        {
            Initializer();
        }
        private void Initializer()
        {
            _modulos = new List<Modulo>();
        }

        #endregion

        #region DataMembers
        private string _codigo;
        private string _nombre;
        private List<Modulo> _modulos;
        #endregion

        #region Properties

        public string Codigo
        {
            get { return _codigo; }
            set { this._codigo = value; }
        }
        public string Nombre
        {
            get { return _nombre; }
            set { this._nombre = value; }
        }
        public List<Modulo> Modulos
        {
            get { return _modulos; }
            set { this._modulos = value; }
        }

        public bool Contains (Modulo modulo)
        {
            return _modulos.Contains(modulo);
        }
        #endregion
    }
}

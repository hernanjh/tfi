﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace GSoluard.Interfaces
{
    [Serializable]
    public class HGSEntity
    {
        int _oid = 0;
        string _odescription = "";
        EstadoPersistencia _estadoPersistencia = EstadoPersistencia.INSERT;
        bool _activo = true;

        public HGSEntity()
        {

        }

        public int OID
        {
            get { return _oid; }
            set { _oid = value; }
        }

        public string ODescription
        {
            get { return _odescription; }
            set { _odescription = value; }
        }
        public bool Activo
        {
            get { return _activo; }
            set { _activo = value; }
        }

        public EstadoPersistencia Persistencia
        {
            get { return _estadoPersistencia; }
            set { _estadoPersistencia = value; }
        }
        
        public override bool Equals(object obj)
        {
            if (!(obj is HGSEntity))
                return false;

            if (((HGSEntity)obj).OID == this.OID)
                return true;

            return false;
        }
    }

    [Serializable]
    public enum EstadoPersistencia
    {
        INSERT = 1,
        UPDATE = 2,
        DELETE = 3,
        UNDELETE = 4
    };
}

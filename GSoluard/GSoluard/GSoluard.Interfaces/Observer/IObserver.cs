﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GSoluard.Interfaces.Observer
{
    public interface IObserver
    {
        void RunEvent();
    }
}

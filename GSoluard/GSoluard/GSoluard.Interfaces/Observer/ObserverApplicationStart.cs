﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GSoluard.Interfaces.Observer
{
    public class ObserverApplicationStart : Observable
    {
        private static ObserverApplicationStart instance;

        private ObserverApplicationStart() { }

        public static ObserverApplicationStart Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ObserverApplicationStart();
                }
                return instance;
            }
        }
    }
}

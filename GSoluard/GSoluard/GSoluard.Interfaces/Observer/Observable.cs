﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GSoluard.Interfaces.Observer
{
    public abstract class Observable
    {
        private List<IObserver> observers = new List<IObserver>();

        public void Subscribe(IObserver obs)
        {
            observers.Add(obs);
        }

        public void UnSubscribe(IObserver obs)
        {
            observers.Remove(obs);
        }

        public virtual void Notify()
        {
            foreach (IObserver o in observers)
            {
                o.RunEvent();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GSoluard.Interfaces.Observer
{
    public class ObserverSessionEnd : Observable
    {
        private static ObserverSessionEnd instance;

        private ObserverSessionEnd() { }

        public static ObserverSessionEnd Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ObserverSessionEnd();
                }
                return instance;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GSoluard.Interfaces
{
    public class HGSException : Exception
    {
        private HGSException()
        {

        }
        public string Codigo { get; set; }
        public bool FatalError { get; set; }

        public HGSException(string code, string msg, Exception inner)
            : base(msg, inner)
        {
            Codigo = code;
            FatalError = false;
        }
    }
}

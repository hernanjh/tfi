﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace GSoluard.Interfaces
{
    public class HttpHelper
    {
        public static string Get(string key)
        {
            object value = HttpContext.Current.Request.QueryString[key];
            return (value == null) ? null : value.ToString();
        }


        public static string Post(string key)
        {
            object value = HttpContext.Current.Request.Form[key];
            return (value == null) ? null : value.ToString();
        }

        public static T Session<T>(string key)
        {
            object value = HttpContext.Current.Session[key];

            if (value is T)
                return (T)value;
            else
                return default(T);           
        }

        public static void ClearSession(string key)
        {
            HttpContext.Current.Session[key] = null;
        }

        public static void StoreInSession(string key, object value)
        {
            HttpContext.Current.Session[key] = value;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Administration.Entities;
using GSoluard.Interfaces;
using GSoluard.DAO;
using System.Data;
using GSoluard.Administration.Exceptions;

namespace GSoluard.Administration.Engine.Parametrizacion
{
    public class GestorCategoria
    {

        public static Categoria Obtener(int OID)
        {
            try
            {
                IRepository repo = RepositoryFactory.Create(Config.RepositoryName);
                repo.Nombre = "CATEGORIA_SP";
                repo.AddParameter("@ACCION", "BYID");
                repo.AddParameter("@ID", OID);
                DataTable dt = repo.ExecuteDataTable();
                Categoria result = dt.ToGenericList<Categoria>(CategoriaConverter).FirstOrDefault();
                return result;
            }
            catch (Exception ex)
            {
                throw new CategoriaGetException(ex);
            }
        }

        public static void Guardar(Categoria obj)
        {
            try
            {
                IRepository repo = RepositoryFactory.CreateTransactional(Config.RepositoryName);
                repo.Nombre = "CATEGORIA_PROC";
                repo.AddParameter("@ACCION", obj.Persistencia);
                repo.AddParameter("@ID", obj.OID);
                repo.AddParameter("@DESCRIPCION", obj.Descripcion);
                repo.AddParameter("@IDUSUARIO", HGSContext.Usuario.OID);
                obj.OID = repo.ExecuteReturnValue().ToInteger();
                repo.Commit();


            }
            catch (Exception ex)
            {
                throw new CategoriaSaveException(ex);
            }
        }


        public static void Eliminar(Categoria obj)
        {
            obj.Persistencia = EstadoPersistencia.DELETE;
            Guardar(obj);
            obj.Persistencia = EstadoPersistencia.UPDATE;
            obj.Activo = false;
        }
        public static void ReActivar(Categoria obj)
        {
            obj.Persistencia = EstadoPersistencia.UNDELETE;
            Guardar(obj);
            obj.Persistencia = EstadoPersistencia.UPDATE;
            obj.Activo = true;
        }


        public static List<Categoria> Consultar(string descripcion, bool? activo)
        {
            try
            {
                IRepository repo = RepositoryFactory.Create(Config.RepositoryName);
                repo.Nombre = "CATEGORIA_SP";
                repo.AddParameter("@ACCION", "CONSULTA");
                repo.AddParameter("@ID", null);
                repo.AddParameter("@DESCRIPCION", descripcion);
                repo.AddParameter("@ACTIVO", activo);
                DataTable dt = repo.ExecuteDataTable();
                List<Categoria> result = dt.ToGenericList<Categoria>(CategoriaConverter);
                return result;
            }
            catch (Exception ex)
            {
                throw new CategoriaGetException(ex);
            }
        }

        public static List<Categoria> Consultar()
        {
            return Consultar(null, null);
        }




        #region Converter
        public static Categoria CategoriaConverter(DataRow row)
        {

            return new Categoria
            {
                OID = row["catID"].ToInteger(),
                Persistencia = EstadoPersistencia.UPDATE,
                Descripcion = row["catDESCRIPCION"].ToString(),
                Activo = row.IsNull("FECHABAJA") ? true : false
            };
        }
        #endregion


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Administration.Entities;
using GSoluard.Interfaces;
using GSoluard.DAO;
using System.Data;
using GSoluard.Administration.Exceptions;

namespace GSoluard.Administration.Engine.Parametrizacion
{
    public class GestorFormaPago
    {

        public static FormaPago Obtener(int OID)
        {
            try
            {
                IRepository repo = RepositoryFactory.Create(Config.RepositoryName);
                repo.Nombre = "FORMAPAGO_SP";
                repo.AddParameter("@ACCION", "BYID");
                repo.AddParameter("@ID", OID);
                DataTable dt = repo.ExecuteDataTable();
                FormaPago result = dt.ToGenericList<FormaPago>(FormaPagoConverter).FirstOrDefault();
                return result;
            }
            catch (Exception ex)
            {
                throw new FormaPagoGetException(ex);
            }
        }

        public static void Guardar(FormaPago obj)
        {
            try
            {
                IRepository repo = RepositoryFactory.CreateTransactional(Config.RepositoryName);
                repo.Nombre = "FORMAPAGO_PROC";
                repo.AddParameter("@ACCION", obj.Persistencia);
                repo.AddParameter("@ID", obj.OID);
                repo.AddParameter("@DESCRIPCION", obj.Descripcion);
                repo.AddParameter("@IDUSUARIO", HGSContext.Usuario.OID);
                obj.OID = repo.ExecuteReturnValue().ToInteger();
                repo.Commit();


            }
            catch (Exception ex)
            {
                throw new FormaPagoSaveException(ex);
            }
        }


        public static void Eliminar(FormaPago obj)
        {
            obj.Persistencia = EstadoPersistencia.DELETE;
            Guardar(obj);
            obj.Persistencia = EstadoPersistencia.UPDATE;
            obj.Activo = false;
        }
        public static void ReActivar(FormaPago obj)
        {
            obj.Persistencia = EstadoPersistencia.UNDELETE;
            Guardar(obj);
            obj.Persistencia = EstadoPersistencia.UPDATE;
            obj.Activo = true;
        }


        public static List<FormaPago> Consultar(string descripcion, bool? activo)
        {
            try
            {
                IRepository repo = RepositoryFactory.Create(Config.RepositoryName);
                repo.Nombre = "FORMAPAGO_SP";
                repo.AddParameter("@ACCION", "CONSULTA");
                repo.AddParameter("@ID", null);
                repo.AddParameter("@DESCRIPCION", descripcion);
                repo.AddParameter("@ACTIVO", activo);
                DataTable dt = repo.ExecuteDataTable();
                List<FormaPago> result = dt.ToGenericList<FormaPago>(FormaPagoConverter);
                return result;
            }
            catch (Exception ex)
            {
                throw new FormaPagoGetException(ex);
            }
        }

        public static List<FormaPago> Consultar()
        {
            return Consultar(null, null);
        }




        #region Converter
        public static FormaPago FormaPagoConverter(DataRow row)
        {

            return new FormaPago
            {
                OID = row["fpID"].ToInteger(),
                Persistencia = EstadoPersistencia.UPDATE,
                Descripcion = row["fpDESCRIPCION"].ToString(),
                Activo = row.IsNull("FECHABAJA") ? true : false
            };
        }
        #endregion


    }
}

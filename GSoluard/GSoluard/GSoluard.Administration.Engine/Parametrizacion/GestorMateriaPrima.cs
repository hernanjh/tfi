﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Administration.Entities;
using GSoluard.Interfaces;
using GSoluard.DAO;
using System.Data;
using GSoluard.Administration.Exceptions;

namespace GSoluard.Administration.Engine.Parametrizacion
{
    public class GestorMateriaPrima
    {

        public static MateriaPrima Obtener(int OID)
        {
            try
            {
                IRepository repo = RepositoryFactory.Create(Config.RepositoryName);
                repo.Nombre = "MATERIAPRIMA_SP";
                repo.AddParameter("@ACCION", "BYID");
                repo.AddParameter("@ID", OID);
                DataTable dt = repo.ExecuteDataTable();
                MateriaPrima result = dt.ToGenericList<MateriaPrima>(MateriaPrimaConverter).FirstOrDefault();
                return result;
            }
            catch (Exception ex)
            {
                throw new MateriaPrimaGetException(ex);
            }
        }

        public static void Guardar(MateriaPrima obj)
        {
            try
            {
                IRepository repo = RepositoryFactory.CreateTransactional(Config.RepositoryName);
                repo.Nombre = "MATERIAPRIMA_PROC";
                repo.AddParameter("@ACCION", obj.Persistencia);
                repo.AddParameter("@ID", obj.OID);
                repo.AddParameter("@NOMBRE", obj.Nombre);
                repo.AddParameter("@DESCRIPCION", obj.Descripcion);
                repo.AddParameter("@STOCK", obj.Stock);
                repo.AddParameter("@IDUSUARIO", HGSContext.Usuario.OID);
                obj.OID = repo.ExecuteReturnValue().ToInteger();
                repo.Commit();


            }
            catch (Exception ex)
            {
                throw new MateriaPrimaSaveException(ex);
            }
        }

        public static void ActualizarSotck(int idmateriaprima, int cantidad)
        {
            try
            {
                IRepository repo = RepositoryFactory.CreateTransactional(Config.RepositoryName);
                repo.Nombre = "MATERIAPRIMA_PROC";
                repo.AddParameter("@ACCION", "UPD_STOCK");
                repo.AddParameter("@ID", idmateriaprima);
                repo.AddParameter("@STOCK", cantidad);
                repo.ExecuteNonQuery();
                repo.Commit();


            }
            catch (Exception ex)
            {
                throw new MateriaPrimaSaveException(ex);
            }
        }

        public static void Eliminar(MateriaPrima obj)
        {
            obj.Persistencia = EstadoPersistencia.DELETE;
            Guardar(obj);
            obj.Persistencia = EstadoPersistencia.UPDATE;
            obj.Activo = false;
        }
        public static void ReActivar(MateriaPrima obj)
        {
            obj.Persistencia = EstadoPersistencia.UNDELETE;
            Guardar(obj);
            obj.Persistencia = EstadoPersistencia.UPDATE;
            obj.Activo = true;
        }


        public static List<MateriaPrima> Consultar(string nombre, string descripcion, bool? activo)
        {
            try
            {
                IRepository repo = RepositoryFactory.Create(Config.RepositoryName);
                repo.Nombre = "MATERIAPRIMA_SP";
                repo.AddParameter("@ACCION", "CONSULTA");
                repo.AddParameter("@ID", null);
                repo.AddParameter("@NOMBRE", nombre);
                repo.AddParameter("@DESCRIPCION", descripcion);
                repo.AddParameter("@ACTIVO", activo);
                DataTable dt = repo.ExecuteDataTable();
                List<MateriaPrima> result = dt.ToGenericList<MateriaPrima>(MateriaPrimaConverter);
                return result;
            }
            catch (Exception ex)
            {
                throw new MateriaPrimaGetException(ex);
            }
        }

        public static List<MateriaPrima> Consultar()
        {
            return Consultar(null, null, null);
        }


        public static void IncrementarStock(int idmateriaprima, int cantidad)
        {
            ActualizarSotck(idmateriaprima, cantidad);
        }

        public static void DecrementarStock(int idmateriaprima, int cantidad)
        {
            ActualizarSotck(idmateriaprima, -cantidad);
        }

        #region Converter
        public static MateriaPrima MateriaPrimaConverter(DataRow row)
        {

            return new MateriaPrima
            {
                OID = row["mpID"].ToInteger(),
                Persistencia = EstadoPersistencia.UPDATE,
                Nombre = row["mpNOMBRE"].ToString(),
                Descripcion = row["mpDESCRIPCION"].ToString(),
                Stock = row["mpSTOCK"].ToInteger(),
                Activo = row.IsNull("FECHABAJA") ? true : false
            };
        }
        #endregion


    }
}

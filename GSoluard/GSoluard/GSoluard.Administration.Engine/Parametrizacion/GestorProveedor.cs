﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Administration.Entities;
using GSoluard.Interfaces;
using GSoluard.DAO;
using System.Data;
using GSoluard.Administration.Exceptions;

namespace GSoluard.Administration.Engine.Parametrizacion
{
    public class GestorProveedor
    {

        public static Proveedor Obtener(int OID)
        {
            try
            {
                IRepository repo = RepositoryFactory.Create(Config.RepositoryName);
                repo.Nombre = "PROVEEDOR_SP";
                repo.AddParameter("@ACCION", "BYID");
                repo.AddParameter("@ID", OID);
                DataTable dt = repo.ExecuteDataTable();
                Proveedor result = dt.ToGenericList<Proveedor>(Converter).FirstOrDefault();
                return result;
            }
            catch (Exception ex)
            {
                throw new ProveedorGetException(ex);
            }
        }

        public static void Guardar(Proveedor obj)
        {
            try
            {
                IRepository repo = RepositoryFactory.CreateTransactional(Config.RepositoryName);
                repo.Nombre = "PROVEEDOR_PROC";
                repo.AddParameter("@ACCION", obj.Persistencia);
                repo.AddParameter("@ID", obj.OID);
                repo.AddParameter("@RAZONSOCIAL", obj.RazonSocial);
                repo.AddParameter("@CUIT", obj.Cuit);
                repo.AddParameter("@IDUSUARIO", HGSContext.Usuario.OID);
                obj.OID = repo.ExecuteReturnValue().ToInteger();
                repo.Commit();


            }
            catch (Exception ex)
            {
                throw new ProveedorSaveException(ex);
            }
        }


        public static void Eliminar(Proveedor obj)
        {
            obj.Persistencia = EstadoPersistencia.DELETE;
            Guardar(obj);
            obj.Persistencia = EstadoPersistencia.UPDATE;
            obj.Activo = false;
        }
        public static void ReActivar(Proveedor obj)
        {
            obj.Persistencia = EstadoPersistencia.UNDELETE;
            Guardar(obj);
            obj.Persistencia = EstadoPersistencia.UPDATE;
            obj.Activo = true;
        }


        public static List<Proveedor> Consultar(string razonsocial, string cuit, bool? activo)
        {
            try
            {
                IRepository repo = RepositoryFactory.Create(Config.RepositoryName);
                repo.Nombre = "PROVEEDOR_SP";
                repo.AddParameter("@ACCION", "CONSULTA");
                repo.AddParameter("@ID", null);
                repo.AddParameter("@RAZONSOCIAL", razonsocial);
                repo.AddParameter("@CUIT", cuit);
                repo.AddParameter("@ACTIVO", activo);
                DataTable dt = repo.ExecuteDataTable();
                List<Proveedor> result = dt.ToGenericList<Proveedor>(Converter);
                return result;
            }
            catch (Exception ex)
            {
                throw new ProveedorGetException(ex);
            }
        }

        public static List<Proveedor> Consultar()
        {
            return Consultar(null, null, null);
        }




        #region Converter
        public static Proveedor Converter(DataRow row)
        {

            return new Proveedor
            {
                OID = row["pvID"].ToInteger(),
                Persistencia = EstadoPersistencia.UPDATE,
                RazonSocial = row["pvRAZONSOCIAL"].ToString(),
                Cuit = row["pvCUIT"].ToString(),
                Activo = row.IsNull("FECHABAJA") ? true : false
            };
        }
        #endregion


    }
}

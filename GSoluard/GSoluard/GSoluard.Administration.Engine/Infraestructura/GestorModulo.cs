﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Administration.Entities;
using GSoluard.Interfaces;
using GSoluard.DAO;
using System.Data;
using GSoluard.Administration.Exceptions;

namespace GSoluard.Administration.Engine
{
    public class GestorModulo
    {
        /// <summary>
        /// Permite obtener el Modulo segun su identificador
        /// </summary>
        /// <param name="OID">Identificador</param>
        /// <returns>Modulo</returns>
        public static Modulo Obtener(int OID)
        {
            /*
            try
            {
                IRepository repo = RepositoryFactory.Create(Config.RepositoryName);
                repo.Nombre = "BANCOS_SP";
                repo.AddParameter("@ACCION", "BYID");
                repo.AddParameter("@ID", OID);
                DataTable dt = repo.ExecuteDataTable();
                Banco result = dt.ToGenericList<Banco>(BancoConverter).FirstOrDefault();
                return result;
            }
            catch (Exception ex)
            {
                throw new BancoGetException(ex);
            }*/

            return new Modulo();
        }

        /// <summary>
        /// Permite obtener el Modulo segun la url de la pagina
        /// </summary>
        /// <param name="OID">Identificador</param>
        /// <returns>Modulo</returns>
        public static Modulo Obtener(string url)
        {            
            try
            {
                IRepository repo = RepositoryFactory.Create(Config.RepositoryName);
                repo.Nombre = "MODULOS_PAGINAS_PROC";
                repo.AddParameter("@ACCION", "BYURL");
                repo.AddParameter("@MODULO", null);
                repo.AddParameter("@PAGINA", url);
                DataTable dt = repo.ExecuteDataTable();
                Modulo result = dt.ToGenericList<Modulo>(ModuloConverter).FirstOrDefault();
                return result;
            }
            catch (Exception ex)
            {
                throw new ModuloGetException(ex);
            }
        }

        /// <summary>
        /// Permite obtener todos los modulos
        /// </summary>
        /// <returns>Lista de Modulos</returns>
        public static List<Modulo> ObtenerTodos()
        {
            return ObtenerTodos(null);
        }

        
        private static List<Modulo> ObtenerTodos(Modulo modulo)
        {
            try
            {
                IRepository repo = RepositoryFactory.Create(Config.RepositoryName);
                repo.Nombre = "MODULOS_SP";
                repo.AddParameter("@ACCION", "TODOS");
                repo.AddParameter("@IDMODULO", (modulo == null) ? 0 : modulo.OID);
                DataTable dt = repo.ExecuteDataTable();
                List<Modulo> result = dt.ToGenericList<Modulo>(ModuloCompleteConverter);
                return result;
            }
            catch (Exception ex)
            {
                throw new ModuloGetException(ex);
            }
        }

        private static List<string> ObtenerPaginas()
        {
            try
            {
                IRepository repo = RepositoryFactory.Create(Config.RepositoryName);
                repo.Nombre = "MODULOS_PAGINAS_PROC";
                repo.AddParameter("@ACCION", "PAGINAS");
                DataTable dt = repo.ExecuteDataTable();
                List<string> result = dt.ToGenericList<string>(PaginaConverter);
                return result;
            }
            catch (Exception ex)
            {
                throw new ModuloGetException(ex);
            }
        }

        private static void EliminarPagina(string url)
        {
            try
            {
                IRepository repo = RepositoryFactory.Create(Config.RepositoryName);
                repo.Nombre = "MODULOS_PAGINAS_PROC";
                repo.AddParameter("@ACCION", "3");
                repo.AddParameter("@MODULO", null);
                repo.AddParameter("@PAGINA", url);
                repo.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new ModuloSetException(ex);
            }
        }

        /// <summary>
        /// Agrega el modulo perteneciente a cada pagina del diccionario
        /// </summary>
        /// <param name="pages">Dictionary<string, Modulo> pages</param>
        public static void SetModulosPages(Dictionary<string, Modulo> pages)
        {
            List<string> paginas = ObtenerPaginas();
            List<string> keys = new List<string>(pages.Keys);

            //Elimina las paginas que ya no esten en el diccionario
            List<string> fordelete = paginas.Except(keys).ToList(); 
            foreach (string url in fordelete)
            {
                EliminarPagina(url);
            }

            //Obtiene el modulo de cada pagina, si la misma no existe la inserta con modulo null
            foreach (string key in keys)
            {
                pages[key] = Obtener(key);
            }
        }


        #region Converter

        public static Modulo ModuloCompleteConverter(DataRow row)
        {
            Modulo modulo = GestorModulo.ModuloConverter(row);
            modulo.Modulos = ObtenerTodos(modulo);
            return modulo;
        }

        public static Modulo ModuloConverter(DataRow row)
        {
            return new Modulo
            {
                OID = row["modID"].ToInteger(),
                Persistencia = EstadoPersistencia.UPDATE,
                Codigo = row["modCODIGO"].ToString(),
                Nombre = row["modNOMBRE"].ToString(),
                Descripcion = row["modDESCRIPCION"].ToString(),
                Link = row["modLINK"].ToString(),
                Orden = row["modORDEN"].ToInteger(),
                IsMenu = row["modISMENU"].ToBoolean()
            };
        }

        public static string PaginaConverter(DataRow row)
        {
            return row["mpPAGINA"].ToString();

        }
        #endregion
    }
}

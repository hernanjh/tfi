﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Administration.Entities;
using System.Data;
using GSoluard.Interfaces;
using GSoluard.DAO;
using GSoluard.Administration.Exceptions;

namespace GSoluard.Administration.Engine
{
    public class GestorPermisos
    {
        /// <summary>
        /// Permite obtener los modulos de un usuario
        /// </summary>
        /// <param name="usuario">Usuario</param>
        /// <returns>Lista de modulos</returns>
        public static List<Modulo> Obtener(Usuario usuario)
        {            
            return ObtenerModulos(usuario, null, false);
        }

        public static List<Modulo> Obtener(Usuario usuario, bool pub)
        {
            return ObtenerModulos(usuario, null, pub);
        }

        static List<Modulo> ObtenerModulos(Usuario usuario, Modulo modulo, bool pub)
        {
            try
            {
                IRepository repo = RepositoryFactory.Create(Config.RepositoryName);
                repo.Nombre = "PERMISOS_SP";
                repo.AddParameter("@ACCION", "BYUSER");
                repo.AddParameter("@IDUSUARIO", usuario.OID);
                repo.AddParameter("@IDMODULO", (modulo == null) ? 0 : modulo.OID);
                repo.AddParameter("@PUBLIC", pub);
                DataTable dt = repo.ExecuteDataTable();
                List<Modulo> result = (from row in dt.AsEnumerable()
                                        select PermisoConverter(usuario, row, pub)).ToList();
                return result;
            }
            catch (Exception ex)
            {
                throw new RolGetException(ex);
            }
        }

        #region Converter
        public static Modulo PermisoConverter(Usuario usuario, DataRow row, bool pub)
        {
            Modulo modulo = GestorModulo.ModuloConverter(row);
            modulo.Modulos = ObtenerModulos(usuario, modulo, pub);
            return modulo;
        }

        #endregion
    }
}

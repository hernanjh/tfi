﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Administration.Entities;
using GSoluard.Interfaces;
using GSoluard.DAO;
using System.Data;
using GSoluard.Administration.Exceptions;
using GSoluard.Administration.Engine.Parametrizacion;

namespace GSoluard.Administration.Engine.Procesos
{
    public class GestorPedidoDetalle
    {

        public static PedidoDetalle Obtener(int OID)
        {
            try
            {
                IRepository repo = RepositoryFactory.Create(Config.RepositoryName);
                repo.Nombre = "PEDIDODETALLE_SP";
                repo.AddParameter("@ACCION", "BYID");
                repo.AddParameter("@ID", OID);
                DataTable dt = repo.ExecuteDataTable();
                PedidoDetalle result = dt.ToGenericList<PedidoDetalle>(Converter).FirstOrDefault();
                return result;
            }
            catch (Exception ex)
            {
                throw new PedidoDetalleGetException(ex);
            }
        }

        public static void Guardar(Pedido obj)
        {
            try
            {
                /*IRepository repo = RepositoryFactory.CreateTransactional(Config.RepositoryName);
                repo.Nombre = "INGRESOSDETALLE_PROC";
                repo.AddParameter("@ACCION", "DELETEALL");
                repo.AddParameter("@IDINGRESO", obj.OID);
                repo.ExecuteNonQuery();
                repo.Commit();*/

                foreach (PedidoDetalle detalle in obj.PedidoDetalles)
                {
                    Guardar(obj.OID, detalle);
                }

            }
            catch (Exception ex)
            {
                throw new PedidoDetalleSaveException(ex);
            }
        }

        public static void Guardar(int? idpedido, PedidoDetalle obj)
        {
            try
            {
                //resto el stock de la materia prima modificada o borrada
                /*if (obj.Persistencia == EstadoPersistencia.DELETE || obj.Persistencia == EstadoPersistencia.UPDATE)
                {
                    PedidoDetalle orig = Obtener(obj.OID);
                    GestorMateriaPrima.DecrementarStock(orig.MateriaPrima.OID, orig.Cantidad);
                }*/

                IRepository repo = RepositoryFactory.CreateTransactional(Config.RepositoryName);
                repo.Nombre = "PEDIDODETALLE_PROC";
                repo.AddParameter("@ACCION", obj.Persistencia);
                repo.AddParameter("@ID", obj.OID);
                repo.AddParameter("@IDPEDIDO", idpedido);
                repo.AddParameter("@IDPRODUCTO", obj.Producto.OID);
                repo.AddParameter("@CANTIDAD", obj.Cantidad);
                repo.AddParameter("@PRECIO", obj.Precio);
                obj.OID = repo.ExecuteReturnValue().ToInteger();
                repo.Commit();

               /* if (obj.Persistencia == EstadoPersistencia.INSERT || obj.Persistencia == EstadoPersistencia.UPDATE)
                    GestorMateriaPrima.IncrementarStock(obj.MateriaPrima.OID, obj.Cantidad);*/
            }
            catch (Exception ex)
            {
                throw new PedidoDetalleSaveException(ex);
            }
        }


        public static void Eliminar(PedidoDetalle obj)
        {
            obj.Persistencia = EstadoPersistencia.DELETE;
            Guardar(null, obj);
            obj.Persistencia = EstadoPersistencia.UPDATE;
            obj.Activo = false;
        }


        public static void ReActivar(PedidoDetalle obj)
        {
            obj.Persistencia = EstadoPersistencia.UNDELETE;
            Guardar(null, obj);
            obj.Persistencia = EstadoPersistencia.UPDATE;
            obj.Activo = true;
        }

        public static List<PedidoDetalle> Consultar(int? idpedido, int? idproducto)
        {
            try
            {
                IRepository repo = RepositoryFactory.Create(Config.RepositoryName);
                repo.Nombre = "PEDIDODETALLE_SP";
                repo.AddParameter("@ACCION", "CONSULTA");
                repo.AddParameter("@ID", null);
                repo.AddParameter("@IDPEDIDO", idpedido);
                repo.AddParameter("@IDPRODUCTO", idproducto);
                repo.AddParameter("@CANTIDAD", null);
                repo.AddParameter("@PRECIO", null);
                DataTable dt = repo.ExecuteDataTable();
                List<PedidoDetalle> result = dt.ToGenericList<PedidoDetalle>(Converter);
                return result;
            }
            catch (Exception ex)
            {
                throw new IngresoDetalleGetException(ex);
            }
        }

        public static List<PedidoDetalle> ObtenerxPedido(int idpedido)
        {
            return Consultar(idpedido, null);
        }

        public static List<PedidoDetalle> Activos(Pedido entidad)
        {
            try
            {
                return entidad.PedidoDetalles.Where(x => x.Persistencia != EstadoPersistencia.DELETE).ToList();
            }
            catch (Exception ex)
            {
                throw new PedidoDetalleGetException(ex);
            }
        }

        #region Converter
        public static PedidoDetalle Converter(DataRow row)
        {

            return new PedidoDetalle
            {
                OID = row["PdID"].ToInteger(),
                Persistencia = EstadoPersistencia.UPDATE,
                Cantidad = row["PdCANTIDAD"].ToInteger(),
                Producto = GestorProducto.Obtener(row["pdIDPRODUCTO"].ToInteger()),
                Precio = row["pdPRECIOU"].ToFloat(),
                Activo = true
            };
        }
        #endregion


    }
}

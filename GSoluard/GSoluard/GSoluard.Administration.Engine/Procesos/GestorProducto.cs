﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Administration.Entities;
using GSoluard.Interfaces;
using GSoluard.DAO;
using System.Data;
using GSoluard.Administration.Exceptions;
using GSoluard.Administration.Engine.Parametrizacion;

namespace GSoluard.Administration.Engine.Procesos
{
    public class GestorProducto
    {

        public static Producto Obtener(int OID)
        {
            try
            {
                IRepository repo = RepositoryFactory.Create(Config.RepositoryName);
                repo.Nombre = "PRODUCTO_SP";
                repo.AddParameter("@ACCION", "BYID");
                repo.AddParameter("@ID", OID);
                DataTable dt = repo.ExecuteDataTable();
                Producto result = dt.ToGenericList<Producto>(Converter).FirstOrDefault();
                return result;
            }
            catch (Exception ex)
            {
                throw new ProductoGetException(ex);
            }
        }

        public static void Guardar(Producto obj)
        {
            try
            {
                IRepository repo = RepositoryFactory.CreateTransactional(Config.RepositoryName);
                repo.Nombre = "PRODUCTO_PROC";
                repo.AddParameter("@ACCION", obj.Persistencia);
                repo.AddParameter("@ID", obj.OID);
                repo.AddParameter("@NOMBRE", obj.Nombre);
                repo.AddParameter("@DESCRIPCION", obj.Descripcion);
                repo.AddParameter("@STOCK", obj.Stock);
                repo.AddParameter("@PRECIO", obj.Precio);
                repo.AddParameter("@IMAGEN", obj.Imagen);
                repo.AddParameter("@IDCATEGORIA", obj.Categoria.OID);
                obj.OID = repo.ExecuteReturnValue().ToInteger();
                repo.Commit();

                GestorProductoMateriaPrima.Guardar(obj);

            }
            catch (Exception ex)
            {
                throw new ProductoSaveException(ex);
            }
        }


        public static void Eliminar(Producto obj)
        {
            obj.Persistencia = EstadoPersistencia.DELETE;
            Guardar(obj);
            obj.Persistencia = EstadoPersistencia.UPDATE;
            obj.Activo = false;
        }
        public static void ReActivar(Producto obj)
        {
            obj.Persistencia = EstadoPersistencia.UNDELETE;
            Guardar(obj);
            obj.Persistencia = EstadoPersistencia.UPDATE;
            obj.Activo = true;
        }


        public static List<Producto> Consultar(string nombre)
        {
            try
            {
                IRepository repo = RepositoryFactory.Create(Config.RepositoryName);
                repo.Nombre = "PRODUCTO_SP";
                repo.AddParameter("@ACCION", "CONSULTA");
                repo.AddParameter("@ID", null);
                repo.AddParameter("@NOMBRE", nombre);
                DataTable dt = repo.ExecuteDataTable();
                List<Producto> result = dt.ToGenericList<Producto>(Converter);
                return result;
            }
            catch (Exception ex)
            {
                throw new ProductoGetException(ex);
            }
        }

        public static List<Producto> Consultar()
        {
            return Consultar(null);
        }

        public static void ActualizarSotck(int id, int cantidad)
        {
            try
            {
                IRepository repo = RepositoryFactory.CreateTransactional(Config.RepositoryName);
                repo.Nombre = "PRODUCTO_PROC";
                repo.AddParameter("@ACCION", "UPD_STOCK");
                repo.AddParameter("@ID", id);
                repo.AddParameter("@STOCK", cantidad);
                repo.ExecuteNonQuery();
                repo.Commit();


            }
            catch (Exception ex)
            {
                throw new ProductoSaveException(ex);
            }
        }

        public static void IncrementarStock(int id, int cantidad)
        {
            ActualizarSotck(id, cantidad);
        }

        public static void DecrementarStock(int id, int cantidad)
        {
            ActualizarSotck(id, -cantidad);
        }

        #region Converter
        public static Producto Converter(DataRow row)
        {

            return new Producto
            {
                OID = row["prodID"].ToInteger(),
                Persistencia = EstadoPersistencia.UPDATE,
                Nombre = row["prodNOMBRE"].ToString(),
                Descripcion = row["prodDESCRIPCION"].ToString(),
                Stock = row["prodSTOCK"].ToInteger(),
                Precio = row["prodPRECIO"].ToFloat(),
                Imagen = row["prodIMAGEN"].ToBytes(),
                Categoria = GestorCategoria.Obtener(row["prodIDCATEGORIA"].ToInteger()),
                MateriasPrimas = GestorProductoMateriaPrima.ObtenerxProducto(row["prodID"].ToInteger()),
                Activo = true
            };
        }
        #endregion


    }
}

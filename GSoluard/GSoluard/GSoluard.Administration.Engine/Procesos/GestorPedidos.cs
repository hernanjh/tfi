﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Administration.Entities;
using GSoluard.Interfaces;
using GSoluard.DAO;
using System.Data;
using GSoluard.Administration.Exceptions;
using GSoluard.Administration.Engine.Parametrizacion;

namespace GSoluard.Administration.Engine.Procesos
{
    public class GestorPedidos
    {

        public static Pedido Obtener(int OID)
        {
            try
            {
                IRepository repo = RepositoryFactory.Create(Config.RepositoryName);
                repo.Nombre = "PEDIDOS_SP";
                repo.AddParameter("@ACCION", "BYID");
                repo.AddParameter("@ID", OID);
                DataTable dt = repo.ExecuteDataTable();
                Pedido result = dt.ToGenericList<Pedido>(Converter).FirstOrDefault();
                return result;
            }
            catch (Exception ex)
            {
                throw new PedidoGetException(ex);
            }
        }

        public static Pedido ObtenerPendiente(int idusuario)
        {
            try
            {
                IRepository repo = RepositoryFactory.Create(Config.RepositoryName);
                repo.Nombre = "PEDIDOS_SP";
                repo.AddParameter("@ACCION", "CONSULTA");
                repo.AddParameter("@ID", null);
                repo.AddParameter("@NUMERO", null);
                repo.AddParameter("@IDUSUARIO", idusuario);
                repo.AddParameter("@IDESTADO", 1);
                DataTable dt = repo.ExecuteDataTable();
                if (dt.Rows.Count == 1)
                {
                    return Converter(dt.Rows[0]);
                }
                else
                {
                    Pedido pedido = new Pedido();
                    pedido.Usuario = new Usuario { OID = idusuario };
                    pedido.FormaPago = new FormaPago { OID = 2 };
                    pedido.Estado = 1;
                    Guardar(pedido);

                    return ObtenerPendiente(idusuario);

                }
            }
            catch (Exception ex)
            {
                throw new PedidoGetException(ex);
            }
        }

        public static void Guardar(Pedido obj)
        {
            try
            {
                IRepository repo = RepositoryFactory.CreateTransactional(Config.RepositoryName);
                repo.Nombre = "PEDIDOS_PROC";
                repo.AddParameter("@ACCION", obj.Persistencia);
                repo.AddParameter("@ID", obj.OID);
                repo.AddParameter("@NUMERO", obj.Numero);
                repo.AddParameter("@IDUSUARIO", obj.Usuario.OID);
                repo.AddParameter("@IDFORMAPAGO", obj.FormaPago.OID);
                repo.AddParameter("@IDESTADO", obj.Estado);
                obj.OID = repo.ExecuteReturnValue().ToInteger();
                repo.Commit();

                GestorPedidoDetalle.Guardar(obj);

            }
            catch (Exception ex)
            {
                throw new PedidoSaveException(ex);
            }
        }

        public static void GuardarSoloCantidades(Pedido pedido)
        {
            //GestorPedidoDetalle.GuardarCantidades(pedido);
        }
        public static void Eliminar(Pedido obj)
        {
            obj.Persistencia = EstadoPersistencia.DELETE;
            Guardar(obj);
            obj.Persistencia = EstadoPersistencia.UPDATE;
            obj.Activo = false;
        }
        public static void ReActivar(Pedido obj)
        {
            obj.Persistencia = EstadoPersistencia.UNDELETE;
            Guardar(obj);
            obj.Persistencia = EstadoPersistencia.UPDATE;
            obj.Activo = true;
        }


        public static List<Pedido> Consultar(int? numero, int? idusuario, int? idestado)
        {
            try
            {
                IRepository repo = RepositoryFactory.Create(Config.RepositoryName);
                repo.Nombre = "PEDIDOS_SP";
                repo.AddParameter("@ACCION", "CONSULTA");
                repo.AddParameter("@ID", null);
                repo.AddParameter("@NUMERO", numero);
                repo.AddParameter("@IDUSUARIO", idusuario);
                repo.AddParameter("@IDESTADO", idestado);
                DataTable dt = repo.ExecuteDataTable();
                List<Pedido> result = dt.ToGenericList<Pedido>(Converter);
                return result;
            }
            catch (Exception ex)
            {
                throw new PedidoGetException(ex);
            }
        }



        public static List<Pedido> Consultar()
        {
            return Consultar(null, null, null);
        }




        #region Converter
        public static Pedido Converter(DataRow row)
        {

            return new Pedido
            {
                OID = row["pID"].ToInteger(),
                Persistencia = EstadoPersistencia.UPDATE,
                Numero = row["pNUMERO"].ToInteger(),
                Usuario = GestorUsuario.Obtener(row["pIDUSUARIO"].ToInteger()),
                PedidoDetalles = GestorPedidoDetalle.ObtenerxPedido(row["pID"].ToInteger()),
                Estado = row["pIDESTADO"].ToInteger(),
                FormaPago = GestorFormaPago.Obtener(row["pIDFORMAPAGO"].ToInteger()),
                Fecha = row["FECHAALTA"].ToDateTime(),
                Activo = true
            };
        }
        #endregion


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Administration.Entities;
using GSoluard.Interfaces;
using GSoluard.DAO;
using System.Data;
using GSoluard.Administration.Exceptions;
using GSoluard.Administration.Engine.Parametrizacion;

namespace GSoluard.Administration.Engine.Procesos
{
    public class GestorIngresos
    {

        public static Ingreso Obtener(int OID)
        {
            try
            {
                IRepository repo = RepositoryFactory.Create(Config.RepositoryName);
                repo.Nombre = "INGRESOS_SP";
                repo.AddParameter("@ACCION", "BYID");
                repo.AddParameter("@ID", OID);
                DataTable dt = repo.ExecuteDataTable();
                Ingreso result = dt.ToGenericList<Ingreso>(Converter).FirstOrDefault();
                return result;
            }
            catch (Exception ex)
            {
                throw new IngresoGetException(ex);
            }
        }

        public static void Guardar(Ingreso obj)
        {
            try
            {
                IRepository repo = RepositoryFactory.CreateTransactional(Config.RepositoryName);
                repo.Nombre = "INGRESOS_PROC";
                repo.AddParameter("@ACCION", obj.Persistencia);
                repo.AddParameter("@ID", obj.OID);
                repo.AddParameter("@NUMERO", obj.Numero);
                repo.AddParameter("@IDPROVEEDOR", obj.Proveedor.OID);
                repo.AddParameter("@FECHA", obj.Fecha);
                obj.OID = repo.ExecuteReturnValue().ToInteger();
                repo.Commit();

                GestorIngresosDetalle.Guardar(obj);

            }
            catch (Exception ex)
            {
                throw new IngresoSaveException(ex);
            }
        }


        public static void Eliminar(Ingreso obj)
        {
            obj.Persistencia = EstadoPersistencia.DELETE;
            Guardar(obj);
            obj.Persistencia = EstadoPersistencia.UPDATE;
            obj.Activo = false;
        }
        public static void ReActivar(Ingreso obj)
        {
            obj.Persistencia = EstadoPersistencia.UNDELETE;
            Guardar(obj);
            obj.Persistencia = EstadoPersistencia.UPDATE;
            obj.Activo = true;
        }


        public static List<Ingreso> Consultar(string numero)
        {
            try
            {
                IRepository repo = RepositoryFactory.Create(Config.RepositoryName);
                repo.Nombre = "INGRESOS_SP";
                repo.AddParameter("@ACCION", "CONSULTA");
                repo.AddParameter("@ID", null);
                repo.AddParameter("@NUMERO", numero);
                DataTable dt = repo.ExecuteDataTable();
                List<Ingreso> result = dt.ToGenericList<Ingreso>(Converter);
                return result;
            }
            catch (Exception ex)
            {
                throw new IngresoGetException(ex);
            }
        }

        public static List<Ingreso> Consultar()
        {
            return Consultar(null);
        }


        #region Converter
        public static Ingreso Converter(DataRow row)
        {

            return new Ingreso
            {
                OID = row["iID"].ToInteger(),
                Persistencia = EstadoPersistencia.UPDATE,
                Numero = row["iNUMERO"].ToString(),
                Proveedor = GestorProveedor.Obtener(row["iIDPROVEEDOR"].ToInteger()),
                IngresoDetalles = GestorIngresosDetalle.ObtenerxIngreso(row["iID"].ToInteger()),
                Fecha = row["iFECHA"].ToDateTime(),
                Activo = true
            };
        }
        #endregion


    }
}

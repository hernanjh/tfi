﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Administration.Entities;
using GSoluard.Interfaces;
using GSoluard.DAO;
using System.Data;
using GSoluard.Administration.Exceptions;
using GSoluard.Administration.Engine.Parametrizacion;

namespace GSoluard.Administration.Engine.Procesos
{
    public class GestorIngresosDetalle
    {

        public static IngresoDetalle Obtener(int OID)
        {
            try
            {
                IRepository repo = RepositoryFactory.Create(Config.RepositoryName);
                repo.Nombre = "INGRESOSDETALLE_SP";
                repo.AddParameter("@ACCION", "BYID");
                repo.AddParameter("@ID", OID);
                DataTable dt = repo.ExecuteDataTable();
                IngresoDetalle result = dt.ToGenericList<IngresoDetalle>(Converter).FirstOrDefault();
                return result;
            }
            catch (Exception ex)
            {
                throw new IngresoDetalleGetException(ex);
            }
        }

        public static void Guardar(Ingreso obj)
        {
            try
            {
                /*IRepository repo = RepositoryFactory.CreateTransactional(Config.RepositoryName);
                repo.Nombre = "INGRESOSDETALLE_PROC";
                repo.AddParameter("@ACCION", "DELETEALL");
                repo.AddParameter("@IDINGRESO", obj.OID);
                repo.ExecuteNonQuery();
                repo.Commit();*/

                foreach (IngresoDetalle ingresodetalle in obj.IngresoDetalles)
                {
                    Guardar(obj.OID, ingresodetalle);
                }

            }
            catch (Exception ex)
            {
                throw new IngresoDetalleSaveException(ex);
            }
        }

        public static void Guardar(int? idingreso, IngresoDetalle obj)
        {
            try
            {
                //resto el stock de la materia prima modificada o borrada
                if (obj.Persistencia == EstadoPersistencia.DELETE || obj.Persistencia == EstadoPersistencia.UPDATE)
                {
                    IngresoDetalle orig = Obtener(obj.OID);
                    GestorMateriaPrima.DecrementarStock(orig.MateriaPrima.OID, orig.Cantidad);
                }

                IRepository repo = RepositoryFactory.CreateTransactional(Config.RepositoryName);
                repo.Nombre = "INGRESOSDETALLE_PROC";
                repo.AddParameter("@ACCION", obj.Persistencia);
                repo.AddParameter("@ID", obj.OID);
                repo.AddParameter("@IDINGRESO", idingreso);
                repo.AddParameter("@IDMATERIAPRIMA", obj.MateriaPrima.OID);
                repo.AddParameter("@CANTIDAD", obj.Cantidad);
                obj.OID = repo.ExecuteReturnValue().ToInteger();
                repo.Commit();

                if (obj.Persistencia == EstadoPersistencia.INSERT || obj.Persistencia == EstadoPersistencia.UPDATE)
                    GestorMateriaPrima.IncrementarStock(obj.MateriaPrima.OID, obj.Cantidad);

            }
            catch (Exception ex)
            {
                throw new IngresoDetalleSaveException(ex);
            }
        }

        public static void Eliminar(IngresoDetalle obj)
        {
            obj.Persistencia = EstadoPersistencia.DELETE;
            Guardar(null, obj);
            obj.Persistencia = EstadoPersistencia.UPDATE;
            obj.Activo = false;
        }


        public static void ReActivar(IngresoDetalle obj)
        {
            obj.Persistencia = EstadoPersistencia.UNDELETE;
            Guardar(null, obj);
            obj.Persistencia = EstadoPersistencia.UPDATE;
            obj.Activo = true;
        }

        public static List<IngresoDetalle> Consultar(int? idingreso, int? idmateriaprima)
        {
            try
            {
                IRepository repo = RepositoryFactory.Create(Config.RepositoryName);
                repo.Nombre = "INGRESOSDETALLE_SP";
                repo.AddParameter("@ACCION", "CONSULTA");
                repo.AddParameter("@ID", null);
                repo.AddParameter("@IDINGRESO", idingreso);
                repo.AddParameter("@IDMATERIAPRIMA", idmateriaprima);
                repo.AddParameter("@CANTIDAD", null);
                DataTable dt = repo.ExecuteDataTable();
                List<IngresoDetalle> result = dt.ToGenericList<IngresoDetalle>(Converter);
                return result;
            }
            catch (Exception ex)
            {
                throw new IngresoDetalleGetException(ex);
            }
        }

        public static List<IngresoDetalle> ObtenerxIngreso(int idingreso)
        {
            return Consultar(idingreso, null);
        }

        public static List<IngresoDetalle> Activos(Ingreso entidad)
        {
            try
            {
                return entidad.IngresoDetalles.Where(x => x.Persistencia != EstadoPersistencia.DELETE).ToList();
            }
            catch (Exception ex)
            {
                throw new IngresoDetalleGetException(ex);
            }
        }

        #region Converter
        public static IngresoDetalle Converter(DataRow row)
        {

            return new IngresoDetalle
            {
                OID = row["idID"].ToInteger(),
                Persistencia = EstadoPersistencia.UPDATE,
                Cantidad = row["idCANTIDAD"].ToInteger(),
                MateriaPrima = GestorMateriaPrima.Obtener(row["idIDMATERIAPRIMA"].ToInteger()),
                Activo = true
            };
        }
        #endregion


    }
}

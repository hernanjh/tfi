﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Administration.Entities;
using GSoluard.Interfaces;
using GSoluard.DAO;
using System.Data;
using GSoluard.Administration.Exceptions;
using GSoluard.Administration.Engine.Parametrizacion;

namespace GSoluard.Administration.Engine.Procesos
{
    public class GestorProductoMateriaPrima
    {

        public static ProductoMateriaPrima Obtener(int OID)
        {
            try
            {
                IRepository repo = RepositoryFactory.Create(Config.RepositoryName);
                repo.Nombre = "PRODUCTO_MATERIAPRIMA_SP";
                repo.AddParameter("@ACCION", "BYID");
                repo.AddParameter("@ID", OID);
                DataTable dt = repo.ExecuteDataTable();
                ProductoMateriaPrima result = dt.ToGenericList<ProductoMateriaPrima>(Converter).FirstOrDefault();
                return result;
            }
            catch (Exception ex)
            {
                throw new ProductoMateriaPrimaGetException(ex);
            }
        }

        public static void Guardar(Producto obj)
        {
            try
            {
  
                foreach (ProductoMateriaPrima ProductoMateriaPrima in obj.MateriasPrimas)
                {
                    Guardar(obj.OID, ProductoMateriaPrima);
                }

            }
            catch (Exception ex)
            {
                throw new ProductoMateriaPrimaSaveException(ex);
            }
        }

        public static void Guardar(int? id, ProductoMateriaPrima obj)
        {
            try
            {
                //resto el stock de la materia prima modificada o borrada
                if (obj.Persistencia == EstadoPersistencia.DELETE || obj.Persistencia == EstadoPersistencia.UPDATE)
                {
                    ProductoMateriaPrima orig = Obtener(obj.OID);
                    GestorMateriaPrima.IncrementarStock(orig.MateriaPrima.OID, orig.Cantidad);
                }

                IRepository repo = RepositoryFactory.CreateTransactional(Config.RepositoryName);
                repo.Nombre = "PRODUCTO_MATERIAPRIMA_PROC";
                repo.AddParameter("@ACCION", obj.Persistencia);
                repo.AddParameter("@ID", obj.OID);
                repo.AddParameter("@IDPRODUCTO", id);
                repo.AddParameter("@IDMATERIAPRIMA", obj.MateriaPrima.OID);
                repo.AddParameter("@CANTIDAD", obj.Cantidad);
                obj.OID = repo.ExecuteReturnValue().ToInteger();
                repo.Commit();

                if (obj.Persistencia == EstadoPersistencia.INSERT || obj.Persistencia == EstadoPersistencia.UPDATE)
                    GestorMateriaPrima.DecrementarStock(obj.MateriaPrima.OID, obj.Cantidad);

            }
            catch (Exception ex)
            {
                throw new ProductoMateriaPrimaSaveException(ex);
            }
        }

        public static void Eliminar(ProductoMateriaPrima obj)
        {
            obj.Persistencia = EstadoPersistencia.DELETE;
            Guardar(null, obj);
            obj.Persistencia = EstadoPersistencia.UPDATE;
            obj.Activo = false;
        }


        public static void ReActivar(ProductoMateriaPrima obj)
        {
            obj.Persistencia = EstadoPersistencia.UNDELETE;
            Guardar(null, obj);
            obj.Persistencia = EstadoPersistencia.UPDATE;
            obj.Activo = true;
        }

        public static List<ProductoMateriaPrima> Consultar(int? id, int? idmateriaprima)
        {
            try
            {
                IRepository repo = RepositoryFactory.Create(Config.RepositoryName);
                repo.Nombre = "PRODUCTO_MATERIAPRIMA_SP";
                repo.AddParameter("@ACCION", "CONSULTA");
                repo.AddParameter("@ID", null);
                repo.AddParameter("@IDPRODUCTO", id);
                repo.AddParameter("@IDMATERIAPRIMA", idmateriaprima);
                repo.AddParameter("@CANTIDAD", null);
                DataTable dt = repo.ExecuteDataTable();
                List<ProductoMateriaPrima> result = dt.ToGenericList<ProductoMateriaPrima>(Converter);
                return result;
            }
            catch (Exception ex)
            {
                throw new ProductoMateriaPrimaGetException(ex);
            }
        }

        public static List<ProductoMateriaPrima> ObtenerxProducto(int id)
        {
            return Consultar(id, null);
        }

        public static List<ProductoMateriaPrima> Activos(Producto entidad)
        {
            try
            {
                return entidad.MateriasPrimas.Where(x => x.Persistencia != EstadoPersistencia.DELETE).ToList();
            }
            catch (Exception ex)
            {
                throw new ProductoMateriaPrimaGetException(ex);
            }
        }

        #region Converter
        public static ProductoMateriaPrima Converter(DataRow row)
        {

            return new ProductoMateriaPrima
            {
                OID = row["pmID"].ToInteger(),
                Persistencia = EstadoPersistencia.UPDATE,
                Cantidad = row["pmCANTIDAD"].ToInteger(),
                MateriaPrima = GestorMateriaPrima.Obtener(row["pmIDMATERIAPRIMA"].ToInteger()),
                Activo = true
            };
        }
        #endregion


    }
}

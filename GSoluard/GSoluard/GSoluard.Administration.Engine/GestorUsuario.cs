﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Administration.Entities;
using System.Data;
using GSoluard.Interfaces;
using GSoluard.DAO;
using GSoluard.Administration.Exceptions;

namespace GSoluard.Administration.Engine
{
    public static class GestorUsuario
    {
        public static Usuario Obtener(string nick)
        {
            try
            {
                IRepository repo = RepositoryFactory.Create(Config.RepositoryName);
                repo.Nombre = "USUARIOS_SP";
                repo.AddParameter("@ACCION", "BYNICK");
                repo.AddParameter("@ID", null);
                repo.AddParameter("@NICK", nick);
                DataTable dt = repo.ExecuteDataTable();
                Usuario result = dt.ToGenericList<Usuario>(UsuarioConverter).FirstOrDefault();
                return result;
            }
            catch (Exception ex)
            {
                throw new UsuarioGetException(ex);
            }
        }

        public static Usuario Obtener(int oid)
        {
            try
            {
                IRepository repo = RepositoryFactory.Create(Config.RepositoryName);
                repo.Nombre = "USUARIOS_SP";
                repo.AddParameter("@ACCION", "BYID");
                repo.AddParameter("@ID", oid);
                repo.AddParameter("@NICK", null);
                DataTable dt = repo.ExecuteDataTable();
                Usuario result = dt.ToGenericList<Usuario>(UsuarioConverter).FirstOrDefault();
                return result;
            }
            catch (Exception ex)
            {
                throw new UsuarioGetException(ex);
            }
        }
        public static List<Usuario> ObtenerUsuariosPorArea(int areaId)
        {
            List<Usuario> result;

            try
            {
                IRepository repo = RepositoryFactory.Create(Config.RepositoryName);
                repo.Nombre = "USUARIOS_SP";
                repo.AddParameter("@ACCION", "BYAREA");
                repo.AddParameter("@ID", null);
                repo.AddParameter("@NICK", null);
                repo.AddParameter("@IDAREA", areaId);
                DataTable dt = repo.ExecuteDataTable();
                result = dt.ToGenericList<Usuario>(UsuarioConverter);
                return result;
            }
            catch (Exception ex)
            {
                throw new UsuarioGetException(ex);
            }
        
        
        
        }

        public static List<Usuario> ConsultaTodos()
        {
            List<Usuario> result;

            try
            {
                IRepository repo = RepositoryFactory.Create(Config.RepositoryName);
                repo.Nombre = "USUARIOS_SP";
                repo.AddParameter("@ACCION", "CONSULTATODOS");
                repo.AddParameter("@ID", null);
                repo.AddParameter("@NICK", null);
                DataTable dt = repo.ExecuteDataTable();
                result = dt.ToGenericList<Usuario>(UsuarioConverter);
                return result;
            }
            catch (Exception ex)
            {
                throw new UsuarioGetException(ex);
            }
        


        }


        public static void Guardar(Usuario usuario)
        {
            try
            {
                IRepository repo = RepositoryFactory.CreateTransactional(Config.RepositoryName);
                repo.Nombre = "USUARIOS_PROC";
                repo.AddParameter("@ACCION", usuario.Persistencia);
                repo.AddParameter("@ID", usuario.OID);
                repo.AddParameter("@NICK", usuario.Nick);
                repo.AddParameter("@CLAVE", usuario.Clave);
                repo.AddParameter("@BLOQUEADO", usuario.Bloqueado);
                repo.AddParameter("@SUPERADMIN", usuario.IsSuperAdmin);
                repo.AddParameter("@IDUSUARIO", HGSContext.Usuario.OID);
                repo.AddParameter("@usuNOMBRE", usuario.Nombre);
                repo.AddParameter("@usuAPELLIDO", usuario.Apellido);

                repo.ExecuteReturnValue().ToInteger();
                repo.Commit();


                GuardarRolesUsuario(usuario);



            }
            catch (Exception ex)
            {
                throw new UsuarioSaveException(ex);
            }
        
        }

        public static void Eliminar(Usuario usuario)
        {
            usuario.Persistencia = EstadoPersistencia.DELETE;
            Guardar(usuario);
            usuario.Persistencia = EstadoPersistencia.UPDATE;
            usuario.Activo = false;
        }


        public static void GuardarRolesUsuario(Usuario usuario)
        {
            EliminarRolesUsuario(usuario);

            if (usuario.Roles != null)
            {
                foreach (Rol rol in usuario.Roles)
                {
                    GuardarRolUsuario(usuario, rol);
                }
            }
        }
        /// <summary>
        /// Descripcion: Elimino todos los roles de un usuario
        /// </summary>
        /// <param name="usuario"></param>
        private static void EliminarRolesUsuario(Usuario usuario)
        {
             try
            {
                IRepository repo = RepositoryFactory.CreateTransactional(Config.RepositoryName);
                repo.Nombre = "USUARIOS_ROLES_PROC";
                repo.AddParameter("@ACCION", "3");
                repo.AddParameter("@IDUSUARIO", usuario.OID);
                repo.AddParameter("@IDROL", null);
                repo.ExecuteNonQuery().ToInteger();
                repo.Commit();
            }
             catch (Exception ex)
             {
                 throw new UsuarioSaveException(ex);
             }
          
        }

        public static void GuardarRolUsuario(Usuario usuario, Rol rol)
        {
            try
            {
                IRepository repo = RepositoryFactory.CreateTransactional(Config.RepositoryName);
                repo.Nombre = "USUARIOS_ROLES_PROC";
                repo.AddParameter("@ACCION", usuario.Persistencia);
                repo.AddParameter("@IDUSUARIO", usuario.OID);
                repo.AddParameter("@IDROL", rol.OID);
                repo.ExecuteNonQuery().ToInteger();
                repo.Commit();
            }

            catch (Exception ex)
            {
                throw new UsuarioSaveException(ex);
            }
        
        
        }

        public static void Autenticar(string usuario, string password)
        {
            Autenticar(usuario, password, false);
        }


        public static void Autenticar(string usuario, string password, bool pub)
        {

            try
            {
                IRepository repo = RepositoryFactory.Create(Config.RepositoryName);
                repo.Nombre = "AUTENTICAR_PROC";
                repo.AddParameter("@NICK", usuario);
                repo.AddParameter("@CLAVE", password);
                DataTable dt = repo.ExecuteDataTable();
                Usuario result = dt.ToGenericList<Usuario>(UsuarioLightConverter).FirstOrDefault();

                if (result == null)
                    throw new UsuarioAuthException(null);

                if (result.Bloqueado)
                    throw new UsuarioLockException(null);

                LoadContexto(usuario, pub);
            }
            catch (HGSException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new UsuarioGetException(ex);
            }

        }

 
        public static void LoadContexto(string usuario, bool pub)
        {
            HGSContext.Usuario = Obtener(usuario);
            HGSContext.Permisos.Modulos = GestorPermisos.Obtener(HGSContext.Usuario, pub);
        }


        public static string ObtenerUserName()
        {
            return HGSContext.Usuario.Nick;
        }

        #region Converter
        public static Usuario UsuarioLightConverter(DataRow row)
        {
            return new Usuario
            {
                OID = row["usuID"].ToInteger(),
                Persistencia = EstadoPersistencia.UPDATE,
                Nick = row["usuNICK"].ToString(),
                Bloqueado = row["usuBLOQUEADO"].ToBoolean(),
                IsSuperAdmin = row["usuSUPERADMIN"].ToBoolean(),
                Apellido = row["usuAPELLIDO"].ToString(),
                Nombre = row["usuNOMBRE"].ToString()
            };
        }

        public static Usuario UsuarioConverter(DataRow row)
        {

            return new Usuario
            {
                OID = row["usuID"].ToInteger(),
                Persistencia = EstadoPersistencia.UPDATE,
                Roles = GestorRoles.ObtenerxUsuario(row["usuID"].ToInteger()),
                Nick = row["usuNICK"].ToString(),
                Bloqueado = row["usuBLOQUEADO"].ToBoolean(),
                IsSuperAdmin = row["usuSUPERADMIN"].ToBoolean(),
                Clave = row["usuCLAVE"].ToString(),
                Apellido = row["usuAPELLIDO"].ToString(),
                Nombre = row["usuNOMBRE"].ToString()
            };

        }
        #endregion

    }
}

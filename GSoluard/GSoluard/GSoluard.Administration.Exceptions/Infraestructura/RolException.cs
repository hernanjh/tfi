﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Interfaces;

namespace GSoluard.Administration.Exceptions
{
    public class RolGetException : HGSException
    {
        public RolGetException(Exception inner) : base("ROL001", "Ocurrió un error al obtener el Rol", inner) { }
    }
    public class RolSetException : HGSException
    {
        public RolSetException(Exception inner) : base("ROL002", "Ocurrió un error al guardar el Rol", inner) { }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Interfaces;

namespace GSoluard.Administration.Exceptions
{
    public class ModuloGetException : HGSException
    {
        public ModuloGetException(Exception inner) : base("MOD001", "Ocurrió un error al obtener el modulo", inner) { }
    }
    public class ModuloSetException : HGSException
    {
        public ModuloSetException(Exception inner) : base("MOD002", "Ocurrió un error al guardar el modulo", inner) { }
    }
}

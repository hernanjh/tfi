﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Interfaces;

namespace GSoluard.Administration.Exceptions
{
    public class ProductoMateriaPrimaGetException : HGSException
    {
        public ProductoMateriaPrimaGetException(Exception inner) : base("ID001", "Ocurrió un error al obtener la Materia Prima del Producto", inner) { }
    }
    public class ProductoMateriaPrimaSaveException : HGSException
    {
        public ProductoMateriaPrimaSaveException(Exception inner) : base("ID002", "Ocurrió un error al Guardar la Materia Prima del Producto", inner) { }
    }
}

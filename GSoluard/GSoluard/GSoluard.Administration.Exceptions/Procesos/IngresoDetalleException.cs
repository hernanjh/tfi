﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Interfaces;

namespace GSoluard.Administration.Exceptions
{
    public class IngresoDetalleGetException : HGSException
    {
        public IngresoDetalleGetException(Exception inner) : base("ID001", "Ocurrió un error al Obtener el detalle del ingreso", inner) { }
    }
    public class IngresoDetalleSaveException : HGSException
    {
        public IngresoDetalleSaveException(Exception inner) : base("ID002", "Ocurrió un error al Guardar el detalle del ingreso", inner) { }
    }
}

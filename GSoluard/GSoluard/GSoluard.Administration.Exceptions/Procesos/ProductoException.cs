﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Interfaces;

namespace GSoluard.Administration.Exceptions
{
    public class ProductoGetException : HGSException
    {
        public ProductoGetException(Exception inner) : base("ID001", "Ocurrió un error al Obtener el Producto", inner) { }
    }
    public class ProductoSaveException : HGSException
    {
        public ProductoSaveException(Exception inner) : base("ID002", "Ocurrió un error al Guardar el Producto", inner) { }
    }
}

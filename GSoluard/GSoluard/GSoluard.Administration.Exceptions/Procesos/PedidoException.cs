﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Interfaces;

namespace GSoluard.Administration.Exceptions
{
    public class PedidoGetException : HGSException
    {
        public PedidoGetException(Exception inner) : base("PE001", "Ocurrió un error al Obtener el pedido", inner) { }
    }
    public class PedidoSaveException : HGSException
    {
        public PedidoSaveException(Exception inner) : base("PE002", "Ocurrió un error al Guardar el pedido", inner) { }
    }
}

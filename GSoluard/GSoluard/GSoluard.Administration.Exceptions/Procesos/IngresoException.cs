﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Interfaces;

namespace GSoluard.Administration.Exceptions
{
    public class IngresoGetException : HGSException
    {
        public IngresoGetException(Exception inner) : base("ID001", "Ocurrió un error al Obtener el ingreso", inner) { }
    }
    public class IngresoSaveException : HGSException
    {
        public IngresoSaveException(Exception inner) : base("ID002", "Ocurrió un error al Guardar el ingreso", inner) { }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Interfaces;

namespace GSoluard.Administration.Exceptions
{
    public class PedidoDetalleGetException : HGSException
    {
        public PedidoDetalleGetException(Exception inner) : base("PD001", "Ocurrió un error al Obtener el detalle del pedido", inner) { }
    }
    public class PedidoDetalleSaveException : HGSException
    {
        public PedidoDetalleSaveException(Exception inner) : base("PD002", "Ocurrió un error al Guardar el detalle del pedido", inner) { }
    }
}

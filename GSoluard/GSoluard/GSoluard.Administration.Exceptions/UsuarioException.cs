﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Interfaces;

namespace GSoluard.Administration.Exceptions
{
    public class UsuarioGetException : HGSException
    {
        public UsuarioGetException(Exception inner) : base("USU001", "Ocurrió un error al obtener el Usuario", inner) { }
    }
    public class UsuarioSetException : HGSException
    {
        public UsuarioSetException(Exception inner) : base("USU002", "Ocurrió un error al guardar el Usuario", inner) { }
    }
    public class UsuarioLockException : HGSException
    {
        public UsuarioLockException(Exception inner) : base("USU003", "El usuario se encuentra bloqueado", inner) { }
    }
    public class UsuarioAuthException : HGSException
    {
        public UsuarioAuthException(Exception inner) : base("USU004", "El usuario o contraseña son incorrectos", inner) { }
    }
    public class UsuarioSaveException : HGSException
    {
        public UsuarioSaveException(Exception inner) : base("USU001", "Ocurrió un error al guardar el Usuario", inner) { }
    }
}

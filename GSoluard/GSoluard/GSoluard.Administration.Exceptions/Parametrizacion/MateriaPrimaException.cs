﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Interfaces;

namespace GSoluard.Administration.Exceptions
{
    public class MateriaPrimaGetException : HGSException
    {
        public MateriaPrimaGetException(Exception inner) : base("FP001", "Ocurrió un error al Obtener la Materia Prima", inner) { }
    }
    public class MateriaPrimaSaveException : HGSException
    {
        public MateriaPrimaSaveException(Exception inner) : base("FP002", "Ocurrió un error al Guardar la Materia Prima", inner) { }
    }
}

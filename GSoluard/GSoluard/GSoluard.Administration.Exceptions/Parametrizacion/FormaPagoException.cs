﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Interfaces;

namespace GSoluard.Administration.Exceptions
{
    public class FormaPagoGetException : HGSException
    {
        public FormaPagoGetException(Exception inner) : base("FP001", "Ocurrió un error al Obtener la Forma de Pago", inner) { }
    }
    public class FormaPagoSaveException : HGSException
    {
        public FormaPagoSaveException(Exception inner) : base("FP002", "Ocurrió un error al Guardar la Forma de Pago", inner) { }
    }
}

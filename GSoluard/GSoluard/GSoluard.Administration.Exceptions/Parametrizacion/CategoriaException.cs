﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Interfaces;

namespace GSoluard.Administration.Exceptions
{
    public class CategoriaGetException : HGSException
    {
        public CategoriaGetException(Exception inner) : base("CAT001", "Ocurrió un error al Obtener la Categoria", inner) { }
    }
    public class CategoriaSaveException : HGSException
    {
        public CategoriaSaveException(Exception inner) : base("CAT002", "Ocurrió un error al Guardar la Categoria", inner) { }
    }
}

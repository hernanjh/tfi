﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSoluard.Interfaces;

namespace GSoluard.Administration.Exceptions
{
    public class ProveedorGetException : HGSException
    {
        public ProveedorGetException(Exception inner) : base("PV001", "Ocurrió un error al Obtener el Proveedor", inner) { }
    }
    public class ProveedorSaveException : HGSException
    {
        public ProveedorSaveException(Exception inner) : base("PV002", "Ocurrió un error al Guardar el Proveedor", inner) { }
    }
}

﻿/*!
* jQuery Custom Ext Plugin 1.0
*
*
* Copyright 2013 Hernan Hegykozi
* Released under the MIT license:
*   http://www.opensource.org/licenses/mit-license.php
*/

//Hace parpadear un elemento
$.fn.parpadear = function () {
    this.each(function parpadear() {
        $(this).fadeIn(500).delay(250).fadeOut(500, parpadear);
    });
}


//Verifica si contiene un atributo
$.fn.hasAttr = function (name) {
    return this.attr(name) !== undefined;
};

//Selector para saber si un elemento no se ve en pantalla
jQuery.expr[':'].reallyvisible = function (a) {
    return !jQuery(a).is(':hidden') && jQuery(a).css('display') != 'none';
};

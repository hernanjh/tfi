﻿function LoadPageDialog(url, title, width, height, divname) {

    var $dialog = $('<div id="' + divname + '" style="display: block;overflow-x:hidden"></div>')
                .html('<iframe id="iframe" style="border: 0px; overflow-x:hidden; " src="' + url + '" width="100%" height="100%" ></iframe>')
                .dialog({
                    autoOpen: false,
                    modal: true,
                    height: height,
                    width: width,
                    title: title,
                    width: width,
                    height: height,
                    dialogClass: "dialog-WithDropShadow",
                    close: function (event, ui) {
                        
                    },
                    open: function () {
                        $('.overlay').hide().fadeIn();
                        $(this).parent().children().children(".ui-dialog-titlebar-close").removeAttr('title');
                    },
                    show: "fade",
                    hide: "fade"
                });

    $dialog.dialog('open');

    return false;

}


function LoadPageDialogSinTitulo(url, title, width, height, divname) {

    var $dialog = $('<div id="' + divname + '" style="display: block;overflow-x:hidden"></div>')
                .html('<iframe style="border: 0px; overflow-x:hidden; " src="' + url + '" width="100%" height="100%" ></iframe>')
                .dialog({
                    autoOpen: false,
                    modal: true,
                    height: height,
                    width: width,
                   // title: title,
                    width: width,
                    height: height,
                    ialogClass: "dialog-no-titlebar dialog-no-close dialog-WithDropShadow",
                    close: function (event, ui) {

                    },
                    open: function () {
                        $('.overlay').hide().fadeIn();
                        $(this).parent().children().children(".ui-dialog-titlebar-close").removeAttr('title');
                    },
                    show: "fade",
                    hide: "fade"
                });

    $dialog.dialog('open');

    return false;

}



var notifyTypes = [];
notifyTypes.MESSAGE = { container: 'ui-state-highlight ui-corner-all', icon: "ui-icon-info", timeout: 3 };
notifyTypes.ERROR = { container: 'ui-state-error ui-corner-all', icon: "ui-icon-alert", timeout: 6 };
notifyTypes.INFO = { container: 'ui-state-highlight ui-corner-allt', icon: "ui-icon-info", timeout: 0 };
notifyTypes.STICKY = { container: 'ui-state-highlight ui-corner-all', icon: "ui-icon-info", timeout: 0 };

var notifyText = [];
notifyText.DELETED = 'El registro ha sido eliminado correctamente';
notifyText.ERR = 'Ocurrio un error';
notifyText.SAVED = 'El registro ha sido guardado correctamente';

function notify(message, type) {
    var selType = (!type) ? notifyTypes.MESSAGE : notifyTypes[type];
    $('<div class="achtung ui-widget ' + selType.container + '" style="display: none;">' +
    '<span class="ui-icon ' + selType.icon + '" style="float: left; margin-right: .3em;"></span>' + message +
    '</div>').achtung({ timeout: selType.timeout });
};


//Funciones para control Select---
function SelectClear(id) {
    $(id).html("");
}
function SelectDefaultValue(id, json) {	
    SelectClear(id);
    $(id).append($("<option></option>").attr("value", 0).text("Seleccione..."))
}
function SelectLoading(id) {
    SelectClear(id);
    $(id).append($("<option></option>").attr("value", 0).text("Cargando..."))
}


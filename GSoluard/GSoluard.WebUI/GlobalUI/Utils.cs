﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GSoluard.WebUI
{
    public class Utils
    {
        public static string LoadScript(string url)
        {
            string strFilePath = null;
            strFilePath = HttpContext.Current.Server.MapPath(url);
            DateTime dtLastModified = System.IO.File.GetLastWriteTime(strFilePath);
            return "\n<script type='text/javascript' src='" + url + "?v=" + dtLastModified.ToString("yyyyMMddhhmmss") + "'></script>";
        }

        public static string GetRequest(string name)
        {
            string result = null;
            if (HttpContext.Current.Request[name] != null)
            {
                if (HttpContext.Current.Request[name] != "")
                    result = HttpContext.Current.Request[name].ToString();
            }
            return result;
        }
    }
}
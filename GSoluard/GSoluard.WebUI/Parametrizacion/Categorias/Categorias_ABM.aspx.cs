﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GSoluard.Administration.Entities;
using GSoluard.Administration.Engine.Parametrizacion;
using GSoluard.ControlsUI;

namespace GSoluard.WebUI.Parametrizacion
{
    public partial class Categorias_ABM : PageBase
    {

        #region Propiedades

        public Categoria Categoria
        {
            get
            {
                if (ViewState["Categoria"] != null)
                    return (Categoria)ViewState["Categoria"];
                return null;
            }
            set
            {
                ViewState["Categoria"] = value;
            }

        }

        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["id"] == null)
                {
                    Categoria = new Categoria();
                }
                else
                {
                    var Id = Convert.ToInt32(Request.QueryString["id"]);
                    Categoria = GestorCategoria.Obtener(Id);
                    this.txtDescripcion.Text = Categoria.Descripcion;
                }
                Initialize();
            }

        }

        void Initialize()
        {
            this.btnGuardar.Settings(new ButtonExtText());
            this.btnCerrar.Settings(new ButtonExtText());
            this.txtDescripcion.Settings(new TextBoxExtStyle());

        }


        protected void btnGuardar_Click(object sender, EventArgs e)
        {

            Categoria.Descripcion = this.txtDescripcion.Text;

            GestorCategoria.Guardar(Categoria);

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "closedialog", "parent.IntervaloCerrarPopup();", true);

        }


        protected void btnCerrar_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "closedialog", "parent.CerrarPopup();", true);
        }

      
    }
}
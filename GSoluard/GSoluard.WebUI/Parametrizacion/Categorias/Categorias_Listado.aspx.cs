﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GSoluard.Administration.Entities;
using GSoluard.ControlsUI;
using GSoluard.Administration.Engine.Parametrizacion;
using GSoluard.WebControlExtend;

namespace GSoluard.WebUI.Parametrizacion
{
    public partial class Categorias_Listado :  PageBase
    {
       
        #region Propiedades
        public List<Categoria> Categorias
        {
            get
            {
                if (ViewState["Categoria"] != null)
                    return (List<Categoria>)ViewState["Categoria"];
                return null;
            }
            set
            {
                ViewState["Categoria"] = value;
            }

        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Initialize();
                LoadGrid(false);
            }
        }

        void Initialize()
        {
            this.GridCategorias.Settings(new GridViewExtBasic());
            this.btnNuevo.Settings(new ButtonExtText());
            this.btnRefresh.Settings(new ButtonExtText());                        
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadGrid(false);
        }

        protected void btnBorrar_Click(object sender, EventArgs e)
        {
            ImageButton btn = (ImageButton)sender;
            int index = Convert.ToInt32(btn.CommandArgument);
            int id = Convert.ToInt32(GridCategorias.DataKeys[index].Value);
                    
            Categoria obj = Categorias.Find(c => c.OID == id);
            Categorias.Remove(obj);

            GestorCategoria.Eliminar(obj);
            LoadGrid(true);

        }

        

        private void LoadGrid(bool cache)
        {
            if (!cache)
                Categorias = GestorCategoria.Consultar();

            this.GridCategorias.DataSource = Categorias;
            this.GridCategorias.DataBind();
        }

        protected void GridCategorias_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButtonExt Editar = e.Row.FindControl("btnEditar") as ImageButtonExt;
                if (Editar != null)
                {
                    Editar.Settings(new ImageButtonExtEdit());
                    int oid = DataBinder.Eval(e.Row.DataItem, "OID").ToInteger();
                    Editar.OnClientClick = "Editar(" + oid.ToString() + "); return false;";
                }

                ImageButtonExt Borrar = e.Row.FindControl("btnBorrar") as ImageButtonExt;
                if (Borrar != null)
                {
                    Borrar.Settings(new ImageButtonExtDelete());
                }

            }
        }

        protected void GridCategorias_PageIndexChanging(Object sender, GridViewPageEventArgs e)
        {
            GridCategorias.PageIndex = e.NewPageIndex;
            LoadGrid(true);
        }


    }
}
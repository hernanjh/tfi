﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GSoluard.Administration.Entities;
using GSoluard.ControlsUI;
using GSoluard.Administration.Engine.Parametrizacion;
using GSoluard.WebControlExtend;

namespace GSoluard.WebUI.Parametrizacion
{
    public partial class Proveedores_Listado :  PageBase
    {
       
        #region Propiedades
        public List<Proveedor> Proveedores
        {
            get
            {
                if (ViewState["Proveedor"] != null)
                    return (List<Proveedor>)ViewState["Proveedor"];
                return null;
            }
            set
            {
                ViewState["Proveedor"] = value;
            }

        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Initialize();
                LoadGrid(false);
            }
        }

        void Initialize()
        {
            this.GridProveedores.Settings(new GridViewExtBasic());
            this.btnNuevo.Settings(new ButtonExtText());
            this.btnRefresh.Settings(new ButtonExtText());
            this.btnFiltrar.Settings(new ButtonExtText());
            this.bntQuitarFiltros.Settings(new ButtonExtText());
            this.txtCuit.Settings(new TextBoxExtStyle());
            this.txtRazonSocial.Settings(new TextBoxExtStyle());
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadGrid(false);
        }

        protected void btnBorrar_Click(object sender, EventArgs e)
        {
            ImageButton btn = (ImageButton)sender;
            int index = Convert.ToInt32(btn.CommandArgument);
            int id = Convert.ToInt32(GridProveedores.DataKeys[index].Value);
                    
            Proveedor obj = Proveedores.Find(c => c.OID == id);
            Proveedores.Remove(obj);

            GestorProveedor.Eliminar(obj);
            LoadGrid(true);

        }

        

        private void LoadGrid(bool cache)
        {
            if (!cache)
                Proveedores = GestorProveedor.Consultar();

            this.GridProveedores.DataSource = Proveedores;
            this.GridProveedores.DataBind();
        }

        protected void GridProveedores_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButtonExt Editar = e.Row.FindControl("btnEditar") as ImageButtonExt;
                if (Editar != null)
                {
                    Editar.Settings(new ImageButtonExtEdit());
                    int oid = DataBinder.Eval(e.Row.DataItem, "OID").ToInteger();
                    Editar.OnClientClick = "Editar(" + oid.ToString() + "); return false;";
                }

                ImageButtonExt Borrar = e.Row.FindControl("btnBorrar") as ImageButtonExt;
                if (Borrar != null)
                {
                    Borrar.Settings(new ImageButtonExtDelete());
                }

            }
        }

        protected void GridProveedores_PageIndexChanging(Object sender, GridViewPageEventArgs e)
        {
            GridProveedores.PageIndex = e.NewPageIndex;
            LoadGrid(true);
        }

        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            List<Proveedor> filtroResult = null;

            if (!string.IsNullOrEmpty(this.txtRazonSocial.Text))
            {
                filtroResult = Proveedores.FindAll(y => y.RazonSocial.ToLower().Contains(this.txtRazonSocial.Text.ToLower()));
            }

            if (!string.IsNullOrEmpty(this.txtCuit.Text))
            {
                filtroResult = Proveedores.FindAll(y => y.Cuit.ToLower().Contains(this.txtCuit.Text.ToLower()));
            }
            if (filtroResult == null) filtroResult = Proveedores;

            this.GridProveedores.DataSource = filtroResult;
            this.GridProveedores.DataBind();
        }
        protected void btnQuitarFiltros_Click(object sender, EventArgs e)
        {
            this.txtRazonSocial.Text = string.Empty;
            this.txtCuit.Text = string.Empty;

            LoadGrid(true);

        }


    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginasMaster.Master" AutoEventWireup="true" CodeBehind="Proveedores_ABM.aspx.cs" Inherits="GSoluard.WebUI.Parametrizacion.Proveedores_ABM" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <div class="formRegistro ui-widget">	
		<div class="filaCampo">
			<div class="etiqueta">Razon Social  : </div>
			<div class="campo"> <wce:TextBoxExt  ID="txtRazonSocial" runat="server" Required="true" MaxLength="100"></wce:TextBoxExt></div>
		</div>
       <br />
		<div class="filaCampo">
			<div class="etiqueta">Cuit  : </div>
			<div class="campo"> <wce:TextBoxExt  ID="txtCuit" runat="server" Required="true" Type="CuitCuil"></wce:TextBoxExt></div>
		</div>
       <br /><br />
       <div class="filaCampo">
         <wce:ButtonExt ID="btnGuardar" runat="server"  Text="Guardar" 
            onclick="btnGuardar_Click" OnClientClick="return validarForm();" />
         <wce:ButtonExt ID="btnCerrar" runat="server"  Text="Cerrar" OnClientClick="parent.CerrarPopup(); return false;"  />
       </div>
    </div>
<script type="text/javascript">
    //activa la validacion en el onblur de los campos
    validarFormBlurActivo();
</script>



</asp:Content>

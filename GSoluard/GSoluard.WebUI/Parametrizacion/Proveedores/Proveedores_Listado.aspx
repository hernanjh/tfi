﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginasMaster.Master" AutoEventWireup="true" CodeBehind="Proveedores_Listado.aspx.cs" Inherits="GSoluard.WebUI.Parametrizacion.Proveedores_Listado" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <script language="javascript" type="text/javascript">

      $(document).ready(function () {

          $("#<%=btnNuevo.ClientID %>").live('click', function () {
              LoadPageDialog("Proveedores_ABM.aspx", " Nuevo Proveedor", 400, 200, "modificar_div")
          });


      });

      function Editar(id) {
          LoadPageDialog("Proveedores_ABM.aspx?id=" + id, " Modificar Proveedor", 400, 200, "modificar_div");
      }
      function ConfirmarEliminar(control) {

          $("#dialog-confirm-delete").dialog({
              resizable: false,
              height: 140,
              modal: true,
              buttons: {
                  "Eliminar": function () {
                      __doPostBack(control, '');
                      $(this).dialog("close");
                  },
                  "Cancelar": function () {
                      $(this).dialog("close");
                  }
              }
          });
          return false;
      }

      function IntervaloCerrarPopup() {
          notify("El Proveedor ha sido guardado correctamente", 'MESSAGE');
          CerrarPopup();
          $("#<%=btnRefresh.ClientID %>").click();

      }
      function CerrarPopup() {

          $('#modificar_div').dialog('close');
          $('#modificar_div').remove();

      }

  </script>
    <div class="ui-widget">
        <div class="ui-widget-header ui-corner-top">
            <h2>
                Listado de Proveedores </h2>
        </div>
        <div id = "filtros" class="accordion">
          <h3>Filtros </h3>  
         <div>
         <table>
                  <tr>
                     <td>
                     Razon Social:
                     </td>
                     <td>
                      <wce:TextBoxExt  ID="txtRazonSocial" runat="server" MaxLength="100" ></wce:TextBoxExt>
                      </td>         
                  </tr>
                   <tr>
                    <td>  
                     Cuit :
                    </td>
                    <td>
                      <wce:TextBoxExt  ID="txtCuit" runat="server" MaxLength="11"></wce:TextBoxExt>
                   </td>
                  </tr>
        </table>               
      
        <br>
        <wce:ButtonExt ID="btnFiltrar" Text="Filtrar" runat="server" onclick = "btnFiltrar_Click" />
         <wce:ButtonExt ID="bntQuitarFiltros" Text="QuitarFiltros" runat="server" onclick = "btnQuitarFiltros_Click" />
        <hr />
        </div>
      </div>
        <div class="ui-widget-content ui-corner-bottom">
            <wce:ButtonExt ID="btnNuevo" Text="Nuevo" runat="server" OnClientClick="return false;" />
            <wce:ButtonExt ID="btnRefresh" Text="Refrescar" runat="server" OnClick="btnRefresh_Click" />                        
            <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                <ContentTemplate>
                    <wce:GridViewExt ID="GridProveedores" runat="server"
                       DataKeyNames="OID"
                       OnRowDataBound="GridProveedores_RowDataBound" 
                       OnPageIndexChanging="GridProveedores_PageIndexChanging">                       
                        <Columns>
                            <asp:BoundField DataField="RazonSocial" HeaderText="Razon Social" />
                            <asp:BoundField DataField="Cuit" HeaderText="Cuit" ItemStyle-HorizontalAlign="Center" />
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="40px">
                                <ItemTemplate>
                                    <wce:ImageButtonExt ID="btnEditar" runat="server"></wce:ImageButtonExt>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="40px">
                                <ItemTemplate>
                                    <wce:ImageButtonExt ID="btnBorrar" runat="server" 
                                        CommandName="Borrar" CommandArgument="<%# ((GridViewRow)Container).RowIndex %>"
                                        OnClick="btnBorrar_Click" OnClientClick="return ConfirmarEliminar(this.name);">
                                    </wce:ImageButtonExt>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                    </wce:GridViewExt>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnRefresh" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
            <br />
        </div>
    </div>
    <div id="dialog-confirm-delete" title="Confirmar eliminación" style="display: none">
        <p>
            <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>
            ¿Esta seguro de eliminar el registro?</p>
    </div>




</asp:Content>

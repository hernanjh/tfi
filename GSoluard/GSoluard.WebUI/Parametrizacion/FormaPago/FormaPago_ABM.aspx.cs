﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GSoluard.Administration.Entities;
using GSoluard.Administration.Engine.Parametrizacion;
using GSoluard.ControlsUI;

namespace GSoluard.WebUI.Parametrizacion
{
    public partial class FormaPago_ABM : PageBase
    {

        #region Propiedades

        public FormaPago FormaPago
        {
            get
            {
                if (ViewState["FormaPago"] != null)
                    return (FormaPago)ViewState["FormaPago"];
                return null;
            }
            set
            {
                ViewState["FormaPago"] = value;
            }

        }

        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["id"] == null)
                {
                    FormaPago = new FormaPago();
                }
                else
                {
                    var Id = Convert.ToInt32(Request.QueryString["id"]);
                    FormaPago = GestorFormaPago.Obtener(Id);
                    this.txtDescripcion.Text = FormaPago.Descripcion;
                }
                Initialize();
            }

        }

        void Initialize()
        {
            this.btnGuardar.Settings(new ButtonExtText());
            this.btnCerrar.Settings(new ButtonExtText());
            this.txtDescripcion.Settings(new TextBoxExtStyle());
        }


        protected void btnGuardar_Click(object sender, EventArgs e)
        {

            FormaPago.Descripcion = this.txtDescripcion.Text;

            GestorFormaPago.Guardar(FormaPago);

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "closedialog", "parent.IntervaloCerrarPopup();", true);

        }


        protected void btnCerrar_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "closedialog", "parent.CerrarPopup();", true);
        }

      
    }
}
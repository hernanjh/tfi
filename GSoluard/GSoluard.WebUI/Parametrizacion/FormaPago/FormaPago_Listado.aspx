﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginasMaster.Master" AutoEventWireup="true" CodeBehind="FormaPago_Listado.aspx.cs" Inherits="GSoluard.WebUI.Parametrizacion.FormaPago_Listado" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <script language="javascript" type="text/javascript">

      $(document).ready(function () {

          $("#<%=btnNuevo.ClientID %>").live('click', function () {
              LoadPageDialog("FormaPago_ABM.aspx", " Nueva Forma de Pago", 400, 200, "modificar_div")
          });


      });

      function Editar(id) {
          LoadPageDialog("FormaPago_ABM.aspx?id=" + id, " Modificar Forma de Pago", 400, 200, "modificar_div");
      }
      function ConfirmarEliminar(control) {

          $("#dialog-confirm-delete").dialog({
              resizable: false,
              height: 140,
              modal: true,
              buttons: {
                  "Eliminar": function () {
                      __doPostBack(control, '');
                      $(this).dialog("close");
                  },
                  "Cancelar": function () {
                      $(this).dialog("close");
                  }
              }
          });
          return false;
      }

      function IntervaloCerrarPopup() {
          notify("La Forma de Pago ha sido guardada correctamente", 'MESSAGE');
          CerrarPopup();
          $("#<%=btnRefresh.ClientID %>").click();

      }
      function CerrarPopup() {

          $('#modificar_div').dialog('close');
          $('#modificar_div').remove();

      }

  </script>
    <div class="ui-widget">
        <div class="ui-widget-header ui-corner-top">
            <h2>
                Listado de Formas de Pago </h2>
        </div>
        <div class="ui-widget-content ui-corner-bottom">
            <wce:ButtonExt ID="btnNuevo" Text="Nuevo" runat="server" OnClientClick="return false;" />
            <wce:ButtonExt ID="btnRefresh" Text="Refrescar" runat="server" OnClick="btnRefresh_Click" />                        
            <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                <ContentTemplate>
                    <wce:GridViewExt ID="GridFormaPago" runat="server"
                       DataKeyNames="OID"
                       OnRowDataBound="GridFormaPago_RowDataBound" 
                       OnPageIndexChanging="GridFormaPago_PageIndexChanging">                       
                        <Columns>
                            <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" />
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="40px">
                                <ItemTemplate>
                                    <wce:ImageButtonExt ID="btnEditar" runat="server"></wce:ImageButtonExt>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="40px">
                                <ItemTemplate>
                                    <wce:ImageButtonExt ID="btnBorrar" runat="server" 
                                        CommandName="Borrar" CommandArgument="<%# ((GridViewRow)Container).RowIndex %>"
                                        OnClick="btnBorrar_Click" OnClientClick="return ConfirmarEliminar(this.name);">
                                    </wce:ImageButtonExt>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                    </wce:GridViewExt>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnRefresh" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
            <br />
        </div>
    </div>
    <div id="dialog-confirm-delete" title="Confirmar eliminación" style="display: none">
        <p>
            <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>
            ¿Esta seguro de eliminar el registro?</p>
    </div>




</asp:Content>

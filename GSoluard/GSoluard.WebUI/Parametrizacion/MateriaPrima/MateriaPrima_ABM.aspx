﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginasMaster.Master" AutoEventWireup="true" CodeBehind="MateriaPrima_ABM.aspx.cs" Inherits="GSoluard.WebUI.Parametrizacion.MateriaPrima_ABM" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <div class="formRegistro ui-widget">	
		<div class="filaCampo">
			<div class="etiqueta">Nombre  : </div>
			<div class="campo"> <wce:TextBoxExt  ID="txtNombre" runat="server" Required="true" MaxLength="100"></wce:TextBoxExt></div>
		</div><br />
		<div class="filaCampo">
            <div class="etiqueta">Descripcion  : </div>
            <div class="campo"> <wce:TextBoxExt  ID="txtDescripcion" runat="server" TextMode="MultiLine" Height="70px"></wce:TextBoxExt></div>
		</div><br /><br /><br /><br /><br />
		<div class="filaCampo">
            <div class="etiqueta">Stock  : </div>
            <div class="campo"> <wce:TextBoxExt  ID="txtStock" runat="server" MaxLength="5" Width="40px" Type="Entero"></wce:TextBoxExt></div>
		</div>
       <br /><br />
       <div class="filaCampo">
         <wce:ButtonExt ID="btnGuardar" runat="server"  Text="Guardar" 
            onclick="btnGuardar_Click" OnClientClick="return validarForm();" />
         <wce:ButtonExt ID="btnCerrar" runat="server"  Text="Cerrar" OnClientClick="parent.CerrarPopup(); return false;"  />
       </div>
    </div>
<script type="text/javascript">
    //activa la validacion en el onblur de los campos
    validarFormBlurActivo();
    $("#<%=txtStock.ClientID %>").spinner();
</script>



</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="GSoluard.WebUI.Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Grupo Soluard - Gestión de Residuos Electrónicos</title>
    <link rel="Shortcut Icon" href="/Styles/HerGoS-theme/images/favicon.ico" type="image/x-icon" />
    <script src="Scripts/jquery.js" type="text/javascript"></script> 
    <script src="Scripts/jquery.layout.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="Scripts/jquery-ui.js"></script>
    <link href="Styles/HerGoS-theme/jquery-ui.custom.css" rel="stylesheet" type="text/css" />
    <link href="Styles/Site.css" rel="stylesheet" type="text/css" />
    <link href="Styles/HerGoS-theme/layout.css" rel="stylesheet" type="text/css" />
      <style type="text/css">

        /*.panel-header-default {
            background-image: none;
            background-color: #cbddf3;
            background-image: -webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(0%, #dae7f6), color-stop(45%, #cddef3), color-stop(46%, #abc7ec), color-stop(50%, #abc7ec), color-stop(51%, #b8cfee), color-stop(100%, #cbddf3));
            background-image: -webkit-linear-gradient(top, #dae7f6, #cddef3 45%, #abc7ec 46%, #abc7ec 50%, #b8cfee 51%, #cbddf3);
            background-image: -moz-linear-gradient(top, #dae7f6, #cddef3 45%, #abc7ec 46%, #abc7ec 50%, #b8cfee 51%, #cbddf3);
            background-image: -o-linear-gradient(top, #dae7f6, #cddef3 45%, #abc7ec 46%, #abc7ec 50%, #b8cfee 51%, #cbddf3);
            background-image: linear-gradient(top, #dae7f6, #cddef3 45%, #abc7ec 46%, #abc7ec 50%, #b8cfee 51%, #cbddf3);
            font-size: 11px;
            border: 1px solid #99bce8;        
        }       */
        .panel-header-default {
            background-image: none;
            background-color: #ffffff;
            font-size: 11px;
            border: 0px ;        
        }             
        .panel-header-text {
            color: #04408c;
            font-size: 11px;
            font-weight: bold;
            font-family: tahoma, arial, verdana, sans-serif;
            line-height: 15px;
            text-transform: none;
        }           
        .accordion-hd {
            /*background: #d9e7f8;
            border-top-color: #f3f7fb;*/
            background: #ffffff;
            border-top-color: #f3f7fb;
            padding: 4px 5px 5px 5px;
        }        
     
    </style>


   <script type="text/javascript">
       var outerLayout;

       $(function () {

           $(".submenu").menu();
           $(".menu").accordion({
               collapsible: true,
               autoHeight: false,
               clearStyle: true,
               navigation: true,
               fillspace: true
           });

           outerLayout = $("body").layout(layoutSettings_Outer);
           outerLayout.addCloseBtn("#west-closer", "west");


       });

       
       var layoutSettings_Outer = {
           name: "outerLayout"
	        , defaults: {
	            size: "auto"
		        , minSize: 50
		        , contentSelector: ".content"
		        , contentIgnoreSelector: "span"
		        , togglerLength_open: 35
		        , togglerLength_closed: 35
		        , hideTogglerOnSlide: true
		        , togglerTip_open: "Cerrar el panel"
		        , togglerTip_closed: "Abrir el panel"
		        , resizerTip: "Cambiar el tamaño"
		        , fxName: "slide"		// none, slide, drop, scale
		        , fxSpeed_open: 750
		        , fxSpeed_close: 1500
		        , fxSettings_open: { easing: "easeInQuint" }
		        , fxSettings_close: { easing: "easeOutQuint" }
	        }
	        , north: {
                spacing_open: 1	
		        , togglerLength_open: 0
		        , togglerLength_closed: -1
		        , resizable: false
		        , slidable: false
	        }
	        , south: {
	            maxSize: 200
		        , slidable: false
                , resizable: false
		        , initClosed: false
	        }
	        , west: {
	            size: 180
		        , spacing_closed: 21
		        //, togglerLength_closed: 80			
		        , togglerAlign_closed: "top"		
                //, togglerContent_closed:	"M<BR>e<BR>n<BR>u"
		        , togglerLength_open: 0			
		        , slideTrigger_open: "click" 	
		        , initClosed: false
		        , fxSettings_open: { easing: "easeOutBounce" }
	        }

       };
	</script>
</head>
<body>
    <form id="form1" runat="server">
   <div class="ui-layout-center">
      <iframe name="ifmPaginas" id="ifmPaginas" src="/Principal.aspx" frameborder="0" style="width:100%; height:100%;" scrolling="yes" runat="server"> </iframe>
    </div>
    
    <div class="ui-layout-north">
    <div id="logo" style="float:left">
     &nbsp;&nbsp;&nbsp;<img src="Styles/HerGoS-theme/images/Logo_Horiz_Grupo_Soluard.png" height="45px"  />
    </div>



      <div id="userDiv"  style="float:right;padding:10px">        
         <div id="divUsuario" class="ui-corner-all" style="padding:5px;display: inline-block;" runat="server">
         </div> 
       |
       <asp:Button ID="btnSalir" runat="server" onclick="btnSalir_Click" CssClass="ui-button ui-state-default ui-corner-all" ToolTip="Salir del sistema" Text="Salir" />
      </div>
    </div>
    

    <div class="ui-layout-west" style="padding:0px">
    <div class= "panel-header-default" style="position: relative; display: block; top: 0; left: 0; height: 15px; padding: 4px 5px 4px 5px;"> 
    <div class="panel-header-text" style="right: auto; left: 0px; top: 0px; margin: 0px; width: 171px; position: relative; display: block;"></div>    
    <div id="west-closer" style="float:right; width: 15px; height: 15px; right: 0px; top: 3px; margin: 0px; position: absolute !important; display: block; cursor: pointer;"><img src="Styles/HerGoS-theme/images/go-lt-off.gif" /></div>    
    </div>
	<div class="content accordion-hd" style="position: relative; padding:4px;">

    <div runat="server" id="MenuContainer">
    </div> 
    </div>  
    </div>
    </form>
</body>
</html>

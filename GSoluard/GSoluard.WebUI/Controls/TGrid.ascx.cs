﻿using NCalc;
using System;
using System.Collections;
using System.Data;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GSoluard.WebUI
{
    public partial class TGrid : System.Web.UI.UserControl
    {
        ArrayList cell = new ArrayList();
        Style _StyleRow = new Style();
        public TableRow RowTemplate = new TableRow();
        String _RowID = "";
        Style _StyleHeaderRow = new Style();
        Style _StyleTable = new Style();
        public string Height = "400px";
        public object Source = null;
        public bool HiddenPost = true;
        public bool Resizable = false;
        public int CellPadding = 2;
        public int CellSpacing = 0;
        int _pagesize = 20;
        int _totalpages = 0;
        bool _mostrartotal = true;
        bool _paginacion = true;
        bool _recarga = true;
        bool _scroll = true;
        bool _autoBind = true;
        bool _paginadornumerico = true;

        string ControlID = "TablaGrid";

        string _textempty = "No se han encontrado datos.";

        protected void Page_Load(object sender, EventArgs e)
        {
            ControlID = this.ID;

            if (HiddenPost)
                AddHiddenPost();
            if (_autoBind)
                BindDataHeader();
            Recarga.Style.Add("display", "none");
            if (!_recarga)
            {
                Recarga.Visible = false;
            }
            if (!this.IsPostBack && _autoBind)
            {
                BindData();
            }

            if (_paginadornumerico)
                PaginadorNumerico(1);
            if (Resizable)
                this.AddResize();
        }

        void AddHiddenPost()
        {
            foreach (string name in Request.Form)
            {
                if (name != "__VIEWSTATE" && name != "__EVENTVALIDATION" && name != "__PREVIOUSPAGE" && name != "__EVENTARGUMENT" && name != "__EVENTTARGET" && name != "__VIEWSTATEGENERATOR" && !name.Contains(ControlID))
                {
                    LiteralControl h = new LiteralControl("<input type='hidden' name='" + name + "' value='" + Request.Form[name].ToString() + "' />");
                    DivGrilla.Controls.Add(h);
                }
            }
        }

        #region Propiedades

        public string TextEmpty
        {
            get
            {
                return _textempty;
            }
            set
            {
                _textempty = value;
            }
        }

        public int PageSize
        {
            get
            {
                return _pagesize;
            }
            set
            {
                _pagesize = value;
            }
        }
        public bool AutoBind
        {
            get
            {
                return _autoBind;
            }
            set
            {
                _autoBind = value;
            }
        }
        public bool MostrarTotal
        {
            get
            {
                return _mostrartotal;
            }
            set
            {
                _mostrartotal = value;
            }
        }
        public bool MostrarScroll
        {
            get
            {
                return _scroll;
            }
            set
            {
                _scroll = value;
            }
        }
        public bool Paginacion
        {
            get
            {
                return _paginacion;
            }
            set
            {
                _paginacion = value;
            }
        }
        public bool Recargar
        {
            get
            {
                return _recarga;
            }
            set
            {
                _recarga = value;
            }
        }

        public bool PaginacionNumerica
        {
            get
            {
                return _paginadornumerico;
            }
            set
            {
                _paginadornumerico = value;
            }
        }

        public String RowID
        {
            get
            {
                return _RowID;
            }
            set
            {
                _RowID = value;
            }
        }
        public void StyleTable(Style css)
        {
            _StyleTable = css;

        }
        public void StyleRow(Style css)
        {
            _StyleRow = css;
        }
        public void StyleHeaderRow(Style css)
        {
            _StyleHeaderRow = css;
        }
        public void AddColumn(TableCellExt c)
        {
            cell.Add(c);
        }

        #endregion

        public void BindDataControl()
        {
            BindDataHeader();
            BindData();
        }

        private void BindDataHeader()
        {
            Contenedor.Style.Add("height", Height);
            if (_scroll)
                Contenedor.Attributes.Add("class", "DIVSCROLL");

            TGrilla.ApplyStyle(_StyleTable);
            TGrilla.CellPadding = this.CellPadding;
            TGrilla.CellSpacing = this.CellSpacing;

            //Creo los titulos de la tabla
            TableHeaderRow HRow = new TableHeaderRow();
            HRow.ApplyStyle(_StyleHeaderRow);
            TableHeaderCell HCel;
            foreach (TableCellExt c in cell)
            {
                HCel = new TableHeaderCell();

                HCel.Width = c.Width;
                if (c.SortExpression != null)
                {
                    LinkButton txt = new LinkButton();
                    txt.Text = c.HeaderText;
                    txt.CommandName = c.SortExpression;
                    txt.Click += new EventHandler(Grilla_Sorting);
                    txt.OnClientClick = "Grid_Loading();";
                    HCel.Controls.Add(txt);

                }
                else
                {
                    HCel.Text = c.HeaderText;
                }
                HRow.Cells.Add(HCel);
            }
            HCel = new TableHeaderCell();
            HCel.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            HCel.Attributes.Add("style", "width:40px");
            HCel.Wrap = false;
            HRow.Cells.Add(HCel);

            TGrilla.Rows.Add(HRow);

            if (Source != null)
            {
                _totalpages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble((double)((DataTable)Source).Rows.Count / (double)_pagesize)));
                lblTotalRegCon.Text = Convert.ToString(((DataTable)Source).Rows.Count);
            }
            else
            {
                _totalpages = 0;
                lblTotalRegCon.Text = "0";
            }

            lblTotPages.Text = Convert.ToString(_totalpages);

            if (!_mostrartotal)
            {
                trTotales.Visible = false;
            }
            if (!_paginacion)
            {
                trPaginacion.Visible = false;
                trTotalesPagina.Visible = false;

                if (_paginadornumerico)
                    PaginadorNumerico(1);
            }
        }

        private void BindData()
        {
            if (Source == null)
            {
                //--Si no hay un datasource asociado a la grilla---
                TableRow Row_empty = new TableRow();
                Row_empty.ApplyStyle(_StyleRow);
                TableCell cell_empty = new TableCell();
                cell_empty.Text = _textempty;
                cell_empty.HorizontalAlign = HorizontalAlign.Center;
                cell_empty.ColumnSpan = cell.Count;
                cell_empty.Style.Add("height", Height);
                Row_empty.Cells.Add(cell_empty);

                TGrilla.Rows.Add(Row_empty);

                //------------------------------------
                return;
            }

            if (SortExpression.Value != "")
            {
                string[] _sortexpressions = SortExpression.Value.Split(',');
                for (int j = 0; j < _sortexpressions.Length; j++)
                {
                    _sortexpressions[j] += " " + SortDirection.Value;
                }

                ((DataTable)Source).DefaultView.Sort = String.Join(",", _sortexpressions);
                //((DataTable)Source).DefaultView.Sort = SortExpression.Value + " " + SortDirection.Value;
            }


            DataTable SourceTemp = ((DataTable)Source).DefaultView.ToTable();

            DataColumn myDataColumn = new DataColumn();
            myDataColumn.ColumnName = "auto_ID";
            myDataColumn.DataType = System.Type.GetType("System.Int32");

            SourceTemp.Columns.Add(myDataColumn);

            int i = 1;
            foreach (DataRow item in SourceTemp.Rows)
            {
                item["auto_ID"] = i++;
            }
            DataRow[] filas;

            if (_paginacion)
            {
                /*Paginacion del DataTable*/
                int _pageindex = Convert.ToInt32(PageIndex.Value);
                _pageindex = (_pageindex == 0) ? 1 : _pageindex;
                int maxid = _pagesize * _pageindex;
                int minid = maxid - _pagesize;

                filas = SourceTemp.Select("auto_ID > " + minid + " and auto_ID <= " + maxid);

                if (_paginadornumerico)
                    PaginadorNumerico(_pageindex);
            }
            else
            {
                filas = SourceTemp.Select();
            }


            bool decodificar = false;

            //Cargo el listado
            foreach (DataRow RS in filas)
            {
                TableRow Row = new TableRow();
                Row.ApplyStyle(_StyleRow);

                Row.ID = RS["auto_ID"].ToString();
                //RowTemplate.Attributes.Add("onclick", "alert('[RowID]')");


                //---------------------------------------
                //Cargo los atributos del Row template y reemplazo y si hay un campo de la base
                IEnumerator keys = RowTemplate.Attributes.Keys.GetEnumerator();

                while (keys.MoveNext())
                {
                    String key = (String)keys.Current;
                    String value = (String)RowTemplate.Attributes[key];

                    /*Reemplazo un campo en el texto por el valor del DataTable*/
                    MatchCollection pMatches = Regex.Matches(value, @"\[(.*?)\]");
                    foreach (Match m in pMatches)
                    {
                        string campo = m.Groups[1].Value;

                        if (campo == "RowID")
                            value = value.Replace(m.ToString(), this.ID + "_" + Row.ClientID);
                        else
                            value = value.Replace(m.ToString(), RS[campo].ToString());
                    }

                    /*Busco si hay que evaluar alguna condicion*/
                    pMatches = Regex.Matches(value, @"\{(.*?)\}");

                    foreach (Match m in pMatches)
                    {
                        string campo = m.Groups[1].Value;

                        NCalc.Expression e = new NCalc.Expression(campo);
                        /*e.EvaluateFunction += delegate (string name, FunctionArgs args)
                        {
                            if (name == "EncryptString")
                                args.Result = Crypt.EncryptString(args.Parameters[0].Evaluate().ToString(), args.Parameters[1].Evaluate().ToString());
                            if (name == "DecryptString")
                                args.Result = Crypt.DecryptString(args.Parameters[0].Evaluate().ToString(), args.Parameters[1].Evaluate().ToString());
                        };*/
                        campo = e.Evaluate().ToString();

                        value = value.Replace(m.ToString(), campo);
                        value = value.Replace(m.ToString(), campo);
                    }

                    Row.Attributes[key] = value;
                }


                /*---------------------------------------------------------*/

                //-----------------------------------------


                TableCell Cel;
                foreach (TableCellExt c in cell)
                {
                    Cel = (TableCell)clone_control(c);


                    if (c.ID != null)
                    {
                        MatchCollection idMatches = Regex.Matches(c.ID, @"\[(.*?)\]");

                        foreach (Match m in idMatches)
                        {
                            string campo = m.Groups[1].Value;
                            Cel.ID = Cel.ID.Replace(m.ToString(), RS[campo].ToString());
                        }
                    }

                    if (c.DataField != null)
                    {

                        string texto = Format(RS[c.DataField].GetType(), RS[c.DataField].ToString(), c.Format);
                        Cel.Text = (texto != "" && texto != String.Empty) ? texto : "&nbsp;";
                    }
                    else
                    {
                        String texto = c.Text;
                        /*Reemplazo un campo en el texto por el valor del DataTable*/
                        MatchCollection pMatches = Regex.Matches(texto, @"\[(.*?)\]");

                        foreach (Match m in pMatches)
                        {
                            string campo = m.Groups[1].Value;
                            string rp = "";

                            switch (campo)
                            {
                                case "RowID":
                                    rp = this.ID + "_" + Row.ClientID;
                                    break;
                                case "NroFila":
                                    rp = Row.ClientID;
                                    break;

                                default:
                                    if (RS[campo].ToString().Contains("\'") || RS[campo].ToString().Contains("\\"))
                                    {
                                        rp = Format(RS[campo].GetType(), RS[campo].ToString().Replace("\'", "%27"), c.Format);
                                        rp = Format(RS[campo].GetType(), RS[campo].ToString().Replace("\\", "&#92;"), c.Format);

                                        Cel.Attributes["hayQueDecodificar"] = "true";
                                        decodificar = true;
                                    }
                                    else
                                    {
                                        rp = Format(RS[campo].GetType(), RS[campo].ToString(), c.Format);
                                    }
                                    break;
                            }

                            texto = texto.Replace(m.ToString(), rp);
                        }
                        /*---------------------------------------------------------*/

                        /*Busco si hay que evaluar alguna condicion*/
                        pMatches = Regex.Matches(texto, @"\{(.*?)\}");

                        foreach (Match m in pMatches)
                        {
                            string source = m.Groups[1].Value;

                            NCalc.Expression e = new NCalc.Expression(source);
                           /* e.EvaluateFunction += delegate (string name, FunctionArgs args)
                            {
                                if (name == "EncryptString")
                                    args.Result = Crypt.EncryptString(args.Parameters[0].Evaluate().ToString(), args.Parameters[1].Evaluate().ToString());
                                if (name == "DecryptString")
                                    args.Result = Crypt.DecryptString(args.Parameters[0].Evaluate().ToString(), args.Parameters[1].Evaluate().ToString());
                            };*/

                            source = e.Evaluate().ToString();
                            texto = texto.Replace(m.ToString(), source);
                        }
                        /*---------------------------------------------------------*/

                        Cel.Text = (texto != "" && texto != String.Empty) ? texto : "&nbsp;";
                    }

                    Row.Cells.Add(Cel);

                }

                Cel = new TableCell();
                Cel.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                Cel.Attributes.Add("style", "width:20px");
                Cel.Wrap = false;
                Row.Cells.Add(Cel);

                TGrilla.Rows.Add(Row);

            }

            lblTotalRegPag.Text = Convert.ToString(TGrilla.Rows.Count - 1);

            if (decodificar)
            {
                if (!this.Page.ClientScript.IsClientScriptBlockRegistered(this.Page.GetType(), "desencodear_funcion"))
                {
                    string script = @" function desencodear(gridID) {
                                    var elementos = [];

                                    var table = document.getElementById(gridID);

                                    var allElements = table.getElementsByTagName('td');
                                    for (var i = 0; i < allElements.length; i++) {                                        
                                        if (allElements[i].getAttribute('hayQueDecodificar') !== null) {
                                            // Element exists with attribute. Add to array.
                                            elementos.push(allElements[i]);
                                            allElements[i].removeAttribute('hayQueDecodificar');
                                        }
                                    }

                                    for (var i = 0; i < elementos.length; i++) {
                                        var elemento = elementos[i];
                                        elemento.innerHTML = decodeURIComponent(elemento.innerHTML);
                                    }
                                }" + Environment.NewLine;

                    this.Page.ClientScript.RegisterClientScriptBlock(this.Page.GetType(), "desencodear_funcion", script, true);
                }

                if (!this.Page.ClientScript.IsClientScriptBlockRegistered(this.Page.GetType(), "desencodear" + TGrilla.ClientID))
                {
                    this.Page.ClientScript.RegisterClientScriptBlock(this.Page.GetType(), "desencodear" + TGrilla.ClientID, @"$(document).ready(function () {desencodear('" + TGrilla.ClientID + "'); }); " + Environment.NewLine, true);
                }
            }

            if (!this.Page.ClientScript.IsClientScriptBlockRegistered(this.Page.GetType(), "FreezeGridViewHeader_funcion"))
            {
                string script = @"function FreezeGridViewHeader(gridID, headerID) 
                                {
                                    var grid = document.getElementById(gridID);
                                    var theader = document.getElementById(headerID);
                                    if (grid != 'undefined')
                                    {
                                        grid.style.visibility = 'hidden';               

                                        var tags = grid.getElementsByTagName('TBODY');
                                        if (tags != 'undefined')
                                        {        
                                            var tbody = tags[0];
                                            var trs = tbody.getElementsByTagName('TR');
                
                                            var headerHeight = 8;
                                            if (trs != 'undefined') 
                                            {
                                                var headTR = tbody.removeChild(trs[0]);
                                                var header = theader.getElementsByTagName('TBODY');

                                                if(header.length == 0)
                                                    header = theader;
                                                else
                                                    header = header[0];
                                            
                                                header.appendChild(headTR);

                                            }
                                        }
                                        grid.style.visibility = 'visible';
                                    }
                                }" + Environment.NewLine;

                //script += @"function ReloadGrid (){ __doPostBack('" + Recarga.ClientID.Replace("_", "$") + "','');}" + Environment.NewLine;
                //script += @"function Grid_Loading() { document.getElementById('GridLoadingBack').style.display = 'block'; document.getElementById('GridLoading').style.display = 'block';   return true; }" + Environment.NewLine;

                this.Page.ClientScript.RegisterClientScriptBlock(this.Page.GetType(), "FreezeGridViewHeader_funcion", script, true);
            }

            if (TGrilla.Rows.Count - 1 <= 0)
                PageIndex.Value = "0";

            lblCurPage.Text = Convert.ToString(PageIndex.Value);

            //--Si no hay registros en la grilla---
            if (filas.Length == 0)
            {
                TableRow Row_empty = new TableRow();
                Row_empty.ApplyStyle(_StyleRow);

                TableCell cell_empty = new TableCell();
                cell_empty.Text = _textempty;
                cell_empty.HorizontalAlign = HorizontalAlign.Center;
                cell_empty.ColumnSpan = cell.Count;
                cell_empty.Style.Add("height", Height);

                Row_empty.Cells.Add(cell_empty);

                TGrilla.Rows.Add(Row_empty);

            }
            //------------------------------------




            ButtonDisable();
        }

        internal string ExpText(Match m)
        {
            return m.Groups[1].Value;

        }

        internal string Format(Type t, String text, String format)
        {
            //Ejemplos de formatos
            //http://www.csharp-examples.net/string-format-int/
            if (format == "%")
            {
                try
                {
                    return (Convert.ToDecimal(text) / 100).ToString("p2", CultureInfo.GetCultureInfo("es-AR"));
                }
                catch
                {
                    return text;
                }
            }
            if (format == ".%")
            {
                try
                {
                    return Convert.ToDecimal(text).ToString("p2", CultureInfo.GetCultureInfo("es-AR"));
                }
                catch
                {
                    return text;
                }
            }
            if (format == "$")
            {
                try
                {
                    return Convert.ToDecimal(text).ToString("C2", CultureInfo.GetCultureInfo("es-AR"));
                }
                catch
                {
                    return text;
                }
            }
            if (t.ToString() == "System.Double" || t.Equals(typeof(decimal)))
            {
                if (format != "")
                {
                    try
                    {
                        return String.Format(format, Convert.ToDecimal(text));
                    }
                    catch
                    {
                        return String.Format(format, 0);
                    }
                }
                else
                {
                    return FormatNumber(text, 2);
                }
            }

            if (t.ToString() == "System.DateTime")
            {
                if (format != "")
                {
                    try
                    {
                        return System.Convert.ToDateTime(text).ToString(format);
                    }
                    catch
                    {
                        return text;
                    }
                }
                else
                {
                    return FechaDMA(text);
                }
            }

            if (t.ToString() == "System.String")
            {
                if (format != "")
                {
                    try
                    {
                        return String.Format(format, text);
                    }
                    catch
                    {
                        return String.Format(format, "");
                    }
                }
            }

            if (t.ToString() == "System.Int16" || t.ToString() == "System.Int32" || t.ToString() == "System.Int64")
            {
                if (format != "")
                {
                    try
                    {
                        return String.Format(format, Convert.ToInt64(text));
                    }
                    catch
                    {
                        return String.Format(format, 0);
                    }
                }
            }

            return text;

        }

        internal void PaginadorNumerico(int NroPaginas)
        {

            tdPaginadorNumerico.InnerHtml = "";

            int ultimapagina = 1, _nropaginas;

            if (NroPaginas >= 1 && NroPaginas <= 3)
                ultimapagina = (NroPaginas + (7 - NroPaginas));
            else
                ultimapagina = (NroPaginas + 3);

            _nropaginas = NroPaginas;

            if (ultimapagina >= _totalpages)
            {
                ultimapagina = _totalpages;
                _nropaginas = _totalpages - 3;
            }

            tdPaginadorNumerico.InnerHtml = "<b>Página </b>";

            if (_totalpages == 0)
            {
                Button b = new Button();
                b.Text = _totalpages.ToString();
                b.Height = 16;
                b.Width = 25;
                b.Style.Add("align", "center");
                b.CssClass = "BotonImagen TextoBoton";
                b.Visible = true;
                tdPaginadorNumerico.Controls.Add(b);
            }
            else
            {
                for (int i = 1; i <= _totalpages; i++)
                {
                    Button b = new Button();
                    b.Text = i.ToString();
                    b.ID = "bt_" + i.ToString();
                    b.CommandName = i.ToString();
                    b.Height = 16;
                    b.Width = 25;
                    b.Style.Add("align", "center");

                    if (NroPaginas == i)
                        b.CssClass = "TextoBoton CurrentPage";
                    else
                        b.CssClass = "BotonImagen TextoBoton";

                    b.OnClientClick = "Grid_Loading()";

                    if (i >= (_nropaginas - 3) && i <= ultimapagina)
                        b.Visible = true;
                    else
                        b.Visible = false;

                    b.Click += delegate (object o, EventArgs e) { NavigationButtonClick(o, e); };

                    tdPaginadorNumerico.Controls.Add(b);
                }
            }
            Label lbl = new Label();
            lbl.Text = "<b> de " + _totalpages + "</b>";
            tdPaginadorNumerico.Controls.Add(lbl);

        }

        public void NavigationButtonClick(object sender, EventArgs e)
        {

            string opcion;

            opcion = ((Button)sender).CommandName;

            Paginar(opcion);

        }

        public void Paginar(string direccion)
        {

            switch (direccion)
            {
                case "FIRST":
                    PageIndex.Value = "1";
                    break;
                case "PREVIOUS":
                    if (Convert.ToInt32(PageIndex.Value) > 1)
                        PageIndex.Value = Convert.ToString((Convert.ToInt32(PageIndex.Value) - 1));
                    break;
                case "NEXT":
                    if (Convert.ToInt32(PageIndex.Value) < _totalpages)
                        PageIndex.Value = Convert.ToString((Convert.ToInt32(PageIndex.Value) + 1));
                    break;
                case "LAST":
                    PageIndex.Value = Convert.ToString(_totalpages);
                    break;
                default:
                    PageIndex.Value = direccion.ToString();
                    break;
            }
            BindData();
        }

        public void ButtonDisable()
        {
            if (PageIndex.Value == "1" || PageIndex.Value == "0")
            {
                btFIRST.Enabled = false;
                btPREVIOUS.Enabled = false;
            }
            else
            {
                btFIRST.Enabled = true;
                btPREVIOUS.Enabled = true;
            }
            if (Convert.ToInt32(PageIndex.Value) >= _totalpages)
            {
                btNEXT.Enabled = false;
                btLAST.Enabled = false;
            }
            else
            {
                btNEXT.Enabled = true;
                btLAST.Enabled = true;
            }

        }

        public void Grilla_Sorting(Object sender, EventArgs e)
        {
            LinkButton c = (LinkButton)sender;

            string sortDirection = "ASC";
            string sortExpression = c.CommandName;

            if (sortExpression != null)
            {
                if (sortExpression == SortExpression.Value)
                {
                    string lastDirection = SortDirection.Value;
                    if ((lastDirection != null) && (lastDirection == "ASC"))
                    {
                        sortDirection = "DESC";
                    }
                }
            }

            SortExpression.Value = sortExpression;
            SortDirection.Value = sortDirection;

            BindData();

        }

        public void Grilla_reload(object sender, EventArgs e)
        {
            BindData();
        }
        public void AddResize()
        {
            #region ResizeScript

            string ResizeScript = String.Format(@"
function EsperaJquery() {{        

        if (window.jQuery) {{            
            Setheight();
            setTimeout(Setheight, 0);
        }} else {{
            setTimeout(EsperaJquery, 10000);
        }}
    }}


    function Setheight() {{
        if (getParentFrame() !== null ) {{
            if (!($(getParentFrame()).closest('div.drag').length > 0)) {{                      
                if (getParentFrame(window.parent) !== null) {{                                                
                var alto = $(getParentFrame(window.parent)).outerHeight(true) - ($(getParentFrame()).closest('body').outerHeight(true) - $(getParentFrame()).height())
                    $(getParentFrame()).height((alto < 300)? 300:alto);
                }}
            }}       
            $('#{0}_Contenedor').height($(getParentFrame()).height() - ($('#{0}_Contenedor').closest('body').outerHeight(true) - $('#{0}_Contenedor').outerHeight(true)));                        
        }}  
    }}
    function getParentFrame(win) {{
        try {{
            win = win || window;
            var iframes = $('iframe, frame', win.parent.document);
            for (var i = iframes.length; i-- > 0; ) {{
                var iframe = iframes[i];
                var idoc = '';
                try {{
                    idoc = 'contentDocument' in iframe ? iframe.contentDocument : iframe.contentWindow.document;
                }} catch (e) {{
                    continue;
                }}
                if (idoc === win.document)
                    return iframe;
            }}
            return null;
        }} catch (e) {{
            return null;
        }}
    }}
    (function () {{
        if (!window.jQuery) {{
            document.write('<script src= ""/Scripts/jquery.js""><\/script>');
        }}
        if (window.addEventListener) {{
            window.addEventListener('load', EsperaJquery);
        }} else {{
            window.attachEvent('onload', EsperaJquery);
        }}
    }} ());
 ", ControlID);


            #endregion


            if (!this.Page.ClientScript.IsClientScriptBlockRegistered(this.Page.GetType(), "javascript"))
            {
                this.Page.ClientScript.RegisterClientScriptBlock(this.Page.GetType(), "javascript", "<script>" + ResizeScript + "</script>");
            }
        }

        #region Funciones
        string FormatNumber(object val, int dec)
        {
            String ceros = "";
            for (int i = 0; i < dec; i++)
            {
                ceros = ceros + "0";
            }
            if (ceros != "") ceros = "." + ceros;
            try
            {
                return String.Format("{0:,0" + ceros + "}", Convert.ToDecimal(val));
            }
            catch
            {
                return String.Format("{0:,0" + ceros + "}", Convert.ToDecimal("0" + ceros));
            }

        }

        string FechaDMA(string f)
        {
            try
            {
                DateTime fech = System.Convert.ToDateTime(f);

                return fech.ToString("dd/MM/yyyy");

            }
            catch (FormatException)
            {
                try
                {
                    return DateTime.ParseExact(f, "yyyyMMdd", null).ToString("dd/MM/yyyy");
                }
                catch (Exception)
                {

                    return "";
                }

            }
        }

        /// <summary>Clona un control.
        /// <param name="src_ctl">El control origen a clonar.</param>
        /// </summary>
        Control clone_control(Control src_ctl)
        {
            try
            {
                Type t = src_ctl.GetType();
                Object obj = Activator.CreateInstance(t);
                Control dst_ctl = (Control)obj;

                System.ComponentModel.PropertyDescriptorCollection src_pdc = System.ComponentModel.TypeDescriptor.GetProperties(src_ctl);
                System.ComponentModel.PropertyDescriptorCollection dst_pdc = System.ComponentModel.TypeDescriptor.GetProperties(dst_ctl);

                for (int i = 0; i < src_pdc.Count; i++)
                {

                    if (src_pdc[i].Attributes.Contains(System.ComponentModel.DesignerSerializationVisibilityAttribute.Content))
                    {

                        object collection_val = src_pdc[i].GetValue(src_ctl);
                        if ((collection_val is IList) == true)
                        {
                            foreach (object child in (IList)collection_val)
                            {
                                Control new_child = clone_control(child as Control);
                                object dst_collection_val = dst_pdc[i].GetValue(dst_ctl);
                                ((IList)dst_collection_val).Add(new_child);
                            }
                        }
                    }
                    else
                    {
                        dst_pdc[src_pdc[i].Name].SetValue(dst_ctl, src_pdc[i].GetValue(src_ctl));
                    }
                }
                return dst_ctl;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

    }
    /*Extencion del Control TableCell*/
    public class TableCellExt : TableCell
    {
        private string _DataField;
        private string _HeaderText;
        private string _SortExpression;
        private string _format;

        public TableCellExt()
        {
            _format = "";
        }

        /*Propiedades Extendidas*/
        public string DataField
        {
            get
            {
                return _DataField;
            }
            set
            {
                _DataField = value;
            }
        }

        public string HeaderText
        {
            get
            {
                return _HeaderText;
            }
            set
            {
                _HeaderText = value;
            }
        }
        public string SortExpression
        {
            get
            {
                return _SortExpression;
            }
            set
            {
                _SortExpression = value;
            }
        }
        public string Format
        {
            get
            {
                return _format;
            }
            set
            {
                _format = value;
            }
        }
    }
}
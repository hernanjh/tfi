﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GSoluard.Administration.Entities;
using GSoluard.ControlsUI;
using GSoluard.Administration.Engine.Parametrizacion;
using GSoluard.WebControlExtend;
using GSoluard.Administration.Engine.Procesos;
using System.Globalization;
using System.Threading;

namespace GSoluard.WebUI.Parametrizacion
{
    public partial class Productos_Listado :  PageBase
    {
       
        #region Propiedades
        public List<Producto> Productos
        {
            get
            {
                if (ViewState["Producto"] != null)
                    return (List<Producto>)ViewState["Producto"];
                return null;
            }
            set
            {
                ViewState["Producto"] = value;
            }

        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("es-AR");
            if (!Page.IsPostBack)
            {
                Initialize();
                LoadGrid(false);
            }
        }

        void Initialize()
        {
            this.GridProductos.Settings(new GridViewExtBasic());
            this.btnNuevo.Settings(new ButtonExtText());
            this.btnRefresh.Settings(new ButtonExtText());
            this.btnFiltrar.Settings(new ButtonExtText());
            this.bntQuitarFiltros.Settings(new ButtonExtText());
            this.txtNombreDescripcion.Settings(new TextBoxExtStyle());
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadGrid(false);
        }

        protected void btnBorrar_Click(object sender, EventArgs e)
        {
            ImageButton btn = (ImageButton)sender;
            int index = Convert.ToInt32(btn.CommandArgument);
            int id = Convert.ToInt32(GridProductos.DataKeys[index].Value);

            Producto obj = Productos.Find(c => c.OID == id);
            Productos.Remove(obj);

            GestorProducto.Eliminar(obj);
            LoadGrid(true);

        }

        

        private void LoadGrid(bool cache)
        {
            if (!cache)
                Productos = GestorProducto.Consultar();

            this.GridProductos.DataSource = Productos;
            this.GridProductos.DataBind();
        }

        protected void GridProductos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButtonExt Editar = e.Row.FindControl("btnEditar") as ImageButtonExt;
                if (Editar != null)
                {
                    Editar.Settings(new ImageButtonExtEdit());
                    int oid = DataBinder.Eval(e.Row.DataItem, "OID").ToInteger();
                    Editar.OnClientClick = "Editar(" + oid.ToString() + "); return false;";
                }

                ImageButtonExt Borrar = e.Row.FindControl("btnBorrar") as ImageButtonExt;
                if (Borrar != null)
                {
                    Borrar.Settings(new ImageButtonExtDelete());
                }

            }
        }

        protected void GridProductos_PageIndexChanging(Object sender, GridViewPageEventArgs e)
        {
            GridProductos.PageIndex = e.NewPageIndex;
            LoadGrid(true);
        }

        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            List<Producto> filtroResult = null;

            if (!string.IsNullOrEmpty(this.txtNombreDescripcion.Text))
            {
                filtroResult = Productos.FindAll(FindProductos);
            }

            if (filtroResult == null) filtroResult = Productos;

            this.GridProductos.DataSource = filtroResult;
            this.GridProductos.DataBind();
        }
        private bool FindProductos(Producto obj)
        {
            if (obj.Nombre.ToLower().Contains(this.txtNombreDescripcion.Text.ToLower()) || obj.Descripcion.ToLower().Contains(this.txtNombreDescripcion.Text.ToLower()))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected void btnQuitarFiltros_Click(object sender, EventArgs e)
        {
            this.txtNombreDescripcion.Text = string.Empty;

            LoadGrid(true);

        }


    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GSoluard.Administration.Entities;
using GSoluard.Administration.Engine.Parametrizacion;
using GSoluard.ControlsUI;
using GSoluard.Administration.Engine.Procesos;
using GSoluard.WebControlExtend;
using GSoluard.Interfaces;
using System.Globalization;
using System.Threading;

namespace GSoluard.WebUI.Parametrizacion
{
    public partial class Pedidos_ABM : PageBase
    {

        #region Propiedades

        public Pedido Pedido
        {
            get
            {
                if (ViewState["Pedido"] != null)
                    return (Pedido)ViewState["Pedido"];
                return null;
            }
            set
            {
                ViewState["Pedido"] = value;
            }

        }

        public int ContadorDetalles
        {
            get
            {
                if (ViewState["ContadorDetalles"] != null)
                    return (int)ViewState["ContadorDetalles"];
                return 0;
            }
            set
            {
                ViewState["ContadorDetalles"] = value;
            }
        }

        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("es-AR");

            if (!Page.IsPostBack)
            {
               
                if (Request.QueryString["id"] == null)
                {
                    Pedido = new Pedido();
                }
                else
                {
                    var Id = Convert.ToInt32(Request.QueryString["id"]);
                    Pedido = GestorPedidos.Obtener(Id);
                    this.txtNumero.Text = Pedido.Numero.ToString();
                    this.txtUsuario.Text = Pedido.Usuario.NombresYApellidos;
                    this.txtFormaPago.Text = Pedido.FormaPago.ToString();
                    this.txtFecha.Text = Pedido.Fecha.Value.ToShortDateString();
                    LoadGridDetalles();
                }
                Initialize();
            }

        }

        void Initialize()
        {
            this.btnGuardar.Settings(new ButtonExtText());
            this.btnCerrar.Settings(new ButtonExtText());
            this.txtNumero.Settings(new TextBoxExtStyle());
            this.txtUsuario.Settings(new TextBoxExtStyle());
            this.txtFormaPago.Settings(new TextBoxExtStyle());
            this.txtProducto.Settings(new TextBoxExtStyle());
            //this.txtFecha.Settings(new TextBoxExtStyle());

            this.GridDetalle.Settings(new GridViewExtBasic());

            this.btn_AgregarDetalle.Settings(new ButtonExtText());
            this.btn_Cerrar_dialog_detalle.Settings(new ButtonExtText());


        }



        private void LoadGridDetalles()
        {
            GridDetalle.DataSource = GestorPedidoDetalle.Activos(Pedido);
            GridDetalle.DataBind();

        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {

            GestorPedidos.Guardar(Pedido);

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "closedialog", "parent.IntervaloCerrarPopup();", true);

        }


        protected void btnCerrar_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "closedialog", "parent.CerrarPopup();", true);
        }


        protected void GridDetalle_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Borrar")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                int id = Convert.ToInt32(GridDetalle.DataKeys[index].Value);


                if (id > 0)
                {
                    PedidoDetalle detalle = Pedido.PedidoDetalles.Where(x => x.OID == id).FirstOrDefault();
                    detalle.Persistencia = EstadoPersistencia.DELETE;

                }
                else
                {
                    Pedido.PedidoDetalles.RemoveAt(index);

                }
                LoadGridDetalles();
            }
            if (e.CommandName == "Editar")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                int oid = Convert.ToInt32(GridDetalle.DataKeys[index].Value);
                this.hiddetalle.Value = oid.ToString();
                PedidoDetalle detalle = Pedido.PedidoDetalles.Find(y => y.OID == oid);
                this.txtCantidad.Text = detalle.Cantidad.ToString();
                this.txtProducto.Text = detalle.Producto.ToString();
                this.txtPrecio.Text = detalle.Precio.ToString("F2");
                UpdatePanelABMDetalle.Update();

                ScriptManager.RegisterStartupScript(Page, typeof(Page), "Edicion", "DialogDetalle();", true);
            }


        }

        protected void GridDetalle_RowDataBound(object sender, GridViewRowEventArgs e)
        {


            ImageButtonExt Editar = e.Row.FindControl("btnEditar") as ImageButtonExt;
            if (Editar != null)
            {

                Editar.Settings(new ImageButtonExtEdit());

            }

        }

        protected void btn_AgregarDetalle_Click(object sender, EventArgs e)
        {

            try
            {
                int idDetalle = this.hiddetalle.Value.ToInteger();
                if (idDetalle != 0)
                {
                    foreach (var item in Pedido.PedidoDetalles.Where(x => x.OID == idDetalle))
                    {
                        item.Cantidad = this.txtCantidad.Text.ToInteger();
                        
                    }

                    LoadGridDetalles();
                    hiddetalle.Value = "0";
                }

                this.txtProducto.Text = string.Empty;
                this.txtCantidad.Text = string.Empty;
                this.txtPrecio.Text = string.Empty;

            }
            catch (Exception ex)
            { }

        }
    }
}
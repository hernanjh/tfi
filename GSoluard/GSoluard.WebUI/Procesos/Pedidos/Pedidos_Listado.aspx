﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginasMaster.Master" AutoEventWireup="true" CodeBehind="Pedidos_Listado.aspx.cs" Inherits="GSoluard.WebUI.Parametrizacion.Pedidos_Listado" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <script language="javascript" type="text/javascript">

      $(document).ready(function () {



      });

      function Editar(id) {
          LoadPageDialog("Pedidos_ABM.aspx?id=" + id, " Modificar", 700, 500, "modificar_div");
      }
      function Imprimir(id) {

          LoadPageDialogSinTitulo("Pedido_Impresion.aspx?id=" + id, "Impresion del Pedido", 1091, 550, "impresion_div");

      }


      function ConfirmarEliminar(control) {

          $("#dialog-confirm-delete").dialog({
              resizable: false,
              height: 180,
              modal: true,
              buttons: {
                  "Eliminar": function () {
                      __doPostBack(control, '');
                      $(this).dialog("close");
                  },
                  "Cancelar": function () {
                      $(this).dialog("close");
                  }
              }
          });
          return false;
      }

      function IntervaloCerrarPopup() {
          notify("El Pedido ha sido guardado correctamente", 'MESSAGE');
          CerrarPopup();
          $("#<%=btnRefresh.ClientID %>").click();

      }
      function CerrarPopup() {

          $('#modificar_div').dialog('close');
          $('#modificar_div').remove();

      }

  </script>
    <div class="ui-widget">
        <div class="ui-widget-header ui-corner-top">
            <h2>
                Listado de Pedidos </h2>
        </div>
        <div id = "filtros" class="accordion">
          <h3>Filtros </h3>  
         <div>
         <table>
                  <tr>
                     <td>
                     Numero:
                     </td>
                     <td>
                      <wce:TextBoxExt  ID="txtNumero" runat="server" MaxLength="50" Type="Entero" ></wce:TextBoxExt>
                      </td>         
                  </tr>
        </table>               
      
        <br>
        <wce:ButtonExt ID="btnFiltrar" Text="Filtrar" runat="server" onclick = "btnFiltrar_Click" />
         <wce:ButtonExt ID="bntQuitarFiltros" Text="QuitarFiltros" runat="server" onclick = "btnQuitarFiltros_Click" />
        <hr />
        </div>
      </div>
        <div class="ui-widget-content ui-corner-bottom">
            <wce:ButtonExt ID="btnRefresh" Text="Refrescar" runat="server" OnClick="btnRefresh_Click" />                        
            <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                <ContentTemplate>
                    <wce:GridViewExt ID="GridPedidos" runat="server"
                       DataKeyNames="OID"
                       OnRowDataBound="GridPedidos_RowDataBound" 
                       OnPageIndexChanging="GridPedidos_PageIndexChanging">                       
                        <Columns>
                            <asp:BoundField DataField="Fecha" HeaderText="Fecha" ItemStyle-Width="120px"  ItemStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy}"/>
                            <asp:BoundField DataField="Numero" HeaderText="Numero"  ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="Usuario" HeaderText="Usuario" ItemStyle-HorizontalAlign="Center" />
                            <asp:TemplateField  ItemStyle-HorizontalAlign="Right">
                                <HeaderTemplate>Total</HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblTotal" runat="server" ></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="FormaPago" HeaderText="Forma de Pago" ItemStyle-HorizontalAlign="Center" />
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="40px">
                                <ItemTemplate>
                                    <wce:ImageButtonExt ID="btnImprimir" runat="server"></wce:ImageButtonExt>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                        </Columns>
                    </wce:GridViewExt>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnRefresh" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
            <br />
        </div>
    </div>
    <div id="dialog-confirm-delete" title="Confirmar eliminación" style="display: none">
        <p>
            <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>
            ¿Esta seguro de eliminar el registro?</p>
    </div>




</asp:Content>

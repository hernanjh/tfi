﻿using GSoluard.Administration.Entities;
using GSoluard.Administration.Engine.Parametrizacion;
using GSoluard.ControlsUI;
using GSoluard.Administration.Engine.Procesos;
using GSoluard.WebControlExtend;
using GSoluard.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Globalization;

namespace GSoluard.WebUI.Parametrizacion
{
    public partial class Pedido_Impresion : System.Web.UI.Page
    {
 
        protected void Page_Load(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("es-AR");
            this.btnImprimir.Settings(new ButtonExtText());

            if (Request.QueryString["id"] != null)
            {
                var Id = Convert.ToInt32(Request.QueryString["id"]);
                Pedido pedido = GestorPedidos.Obtener(Id);
                

                this.txtfecha.Text = pedido.Fecha.Value.ToShortDateString();
                this.txtNumero.Text = pedido.Numero.ToString();
                this.txtUsuario.Text = pedido.Usuario.NombresYApellidos;
                this.txtFormaPago.Text = pedido.FormaPago.ToString();

                GridReporte.DataSource = GestorPedidoDetalle.Activos(pedido);
                GridReporte.DataBind();

                float total = ((List<PedidoDetalle>)GridReporte.DataSource).AsEnumerable().Sum(row => row.Precio * row.Cantidad);
                GridReporte.FooterRow.Cells[2].Text = "Total";
                GridReporte.FooterRow.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                GridReporte.FooterRow.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                GridReporte.FooterRow.Cells[3].Text = total.ToString("C2");

            }
        }

        protected void GridReporteTramos_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void GridReporte_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                PedidoDetalle pedidodetalle = (PedidoDetalle)e.Row.DataItem;

                float total = pedidodetalle.Cantidad * pedidodetalle.Precio;

                Label lab = e.Row.FindControl("lblSubtotal") as Label;
                if (lab != null)
                {
                    lab.Text = total.ToString("C2");
                }

            }
        }
    }
}
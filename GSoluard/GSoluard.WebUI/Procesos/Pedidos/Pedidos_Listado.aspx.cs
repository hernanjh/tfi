﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GSoluard.Administration.Entities;
using GSoluard.ControlsUI;
using GSoluard.Administration.Engine.Parametrizacion;
using GSoluard.WebControlExtend;
using GSoluard.Administration.Engine.Procesos;
using System.Globalization;
using System.Threading;

namespace GSoluard.WebUI.Parametrizacion
{
    public partial class Pedidos_Listado :  PageBase
    {
       
        #region Propiedades
        public List<Pedido> Pedidos
        {
            get
            {
                if (ViewState["Pedido"] != null)
                    return (List<Pedido>)ViewState["Pedido"];
                return null;
            }
            set
            {
                ViewState["Pedido"] = value;
            }

        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("es-AR");
            if (!Page.IsPostBack)
            {
                Initialize();
                LoadGrid(false);
            }
        }

        void Initialize()
        {
            this.GridPedidos.Settings(new GridViewExtBasic());
            this.btnRefresh.Settings(new ButtonExtText());
            this.btnFiltrar.Settings(new ButtonExtText());
            this.bntQuitarFiltros.Settings(new ButtonExtText());
            this.txtNumero.Settings(new TextBoxExtStyle());
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadGrid(false);
        }

        protected void btnBorrar_Click(object sender, EventArgs e)
        {
            ImageButton btn = (ImageButton)sender;
            int index = Convert.ToInt32(btn.CommandArgument);
            int id = Convert.ToInt32(GridPedidos.DataKeys[index].Value);

            Pedido obj = Pedidos.Find(c => c.OID == id);
            Pedidos.Remove(obj);

            //GestorProveedor.Eliminar(obj);
            LoadGrid(true);

        }

        

        private void LoadGrid(bool cache)
        {
            if (!cache)
            {
                Pedidos = GestorPedidos.Consultar();
                Pedidos = Pedidos.Where(p => p.Estado == 2).ToList();
            }

            this.GridPedidos.DataSource = Pedidos;
            this.GridPedidos.DataBind();
        }

        protected void GridPedidos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Pedido pedido = (Pedido)e.Row.DataItem;

                ImageButtonExt Imprimir = e.Row.FindControl("btnImprimir") as ImageButtonExt;
                if (Imprimir != null)
                {
                    Imprimir.Settings(new ImageButtonExtPopup());
                    Imprimir.ToolTip = "Imprimir";
                    int oid = pedido.OID;
                    Imprimir.OnClientClick = "Imprimir(" + oid.ToString() + "); return false;";
                }


                Label lab = e.Row.FindControl("lblTotal") as Label;
                if (lab != null)
                {
                    float total = pedido.PedidoDetalles.Sum(p => p.Precio * p.Cantidad);
                    lab.Text = total.ToString("C2");
                }
            }
        }

        protected void GridPedidos_PageIndexChanging(Object sender, GridViewPageEventArgs e)
        {
            GridPedidos.PageIndex = e.NewPageIndex;
            LoadGrid(true);
        }

        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            List<Pedido> filtroResult = null;

            if (!string.IsNullOrEmpty(this.txtNumero.Text))
            {
                filtroResult = Pedidos.FindAll(y => y.Numero.ToString().Contains(this.txtNumero.Text.ToLower()));
            }

            if (filtroResult == null) filtroResult = Pedidos;

            this.GridPedidos.DataSource = filtroResult;
            this.GridPedidos.DataBind();
        }
        protected void btnQuitarFiltros_Click(object sender, EventArgs e)
        {
            this.txtNumero.Text = string.Empty;

            LoadGrid(true);

        }


    }
}
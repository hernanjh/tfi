﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GSoluard.Administration.Entities;
using GSoluard.ControlsUI;
using GSoluard.Administration.Engine.Parametrizacion;
using GSoluard.WebControlExtend;
using GSoluard.Administration.Engine.Procesos;
using System.Globalization;
using System.Threading;

namespace GSoluard.WebUI.Parametrizacion
{
    public partial class Ingresos_Listado :  PageBase
    {
       
        #region Propiedades
        public List<Ingreso> Ingresos
        {
            get
            {
                if (ViewState["Ingreso"] != null)
                    return (List<Ingreso>)ViewState["Ingreso"];
                return null;
            }
            set
            {
                ViewState["Ingreso"] = value;
            }

        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("es-ES");
            if (!Page.IsPostBack)
            {
                Initialize();
                LoadGrid(false);
            }
        }

        void Initialize()
        {
            this.GridIngresos.Settings(new GridViewExtBasic());
            this.btnNuevo.Settings(new ButtonExtText());
            this.btnRefresh.Settings(new ButtonExtText());
            this.btnFiltrar.Settings(new ButtonExtText());
            this.bntQuitarFiltros.Settings(new ButtonExtText());
            this.txtNumero.Settings(new TextBoxExtStyle());
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadGrid(false);
        }

        protected void btnBorrar_Click(object sender, EventArgs e)
        {
            ImageButton btn = (ImageButton)sender;
            int index = Convert.ToInt32(btn.CommandArgument);
            int id = Convert.ToInt32(GridIngresos.DataKeys[index].Value);

            Ingreso obj = Ingresos.Find(c => c.OID == id);
            Ingresos.Remove(obj);

            //GestorProveedor.Eliminar(obj);
            LoadGrid(true);

        }

        

        private void LoadGrid(bool cache)
        {
            if (!cache)
                Ingresos = GestorIngresos.Consultar();

            this.GridIngresos.DataSource = Ingresos;
            this.GridIngresos.DataBind();
        }

        protected void GridIngresos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButtonExt Editar = e.Row.FindControl("btnEditar") as ImageButtonExt;
                if (Editar != null)
                {
                    Editar.Settings(new ImageButtonExtEdit());
                    int oid = DataBinder.Eval(e.Row.DataItem, "OID").ToInteger();
                    Editar.OnClientClick = "Editar(" + oid.ToString() + "); return false;";
                }

                ImageButtonExt Borrar = e.Row.FindControl("btnBorrar") as ImageButtonExt;
                if (Borrar != null)
                {
                    Borrar.Settings(new ImageButtonExtDelete());
                }

                ImageButtonExt Imprimir = e.Row.FindControl("btnImprimir") as ImageButtonExt;
                if (Imprimir != null)
                {
                    Imprimir.Settings(new ImageButtonExtPopup());
                    Imprimir.ToolTip = "Imprimir";
                    int oid = DataBinder.Eval(e.Row.DataItem, "OID").ToInteger();
                    Imprimir.OnClientClick = "Imprimir(" + oid.ToString() + "); return false;";
                }

            }
        }

        protected void GridIngresos_PageIndexChanging(Object sender, GridViewPageEventArgs e)
        {
            GridIngresos.PageIndex = e.NewPageIndex;
            LoadGrid(true);
        }

        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            List<Ingreso> filtroResult = null;

            if (!string.IsNullOrEmpty(this.txtNumero.Text))
            {
                filtroResult = Ingresos.FindAll(y => y.Numero.ToLower().Contains(this.txtNumero.Text.ToLower()));
            }

            if (filtroResult == null) filtroResult = Ingresos;

            this.GridIngresos.DataSource = filtroResult;
            this.GridIngresos.DataBind();
        }
        protected void btnQuitarFiltros_Click(object sender, EventArgs e)
        {
            this.txtNumero.Text = string.Empty;

            LoadGrid(true);

        }


    }
}
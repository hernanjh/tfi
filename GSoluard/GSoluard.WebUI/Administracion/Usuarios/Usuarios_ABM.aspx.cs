﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GSoluard.ControlsUI;
using GSoluard.Administration.Engine;
using GSoluard.Administration.Entities;

namespace GSoluard.WebUI.Administracion.Usuarios
{
    public partial class Usuarios_ABM : PageBase
    {
        #region Propiedades

        public Usuario Usuario
        {
            get
            {
                if (ViewState["Usuario"] != null)
                    return (Usuario)ViewState["Usuario"];
                return null;
            }
            set
            {
                ViewState["Usuario"] = value;
            }

        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadCombos();
                if (Request.QueryString["id"] == null)
                {
                   
                    Usuario = new Usuario();
                    this.lstDisponibles.DataSource = GestorRoles.Consultar();
                    this.lstDisponibles.DataTextField = "Nombre";
                    this.lstDisponibles.DataValueField = "OID";
                    this.lstDisponibles.DataBind();
                }
                else
                {
                    var Id = Convert.ToInt32(Request.QueryString["id"]);
                    Usuario = GestorUsuario.Obtener(Id);
                    this.txtUsuario.Text = Usuario.Nick ;
                    this.txtUsuario.Enabled = Usuario.Nick == string.Empty ? true : false;
                    this.txtClave.Text = Usuario.Clave;
                    this.txtClave.Attributes.Add("value", txtClave.Text);
                    this.txtNombre.Text = Usuario.Nombre;
                    this.txtApellido.Text = Usuario.Apellido;
                    this.chkBloqueado.Checked = Usuario.Bloqueado;
                    this.chkSuperAdmin.Checked = Usuario.IsSuperAdmin;
                 
                    //Opero con Roles 
                    if (Usuario.Roles.Count > 0)
                    {
                        List<Rol> rolesAsignados = Usuario.Roles;
                        this.lstAsignados.DataSource = Usuario.Roles;
                        this.lstAsignados.DataTextField = "Nombre";
                        this.lstAsignados.DataValueField = "OID";
                        this.lstAsignados.DataBind();

                        // a los roles disponibles les resto los asignados
                        var todoslosRoles = GestorRoles.Consultar();

                        foreach (Rol rol in rolesAsignados)
                        {
                            todoslosRoles.RemoveAll(x => x.Nombre == rol.Nombre);

                        }

                        this.lstDisponibles.DataSource = todoslosRoles;
                        this.lstDisponibles.DataTextField = "Nombre";
                        this.lstDisponibles.DataValueField = "OID";
                        this.lstDisponibles.DataBind();

                    }
                    else
                    {
                        this.lstDisponibles.DataSource = GestorRoles.Consultar();
                        this.lstDisponibles.DataTextField = "Nombre";
                        this.lstDisponibles.DataValueField = "OID";
                        this.lstDisponibles.DataBind();

                    }


                }
                Initialize();
               
            }


        }

        void Initialize()
        {
            this.btnGuardar.Settings(new ButtonExtText());
            this.btnCerrar.Settings(new ButtonExtText());
            this.uxLeftBtn.Settings(new ButtonExtText());
            this.uxRightBtn.Settings(new ButtonExtText());
            this.txtApellido.Settings(new TextBoxExtStyle());
            this.txtClave.Settings(new TextBoxExtStyle());
            this.txtNombre.Settings(new TextBoxExtStyle());
            this.txtUsuario.Settings(new TextBoxExtStyle());
            this.lstAsignados.Settings(new ListBoxExtStyle());
            this.lstDisponibles.Settings(new ListBoxExtStyle());
        }


        private void LoadCombos()
        {
                     


        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Usuario.Nick = this.txtUsuario.Text;
                Usuario.Clave = this.txtClave.Text;
                Usuario.Nombre = this.txtNombre.Text;
                Usuario.Apellido = this.txtApellido.Text;
                Usuario.Bloqueado = this.chkBloqueado.Checked;
                Usuario.IsSuperAdmin = this.chkSuperAdmin.Checked;

                Usuario.Roles = new List<Rol>();
                if (this.lstAsignados.Items.Count > 0)
                {
                    foreach (ListItem item in lstAsignados.Items)
                    {
                        Rol rol = new Rol();
                        rol.Nombre = item.Text;
                        rol.Codigo = item.Text;
                        rol.OID = item.Value.ToInteger();

                        Usuario.Roles.Add( rol);
                    }
                }

                GestorUsuario.Guardar(Usuario);

                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "closedialog", "parent.IntervaloCerrarPopup();", true);

            }
            catch (Exception ex)
            { }

        }
        protected void btnCerrar_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "closedialog", "parent.CerrarPopup();", true);
        }

        protected void uxRightBtn_Click(object sender, EventArgs e)
        {
          while(this.lstDisponibles.Items.Count > 0 && lstDisponibles.SelectedItem != null)
          {
            ListItem selectedItem = lstDisponibles.SelectedItem;
            selectedItem.Selected = false;
            this.lstAsignados.Items.Add(selectedItem);
            lstDisponibles.Items.Remove(selectedItem);
          }

        }

        protected void uxLeftBtn_Click(object sender, EventArgs e)
        {
            while (lstAsignados.Items.Count > 0 && lstAsignados.SelectedItem != null)
            {
                ListItem selectedItem = lstAsignados.SelectedItem;
                selectedItem.Selected = false;
                lstDisponibles.Items.Add(selectedItem);
                lstAsignados.Items.Remove(selectedItem);
            }
        }
    }
}
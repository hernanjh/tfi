﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GSoluard.Administration.Entities;
using GSoluard.Administration.Engine;
using GSoluard.ControlsUI;

namespace GSoluard.WebUI.Administracion.Roles
{
    public partial class Roles_ABM : PageBase
    {

        #region Propiedades

        public Rol Rol
        {
            get
            {
                if (ViewState["Rol"] != null)
                    return (Rol)ViewState["Rol"];
                return null;
            }
            set
            {
                ViewState["Rol"] = value;
            }

        }

        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["id"] == null)
                {
                    Rol = new Rol();
                }
                else
                {
                    var Id = Convert.ToInt32(Request.QueryString["id"]);
                    Rol = GestorRoles.Obtener(Id);
                    this.txtCodigo.Text = Rol.Codigo;
                    this.txtNombre.Text = Rol.Nombre;
                   
                }
                Initialize();
            }


        }

        void Initialize()
        {
            this.btnGuardar.Settings(new ButtonExtText());
            this.btnCerrar.Settings(new ButtonExtText());
            this.txtNombre.Settings(new TextBoxExtStyle());
            this.txtCodigo.Settings(new TextBoxExtStyle());
            PopulateModules(null, null);
        }

        void PopulateModules(List<Modulo> modulos, TreeNode parentnode)
        {
            if (modulos == null)
                modulos = GestorModulo.ObtenerTodos();
            foreach (Modulo modulo in modulos)
            {

                TreeNode node = new TreeNode(modulo.Nombre, modulo.OID.ToString());
                node.Checked = Rol.Contains(modulo);
                node.ShowCheckBox = true;
                node.SelectAction = TreeNodeSelectAction.Expand;

                if (parentnode == null)
                    treeModulos.Nodes.Add(node);
                else
                    parentnode.ChildNodes.Add(node);

                if (modulo.Modulos.Count() > 0)
                    PopulateModules(modulo.Modulos, node);

            }

        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            Rol.Codigo = this.txtCodigo.Text;
            Rol.Nombre = this.txtNombre.Text;

            List<Modulo> modulosSelected = new List<Modulo>();
            foreach (TreeNode node in treeModulos.CheckedNodes)
            {
                modulosSelected.Add(new Modulo { OID = node.Value.ToInteger(), Nombre = node.Text });

            }

            Rol.Modulos = modulosSelected;
            GestorRoles.Guardar(Rol);

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "closedialog", "parent.IntervaloCerrarPopup();", true);

        }


        protected void btnCerrar_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "closedialog", "parent.CerrarPopup();", true);
        }

      
    }
}